import Foundation

protocol JSONEncodable {
	func toJSON() -> JSON?
}


//errors
public enum JSONEncoderError: Error {
	case invalidData
	case childNotFound(String)
	case transformFailed(String)
}


public struct JSONEncoder {

	let objectUpdate = JSON()

	static func encode<T: JSONEncodable>(key: String) -> (T?) -> JSON?{
		return{
			property in

			if let property = property{
				return [key: property as AnyObject]
			}

			return nil
		}
	}

//	static func encode<T>(_: T, key: String) throws {
//
//
//		guard let value: JSON = try? value(forKey: key) else { throw JSONEncoderError.childNotFound(key)}
//
//
//	}
//
//


	static func encode<T>(forKeyPath key: String) -> ([T]?) -> JSON? {
		return {
			array in

			if let array = array {
				return [key : array as AnyObject]
			}

			return nil
		}
	}

	static func encode<T: JSONEncodable>(encodableForKey key: String) -> (T?) -> JSON? {
		return {
			model in

			if let model = model, let json = model.toJSON() {
				return [key : json as AnyObject]
			}

			return nil
		}
	}

	static func encode<T: JSONEncodable>(encodableArrayForKey key: String) -> ([T]?) -> JSON? {
		return {
			array in

			if let array = array {
				var encodedArray: [JSON] = []

				for model in array {
					guard let json = model.toJSON() else {
						return nil
					}

					encodedArray.append(json)
				}

				return [key : encodedArray as AnyObject]
			}

			return nil
		}
	}

	// MAARK: - Privates func
//	private func value<T>(forKey key: String) throws -> T{
//		guard let value = JSONData[key] as? T else { throw JSONDecoderError.keyNotFound(key)}
//		return value
//	}


}
