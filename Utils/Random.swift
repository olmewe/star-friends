import SpriteKit

/**
random number generator.
*/
class Random {
	/**
	generates an integer value, from 0 (inclusive) to sample (exclusive)
	- parameter samples: number of possible values
	- returns: random integer
	*/
	static func int(_ samples:Int) -> Int {
		return Int(arc4random_uniform(UInt32(samples)))
	}

	/** random float between 0 and 1 (both inclusive) */
	static var value:CGFloat {
		get {
			return CGFloat(arc4random())/CGFloat(UInt32.max)
		}
	}

	/** random bool */
	static var bool:Bool {
		get {
			return value > 0.5
		}
	}

	/**
	generates a float value, from min to max
	- parameter min: minimum value (inclusive)
	- parameter max: maximum value (inclusive)
	- returns: random float
	*/
	static func range(min:CGFloat,max:CGFloat) -> CGFloat {
		return min+value*(max-min)
	}

	/**
	generates an integer value, from min to max
	- parameter min: minimum value (inclusive)
	- parameter max: maximum value (exclusive)
	- returns: random integer
	*/
	static func range(min:Int,max:Int) -> Int {
		return min+int(max-min)
	}
}
