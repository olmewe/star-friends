import SpriteKit

/**
noise map class. generates random values and uses them a bit like perlin noise maps.
*/
class NoiseMap {
	private let values:[CGFloat]

	/**
	generates random values.
	- parameter samples: length of sample list
	*/
	init(samples:Int) {
		var val:[CGFloat] = []
		for _ in 0..<samples {
			val.append(Random.range(min:-1,max:1))
		}
		values = val
	}

	/**
	gets a value from the noise map.
	- parameter index: map index
	- returns: map value
	*/
	func value(at index:Int) -> CGFloat {
		var i = index%values.count
		if i < 0 { i += values.count }
		return values[i]
	}

	/**
	gets a value from the noise map.
	- parameter index: map index
	- returns: map value
	*/
	func value(at index:CGFloat) -> CGFloat {
		var a = Int(floor(index))%values.count
		if a < 0 { a += values.count }
		var b = Int(ceil(index))%values.count
		if b < 0 { b += values.count }
		var t = index.truncatingRemainder(dividingBy:1)
		if t < 0 { t += 1 }
		return t.lerp(values[a],values[b])
	}
}
