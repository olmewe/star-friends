import SpriteKit

extension CGFloat {
	/**
	clamps this value between min and max.
	- parameter min: minimum value
	- parameter max: maximum value
	- returns: clamped value
	*/
	func clamp(_ min:CGFloat,_ max:CGFloat) -> CGFloat {
		if self <= min { return min }
		if self >= max { return max }
		return self
	}

	/** this value clamped between 0 and 1 */
	var clamp01:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return self
	}

	/** this value eased in, for animations */
	var easeIn:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return self*self
	}

	/** this value eased out, for animations */
	var easeOut:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return (2-self)*self
	}

	/** this value eased, for animations */
	var ease:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return (3-2*self)*self*self
	}

	/**
	linearly interpolates two values using this one as a value between 0 and 1.
	- parameter a: value for 0
	- parameter b: value for 1
	- returns: interpolated value
	*/
	func lerp(_ a:CGFloat,_ b:CGFloat) -> CGFloat {
		if self <= 0 { return a }
		if self >= 1 { return b }
		return a+(b-a)*self
	}

	/**
	linearly interpolates 0 and 1 using this one as a value between a and b.
	it's like the opposite of lerp.
	- parameter a: value for 0
	- parameter b: value for 1
	- returns: interpolated value
	*/
	func lerpInv(_ a:CGFloat,_ b:CGFloat) -> CGFloat {
		if a == b { return 0 }
		if a < b {
			if self <= a { return 0 }
			if self >= b { return 1 }
		} else {
			if self >= a { return 0 }
			if self <= b { return 1 }
		}
		return (self-a)/(b-a)
	}

	/**
	linearly interpolates two points using this float as a value between 0 and 1.
	- parameter a: point for 0
	- parameter b: point for 1
	- returns: interpolated point
	*/
	func lerp(_ a:CGPoint,_ b:CGPoint) -> CGPoint {
		if self <= 0 { return a }
		if self >= 1 { return b }
		return CGPoint(x:lerp(a.x,b.x),y:lerp(a.y,b.y))
	}
}

extension Int {
	/**
	clamps this value between min and max.
	- parameter min: minimum value
	- parameter max: maximum value
	- returns: clamped value
	*/
	func clamp(_ min:Int,_ max:Int) -> Int {
		if self <= min { return min }
		if self >= max { return max }
		return self
	}
}

extension CGPoint {
	/**
	clamps this point between min and max.
	- parameter min: minimum point
	- parameter max: maximum point
	- returns: clamped point
	*/
	func clamp(_ min:CGPoint,_ max:CGPoint) -> CGPoint {
		return CGPoint(x:x.clamp(min.x,max.x),y:y.clamp(min.y,max.y))
	}

	/** squared magnitude of this vector */
	var sqrMagnitude:CGFloat {
		return x*x+y*y
	}

	/** magnitude of this vector */
	var magnitude:CGFloat {
		return sqrt(sqrMagnitude)
	}

	/**
	gets the square distance between this point and another.
	- parameter point: other point to compare
	- returns: squared distance
	*/
	func sqrDistance(to point:CGPoint) -> CGFloat {
		let dx = point.x-x
		let dy = point.y-y
		return dx*dx+dy*dy
	}

	/**
	gets the distance between this point and another.
	- parameter point: other point to compare
	- returns: distance
	*/
	func distance(to point:CGPoint) -> CGFloat {
		return sqrt(sqrDistance(to:point))
	}
}

extension CGSize {
	/** a 1x1 cgsize structure */
	static var one:CGSize {
		return CGSize(width:1,height:1)
	}
}

extension String {
	/** this string, localized */
	var localized:String {
		return NSLocalizedString(self,tableName:nil,bundle:Bundle.main,value:"",comment:"")
	}
}
