import SpriteKit

class SliderNode: SKNode {

	var sliderControl: SKSpriteNode!
	var sliderBar: SKSpriteNode!
	var nText = SKLabelNode()
	var porcent = 50
	var updated = false
	var touchIndex:Int = -1

	init(size: CGSize) {

		let h = size.height

		var s = CGSize(width: size.width, height: h * 0.2)
		sliderBar = SKSpriteNode(texture: SettingNode.spriteAtlas.textureNamed("sliderBar"), color: .clear, size: s)
		sliderBar.position = .zero
		sliderBar.zPosition = 3

		s = CGSize(width: h, height: h)
		sliderControl = SKSpriteNode(texture: SettingNode.spriteAtlas.textureNamed("sliderControl"), color: .clear, size: s)
		sliderControl.position = .zero
		sliderControl.zPosition = 4

		nText.fontName = "Schoolbell"
		nText.fontSize = 11
		nText.fontColor = UIColor(red: 90/255, green: 60/255, blue: 105/255, alpha: 1.0)
		nText.verticalAlignmentMode = .center
		nText.horizontalAlignmentMode = .center
		nText.text = String(porcent)
		nText.position = sliderControl.position
		nText.zPosition = 5

		super.init()

		addChild(sliderBar)
		addChild(sliderControl)
		addChild(nText)

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func update() {
		updated = false
		let m = sliderControl.size.height*0.5
		for i in Input.touchEvents {
			let pos = convert(i.position,from:SceneManager.current)
			if touchIndex < 0 {
				if i.state == .press && abs(pos.x) < sliderBar.size.width*0.5+m && abs(pos.y) < m {
					touchIndex = i.id
					Audio.play(sfx:CommonAudio.sfxSettingsOn)
				} else {
					continue
				}
			} else if i.id == touchIndex {
				if i.state == .release {
					Audio.play(sfx:CommonAudio.sfxSettingsOff)
					touchIndex = -1
				} else if i.state == .miss {
					touchIndex = -1
				}
			} else {
				continue
			}
			let m = sliderBar.size.width*0.45
			let nv = Int(pos.x.lerpInv(-m,m)*100).clamp(0,100)
			if porcent != nv {
				set(porcent:nv)
				updated = true
			}
		}
	}

	func set(porcent:Int) {
		self.porcent = porcent
		let m = sliderBar.size.width*0.5
		sliderControl.position.x = (CGFloat(porcent)/100).lerp(-m,m)
		nText.position.x = sliderControl.position.x
		nText.text = String(porcent)
	}

}
