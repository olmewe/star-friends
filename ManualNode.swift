import SpriteKit

class ManualNode: SKSpriteNode {

	let flipAnimationTime:CGFloat = 0.7
	let numberOfPages = 3
	var isOpen = false
	let atlas = SKTextureAtlas(named: "manual")
	var currentPageNumber = 0
	var page:PageNode
	var menuButton:GameNavbarButtonNode
	var nextButton:GameNavbarButtonNode
	var backButton:GameNavbarButtonNode
	var cameFromMenu = false
	var animating = false
	var animTimer:CGFloat = 0.0
	var prevPos = CGPoint.zero

	init() {

		menuButton = GameNavbarButtonNode(text: "menu")
		nextButton = GameNavbarButtonNode(text: "próximo")
		backButton = GameNavbarButtonNode(text: "voltar")

		menuButton.zPosition = 13
		nextButton.zPosition = 13
		backButton.zPosition = 13

		backButton.isHidden = true

		self.page = PageNode(page: -1, size: .zero, atlas:atlas)

		let size = CGSize(width: Window.sceneSize.width, height: Window.sceneSize.height)
		super.init(texture: atlas.textureNamed("background"), color: .clear, size: size)

		self.isHidden = true

		self.page = loadPage(currentPageNumber)
		changeButtons(toPage: currentPageNumber)
		self.addChild(page)

		let rightSide = SKSpriteNode(texture: atlas.textureNamed("background"), color: .clear, size: size)
		rightSide.xScale = -1
		rightSide.position = CGPoint(x: self.size.width, y: 0.0)

		self.addChild(rightSide)

		let separator = SKSpriteNode(texture: nil, color: self.page.lastColor(), size: CGSize(width: 5.0, height: self.size.height * 0.9477))
		separator.position.x = self.size.width/2
		separator.position.y = 0.0
		separator.zPosition	= 15

		self.addChild(separator)

		changeButtons(toPage: currentPageNumber)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func showManual(fromMenu:Bool) {

		self.cameFromMenu = fromMenu
		self.isOpen = true

		if(fromMenu == false) {
			menuButton.text.set(text: "continuar")
			self.position = CGPoint(x: Window.sceneSize.width * 0.4, y: Window.sceneSize.height * 0.45)

		}

		else {
			menuButton.text.set(text: "menu")
			self.position = CGPoint(x: Window.sceneSize.width * 0.35, y: Window.sceneSize.height * -0.33)
		}

		self.setScale(1.0)
		changeButtons(toPage: currentPageNumber)
		self.setScale(0.01)
		self.isHidden = false
		prevPos = self.position

		let act1 = SKAction.scale(to: 1.0, duration: TimeInterval(flipAnimationTime*0.5))
		let act2 = SKAction.move(to: CGPoint.zero, duration: TimeInterval(flipAnimationTime*0.5))

		let action = SKAction.group([act1, act2])

		action.timingMode = .easeIn

		self.run(action)

		Audio.play(sfx:CommonAudio.sfxManualOpen)
		//page.color = .white
	}

	func closeManual() {

		self.isOpen = false

		let action = SKAction.group([SKAction.scale(to: 0.01, duration: TimeInterval(flipAnimationTime*0.5)), SKAction.move(to: prevPos, duration: TimeInterval(flipAnimationTime*0.5))])
		action.timingMode = .easeOut

		self.run(action){
			Audio.play(sfx:CommonAudio.sfxManualClose)

			self.removeFromParent()
		}
	}

	func update() {

		if(animating) {
			animTimer += Time.deltaTime

			if(animTimer > flipAnimationTime) {
				animating = false
			}
		}

		menuButton.updateButton()
		nextButton.updateButton()
		backButton.updateButton()

		if(menuButton.released) {
			closeManual()
		}

		else if(nextButton.released) {
			showNextPage()
		}

		else if(backButton.parent != nil) {
			if(backButton.released) {
				showPreviousPage()
			}
		}
	}


	private func showNextPage() {

		if !animating {
			animating = true
			animTimer = 0.0
		}

		else {
			return
		}

		if(currentPageNumber + 1 >= numberOfPages) {
			return
		}

		currentPageNumber += 1
		changeButtons(toPage: currentPageNumber)

		let previousPage = self.page
		let nextPage = loadPage(currentPageNumber)
		nextPage.xScale = -1.0

		nextPage.zPosition = 8
		previousPage.zPosition = 7

		self.page = nextPage
		self.page.setLastColor()
		self.addChild(self.page)

		/*nextPage.run(SKAction.scaleX(to: 1.0, duration: TimeInterval(flipAnimationTime))) {
			previousPage.removeFromParent()
			//self.animating = false
			self.page.firstColor()
		}*/

		let act1 = SKAction.scaleX(to: 1.0, duration: TimeInterval(flipAnimationTime))
		let act2 = SKAction.colorize(with: self.page.firstColor(), colorBlendFactor: 1.0, duration: TimeInterval(flipAnimationTime))
		act1.timingMode = .easeIn

		nextPage.run(SKAction.group([act1,act2])) {
			previousPage.removeFromParent()
			self.page.setFirstColor()
			self.animating = false
		}

		Audio.play(sfx:CommonAudio.sfxManualNext)
	}

	private func showPreviousPage() {

		if !animating {
			animating = true
			animTimer = 0.0
		}

		else {
			return
		}

		if(currentPageNumber <= 0) {
			return
		}

		currentPageNumber -= 1
		changeButtons(toPage: currentPageNumber)

		let previousPage = loadPage(currentPageNumber)
		let nextPage = self.page

		nextPage.zPosition = 8
		previousPage.zPosition = 7

		nextPage.setFirstColor()

		self.page = previousPage
		self.page.setFirstColor()

		self.addChild(self.page)


		/*nextPage.run(SKAction.scaleX(to: -1.0, duration: TimeInterval(flipAnimationTime))) {
			nextPage.removeFromParent()
			//self.animating = false
		}*/

		let act1 = SKAction.scaleX(to: -1.0, duration: TimeInterval(flipAnimationTime))
		let act2 = SKAction.colorize(with: self.page.lastColor(), colorBlendFactor: 1.0, duration: TimeInterval(flipAnimationTime))
		act1.timingMode = .easeIn

		nextPage.run(SKAction.group([act1,act2])) {
			nextPage.removeFromParent()
			self.animating = false
		}

		Audio.play(sfx:CommonAudio.sfxManualPrevious)
	}

	private func changeButtons(toPage:Int) {

		let y = self.size.height * -0.4
		let x:CGFloat = 0.0
		let space = self.menuButton.sprite.size.width + 8.0

		if toPage == 0 {

			self.menuButton.position = CGPoint(x: x, y: y)
			self.nextButton.position = CGPoint(x: x + space * 2, y: y)
			self.backButton.isHidden = true

		}

		else if toPage == numberOfPages - 1 {

			self.menuButton.position = CGPoint(x: x, y: y)
			self.nextButton.position = CGPoint(x: x + space * 2, y: y)
			self.backButton.position = CGPoint(x: x + space, y: y)
			self.nextButton.isHidden = true
		}

		else {

			self.menuButton.position = CGPoint(x: x, y: y)
			self.nextButton.position = CGPoint(x: x + space * 2, y: y)
			self.backButton.position = CGPoint(x: x + space, y: y)
			self.backButton.isHidden = false
			self.nextButton.isHidden = false
		}

		if menuButton.parent == nil {
			self.addChild(menuButton)
		}

		if nextButton.parent == nil {
			self.addChild(nextButton)
			self.addChild(backButton)
		}

	}

	private func loadPage(_ i:Int)->PageNode {

		var size = CGSize(width: Window.sceneSize.width, height: Window.sceneSize.height)
		size.width *= 0.923
		size.height *= 0.9477

		let p = PageNode(page: currentPageNumber, size: size, atlas:atlas)
		p.position = CGPoint(x: self.size.width/2, y: 0.0)

		return p

		/*page.run(SKAction.scaleX(to: -1.0, duration: 0.0))
		self.addChild(page)

		page.run(SKAction.scaleX(to: 1.0, duration: 1.0)) {
			self.page.nextColor()
		}*/


	}

	private func getTextNodeWith(scale:CGFloat, maxWidth:CGFloat, text:String)->TextNode {

		let width = (1 / scale) * maxWidth
		let node = TextNode(fontNamed: "Schoolbell")
		node.align = .left
		node.maxWidth = width
		node.set(text: text, animate: false)
		node.setScale(scale)

		return node

	}

}
