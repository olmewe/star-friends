import SpriteKit

/**
dialogue scene, for talking to entities and also avoiding their attacks.
*/
class DialogueScene:Scene {
	override class func create() -> Scene {
		return DialogueScene(size:Window.sceneSize)
	}

	let fadeNode = SKSpriteNode()
	var leaveScene = false
	var transitionTempo:CGFloat = 0
	static var menuTransition = false
	var leaveThanks = false

	let gameNavbarNode = GameNavbarNode()

	let lightFadeNode = SKSpriteNode()
	let backgroundNode = SKSpriteNode()
	let backgroundOffset:CGFloat = 30

	let battleNode = BattleNode()
	let battleStartButton = ButtonSpriteNode()
	var battleTransitionTempo:CGFloat = 0

	var playerBubble:DialogueSpeechBubbleNode!
	var entityBubble:DialogueSpeechBubbleNode!
	let playerPortrait = SKSpriteNode()
	let entityPortrait = SKSpriteNode()
	var playerTextures:[SKTexture] = []
	var entityTextures:[SKTexture] = []
	var playerBar:BattleLifeNode!
	var entityBar:BattleLifeNode!
	var playerBarPosA = CGPoint.zero
	var playerBarPosB = CGPoint.zero
	var entityBarPosA = CGPoint.zero
	var entityBarPosB = CGPoint.zero
	let playerBarText = SKLabelNode(fontNamed:"Schoolbell")
	let entityBarText = SKLabelNode(fontNamed:"Schoolbell")
	var barTextOffset:CGFloat = 0

	var dialogueBlock:DialogueBlock = DialogueBlock()
	var dialogueOptions:[DialogueOption] = []
	var goodState = true

	var availableOptions:[Int] = []
	var dialogueBlockLine:Int = 0
	var playerSpeaks:Bool = true

	static let dispositionMin:CGFloat = 0.2
	static let dispositionStart:CGFloat = 0.5
	static let dispositionMax:CGFloat = 0.8

	var currentlyShowing:DialogueShowing = .text
	enum DialogueShowing {
		case text
		case options
		case prebattle
		case battle
	}

	let bgmDialogue = "bgm/dialogue.mp3"
	let bgmBattle = "bgm/battle.mp3"
	let sfxDialogueBattleButton = "dialogue/battleButton.wav"
	let sfxDialogueOptionsHide = "dialogue/optionsHide.wav"
	let sfxDialogueOptionsShow = "dialogue/optionsShow.wav"
	let sfxDialogueSpeech = "dialogue/speech.wav"
	let sfxDialogueSpeechOption = "dialogue/speechOption.wav"
	let sfxBattleCountdown = "battle/countdown.wav"
	let sfxBattleEnd = "battle/end.wav"
	let sfxBattleEnemyDamage = "battle/enemyDamage.wav"
	let sfxBattleEnemyShoot = "battle/enemyShoot.wav"
	let sfxBattleObstacle = "battle/obstacle.wav"
	let sfxBattlePlayerDamage = "battle/playerDamage.wav"
	let sfxBattlePlayerShoot = "battle/playerShoot.wav"
	let sfxBattleTransition = "battle/transition.wav"

	var currentBgmIsBattle = false
	var textSfxEnabled = false
	var optionSfxEnabled = false

	override func loadResources() {
		Audio.load(batch:"dialogue",files:[
			bgmDialogue,
			bgmBattle,
			sfxDialogueBattleButton,
			sfxDialogueOptionsHide,
			sfxDialogueOptionsShow,
			sfxDialogueSpeech,
			sfxDialogueSpeechOption,
			sfxBattleCountdown,
			sfxBattleEnd,
			sfxBattleEnemyDamage,
			sfxBattleEnemyShoot,
			sfxBattleObstacle,
			sfxBattlePlayerDamage,
			sfxBattlePlayerShoot,
			sfxBattleTransition,
		])
	}

	override func unloadResources() {
		Audio.unload(batch:"dialogue")
	}

	override func load() {
		backgroundColor = .gray

		addChild(fadeNode)
		fadeNode.zPosition = 110
		fadeNode.color = TransitionScene.fadeColour
		fadeNode.colorBlendFactor = 1
		fadeNode.position = CGPoint.zero
		fadeNode.size = Window.sceneSize

		addChild(gameNavbarNode)
		gameNavbarNode.zPosition = 100
		gameNavbarNode.position = CGPoint.zero
		buttons += gameNavbarNode.buttons

		let s = gameNavbarNode.size
		let c = gameNavbarNode.centre.y
		let battleButtonSize:CGFloat = Window.padMode ? 100 : 160
		let bubbleWidth = s.width*0.93
		let margin = s.width-bubbleWidth
		let bubbleHeight = (s.height-margin)*0.25
		let portraitSize = bubbleHeight*0.95
		let portraitOffset = bubbleHeight*0.05
		let posX = bubbleWidth*0.5
		let posY = (s.height-margin)*0.5
		let barSize = CGSize(width:s.width*0.6,height:s.height*0.05)
		let barBattleOffset = s.height*0.445
		let fontSize:CGFloat = Window.padMode ? 10 : 16

		addChild(backgroundNode)
		backgroundNode.zPosition = -1
		backgroundNode.position = gameNavbarNode.centre
		backgroundNode.size = CGSize(width:s.width+backgroundOffset*2,height:s.height+backgroundOffset*2)
		backgroundNode.texture = battleNode.spriteAtlas.textureNamed("background2")

		addChild(lightFadeNode)
		lightFadeNode.zPosition = 99
		lightFadeNode.color = SKColor(colorLiteralRed:63/255,green:67/255,blue:80/255,alpha:1)
		lightFadeNode.colorBlendFactor = 1
		lightFadeNode.size = Window.sceneSize

		addChild(battleNode)
		battleNode.zPosition = 10
		battleNode.position = gameNavbarNode.centre
		battleNode.dialogueScene = self

		addChild(battleStartButton)
		battleStartButton.zPosition = 40
		battleStartButton.position = CGPoint(x:0,y:c+posY-bubbleHeight*0.5-portraitSize)
		battleStartButton.sprite.texture = SKTexture(image:#imageLiteral(resourceName: "ui/battleButtonPlaceholder"))
		battleStartButton.sprite.size = CGSize(width:battleButtonSize,height:battleButtonSize*0.378)
		battleStartButton.interactable = false
		buttons.append(battleStartButton)

		playerBubble = DialogueSpeechBubbleNode(player:true,width:bubbleWidth,height:bubbleHeight)
		addChild(playerBubble)
		playerBubble.position = CGPoint(x:0,y:c+bubbleHeight*0.5-posY)
		playerBubble.zPosition = 1
		for i in playerBubble.options {
			buttons.append(i)
		}

		entityBubble = DialogueSpeechBubbleNode(player:false,width:bubbleWidth,height:bubbleHeight)
		addChild(entityBubble)
		entityBubble.position = CGPoint(x:0,y:c+posY-bubbleHeight*0.5-portraitSize)
		entityBubble.zPosition = 1

		addChild(playerPortrait)
		playerPortrait.size = CGSize(width:portraitSize,height:portraitSize)
		playerPortrait.position = CGPoint(x:portraitOffset+portraitSize*0.5-posX,y:c+bubbleHeight+portraitSize*0.5-posY)
		playerPortrait.zPosition = 0

		addChild(entityPortrait)
		entityPortrait.size = CGSize(width:portraitSize,height:portraitSize)
		entityPortrait.position = CGPoint(x:posX-portraitSize*0.5-portraitOffset,y:c+posY-portraitSize*0.5)
		entityPortrait.zPosition = 0

		playerTextures = [
			SKTexture(image:#imageLiteral(resourceName: "entities/player/portrait0")),
			SKTexture(image:#imageLiteral(resourceName: "entities/player/portrait1")),
		]

		playerBar = BattleLifeNode(type:1,currentValue:0,size:barSize)
		addChild(playerBar)
		playerBar.position = CGPoint.zero
		playerBar.zPosition = 30
		playerBarPosA = CGPoint(x:posX-barSize.width*0.5,y:c+barSize.height*0.5+bubbleHeight+margin*0.5-posY)
		playerBarPosB = CGPoint(x:0,y:c-barBattleOffset)

		entityBar = BattleLifeNode(type:0,currentValue:0,size:barSize)
		addChild(entityBar)
		entityBar.position = CGPoint.zero
		entityBar.zPosition = 30
		entityBarPosA = CGPoint(x:barSize.width*0.5-posX,y:c+posY-barSize.height*0.5)
		entityBarPosB = CGPoint(x:0,y:c+barBattleOffset)

		addChild(entityBarText)
		entityBarText.text = "DISPOSIÇÃO PARA O DIÁLOGO"
		entityBarText.fontSize = fontSize
		entityBarText.fontColor = DialogueSpeechBubbleNode.entityColour
		entityBarText.verticalAlignmentMode = .center
		entityBarText.horizontalAlignmentMode = .center
		entityBarText.position = CGPoint.zero
		entityBarText.zPosition = 30

		addChild(playerBarText)
		playerBarText.text = "PONTOS DE VIDA"
		playerBarText.fontSize = fontSize
		playerBarText.fontColor = DialogueSpeechBubbleNode.playerColour
		playerBarText.verticalAlignmentMode = .center
		playerBarText.horizontalAlignmentMode = .center
		entityBarText.position = CGPoint.zero
		playerBarText.zPosition = 30

		barTextOffset = s.height*0.05
	}

	override func show() {
		Audio.stopBgms()
		Audio.play(bgm:[sfxDialogueSpeech,sfxDialogueSpeechOption],volume:[0,0],isDialogue:true)
		textSfxEnabled = false
		optionSfxEnabled = false

		leaveScene = false
		transitionTempo = 1
		leaveThanks = false

		gameNavbarNode.interactable = false

		playerBubble.reset()
		entityBubble.reset()
		if let name = GameState.dialogueEntity?.name {
			var path:String
			if GameState.room?.entity.isContextual == true {
				path = "contexts/"+LevelContext.current.name+"/"
			} else {
				path = ""
			}
			path += "entities/"+name+"/portrait"
			entityTextures = [
				SKTexture(imageNamed:path+"0"),
				SKTexture(imageNamed:path+"1"),
				SKTexture(imageNamed:path+"2"),
				SKTexture(imageNamed:path+"3"),
				SKTexture(imageNamed:path+"4"),
			]
		} else {
			entityTextures = []
		}
		setPortrait(player:true,index:0)
		setPortrait(player:false,index:(GameState.entityDisposition < DialogueScene.dispositionStart) ? 1 : 3)

		battleNode.isHidden = true
		battleStartButton.isHidden = true
		battleStartButton.interactable = false

		playerBar.updateValue(GameState.playerHealth,animated:false)
		entityBar.updateValue(GameState.entityDisposition,animated:false)
		dialogueBlockLine = 0

		availableOptions.removeAll()

		if showDialogue() {
			if currentlyShowing == .battle {
				Audio.play(bgm:[bgmDialogue,bgmBattle],volume:[0,1])
				currentBgmIsBattle = true
				battleTransitionTempo = 1
			} else {
				Audio.play(bgm:[bgmDialogue,bgmBattle],volume:[1,0])
				currentBgmIsBattle = false
				battleTransitionTempo = 0
			}
			updateBattleTransition()
		} else {
			endScene()
		}
		sceneTransitioning()
	}

	override func hide() {
		entityTextures = []
	}

	override func update() {
		backgroundNode.position = CGPoint(
			x:gameNavbarNode.centre.x+sin(Time.time*0.3)*backgroundOffset*0.5,
			y:gameNavbarNode.centre.y+cos(Time.time*0.3)*backgroundOffset*0.5
		)

		if leaveScene {
			gameNavbarNode.interactable = false
			if transitionTempo < 1 {
				transitionTempo += Time.deltaTime
				if transitionTempo >= 1 {
					transitionTempo = 1
					if DialogueScene.menuTransition {
						TransitionScene.toGame = false
						TransitionScene.thanks = leaveThanks
						SceneManager.show(SceneManager.transitionScene)
					} else {
						RoomScene.menuTransition = false
						SceneManager.show(SceneManager.roomScene)
					}
				} else {
					sceneTransitioning()
				}
			}
		} else {
			if transitionTempo > 0 {
				gameNavbarNode.interactable = false
				transitionTempo -= Time.deltaTime*2
				if transitionTempo <= 0 {
					transitionTempo = 0
					DialogueScene.menuTransition = false
					gameNavbarNode.interactable = true
				}
				sceneTransitioning()
			}
		}

		var battleTransitioning = false
		if currentlyShowing == .battle {
			if !currentBgmIsBattle {
				currentBgmIsBattle = true
				Audio.set(bgm:[bgmDialogue,bgmBattle],volume:[0,1],fade:0.5)
				Audio.play(sfx:sfxBattleTransition)
			}
			if battleTransitionTempo < 1 {
				if battleNode.isHidden {
					battleNode.isHidden = false
				}
				battleTransitionTempo += Time.deltaTime*2
				if battleTransitionTempo >= 1 {
					battleTransitionTempo = 1
				}
				battleTransitioning = true
			}
		} else {
			if currentBgmIsBattle {
				currentBgmIsBattle = false
				Audio.set(bgm:[bgmDialogue,bgmBattle],volume:[1,0],fade:0.5)
				Audio.play(sfx:sfxBattleTransition)
			}
			if battleTransitionTempo > 0 {
				battleTransitionTempo -= Time.deltaTime*2
				if battleTransitionTempo <= 0 {
					battleTransitionTempo = 0
					battleNode.isHidden = true
				} else {
					battleTransitioning = true
				}
			}
		}

		let interactable = menusClosed && !battleTransitioning && transitionTempo <= 0.001
		switch currentlyShowing {
		case .text:
			if interactable {
				for i in Input.touchEvents {
					if i.state == .press {
						if gameNavbarNode.isOverNavbar(pos:i.position) {
							continue
						}
						if !advanceDialogue() {
							endScene()
						}
						break
					}
				}
			}
		case .options:
			if interactable {
				for (n,i) in playerBubble.options.enumerated() {
					guard i.released && availableOptions[n] >= 0 else { continue }
					GameState.dialogueOption = availableOptions[n]
					if dialogueOptions[GameState.dialogueOption].rightAnswer {
						GameState.dialogueRightAnswer = true
						goodState = true
						availableOptions.removeAll(keepingCapacity:true)
					} else {
						GameState.dialogueRightAnswer = false
						goodState = false
						availableOptions[n] = -1
					}
					if !advanceDialogue() {
						endScene()
					}
					break
				}
				for i in Input.touchEvents {
					if i.state == .press {
						if gameNavbarNode.isOverNavbar(pos:i.position) {
							continue
						}
						_ = playerBubble.forceAnimationEnd()
						break
					}
				}
			}
		case .prebattle:
			if !battleStartButton.interactable {
				Audio.play(sfx:sfxDialogueBattleButton)
				battleStartButton.isHidden = false
				battleStartButton.interactable = true
			}
			if battleStartButton.released {
				battleStartButton.isHidden = true
				battleStartButton.interactable = false
				currentlyShowing = .battle
				GameState.playerHealthTemp = GameState.playerHealth
				GameState.entityDispositionTemp = GameState.entityDisposition
				battleNode.reset()
			}
		case .battle:
			if !battleNode.update() {
				GameState.playerHealth = GameState.playerHealthTemp
				GameState.entityDisposition = GameState.entityDispositionTemp
				goodState = GameState.playerHealth > 0
				if !advanceDialogue() {
					endScene()
				}
			}
		}

		playerBubble.update()
		entityBubble.update()
		if currentlyShowing == .battle {
			playerBar.updateValue(GameState.playerHealthTemp,animated:true)
			entityBar.updateValue(GameState.entityDispositionTemp,animated:true)
		} else {
			playerBar.updateValue(GameState.playerHealth,animated:true)
			entityBar.updateValue(GameState.entityDisposition,animated:true)
		}
		if playerBubble.text.characterAnim || entityBubble.text.characterAnim {
			if !textSfxEnabled {
				textSfxEnabled = true
				Audio.set(bgm:sfxDialogueSpeech,volume:1)
			}
		} else {
			if textSfxEnabled {
				textSfxEnabled = false
				Audio.set(bgm:sfxDialogueSpeech,volume:0)
			}
		}
		if entityBubble.options.contains(where:{ $0.text.characterAnim }) {
			if !optionSfxEnabled {
				optionSfxEnabled = true
				Audio.set(bgm:sfxDialogueSpeechOption,volume:1)
			}
		} else {
			if optionSfxEnabled {
				optionSfxEnabled = false
				Audio.set(bgm:sfxDialogueSpeechOption,volume:0)
			}
		}

		if battleTransitioning {
			updateBattleTransition()
		}

		if menusClosed {
			if gameNavbarNode.menuButton.released {
				DialogueScene.menuTransition = true
				Audio.play(sfx:CommonAudio.sfxTransition)
				endScene()
			} else if gameNavbarNode.optionsButton.released {
				openSettings(fromMenu: false)
			} else if gameNavbarNode.manualButton.released {
				openManual(fromMenu: false)
			}
		}
	}

	/** does whatever it needs to do to leave this scene. */
	func endScene() {
		leaveScene = true
		Audio.set(bgm:[bgmDialogue,bgmBattle],volume:[0,0],fade:0.75)
		if !leaveThanks {
			GameState.saveLevelState()
		}
	}

	/** animates the transition between this scene and another. */
	func sceneTransitioning() {
		lightFadeNode.alpha = (transitionTempo*1.25).easeOut
		lightFadeNode.isHidden = fadeNode.alpha <= 0.0001
		if DialogueScene.menuTransition {
			fadeNode.alpha = lightFadeNode.alpha
			fadeNode.isHidden = lightFadeNode.isHidden
		} else {
			fadeNode.isHidden = true
		}
	}

	/**
	updates someone's portrait picture.
	- parameter player: if true, sets player's. otherwise, sets entity's
	- paramter index: texture index
	*/
	func setPortrait(player:Bool,index:Int) {
		let portrait:SKSpriteNode
		let textures:[SKTexture]
		if player {
			portrait = playerPortrait
			textures = playerTextures
		} else {
			portrait = entityPortrait
			textures = entityTextures
		}
		if index >= 0 && index < textures.count {
			portrait.texture = textures[index]
		}
	}

	/** updates positions for the battle node, entity and player bars, their labels, etc. */
	func updateBattleTransition() {
		battleNode.xScale = battleTransitionTempo.easeOut
		let tr = battleTransitionTempo.ease
		playerBar.position = tr.lerp(playerBarPosA,playerBarPosB)
		entityBar.position = tr.lerp(entityBarPosA,entityBarPosB)
		playerBarText.position = CGPoint(x:playerBar.position.x,y:playerBar.position.y+barTextOffset)
		entityBarText.position = CGPoint(x:entityBar.position.x,y:entityBar.position.y-barTextOffset)
	}

	/**
	shows the current line of dialogue.
	- returns: false if the dialogue has ended. true otherwise
	*/
	func showDialogue() -> Bool {
		if !loadState() { return false }
		showDialogueRaw()
		return true
	}

	/**
	advances to the next line of dialogue.
	- returns: false if the dialogue has ended. true otherwise
	*/
	func advanceDialogue() -> Bool {
		let text:DialogueSpeechBubbleNode! = playerSpeaks ? playerBubble : entityBubble
		if text.forceAnimationEnd() {
			return true
		}
		var advance = false
		if GameState.dialogueState == .battle || GameState.dialogueState == .options {
			advance = true
		} else {
			dialogueBlockLine += 1
			if dialogueBlockLine >= dialogueBlock.lines.count {
				advance = true
			}
		}
		if advance && !advanceState() { return false }
		showDialogueRaw()
		return true
	}

	/** shows current dialogue. */
	func showDialogueRaw() {
		var t:String = ""
		var p:Int = -1
		let lastPlayerSpeaks = playerSpeaks

		if GameState.dialogueState == .options {
			playerSpeaks = true
			var length:Int
			if availableOptions.isEmpty {
				let totalLength = min(dialogueOptions.count,playerBubble.options.count)
				if totalLength == 0 {
					GameState.dialogueState = .sadEnding
					dialogueBlockLine = 0
					_ = showDialogue()
					return
				}
				for n in 0..<totalLength {
					let l = dialogueOptions[n]
					if l.clue < 0 || GameState.clues.contains(l.clue) {
						availableOptions.append(n)
					} else {
						availableOptions.append(-1)
					}
				}
				length = totalLength
				for i in (1...length) {
					availableOptions.append(availableOptions.remove(at:Random.int(i)))
				}
			} else {
				length = 0
				for i in availableOptions {
					if i >= 0 { length += 1 }
				}
				if length == 0 {
					GameState.dialogueState = .sadEnding
					dialogueBlockLine = 0
					_ = showDialogue()
					return
				}
			}
			playerBubble.set(options:availableOptions.map { ($0 < 0) ? "" : dialogueOptions[$0].text })
			currentlyShowing = .options
		} else if GameState.dialogueState == .battle {
			currentlyShowing = .prebattle
		} else {
			playerSpeaks = dialogueBlock.lines[dialogueBlockLine].playerSpeaks
			t = dialogueBlock.lines[dialogueBlockLine].text
			p = dialogueBlock.lines[dialogueBlockLine].avatar
			currentlyShowing = .text
		}
		if currentlyShowing == .text {
			let text:DialogueSpeechBubbleNode!
			if playerSpeaks {
				text = playerBubble
			} else {
				text = entityBubble
			}
			text.set(text:t)
			setPortrait(player:playerSpeaks,index:p)
		}
		if currentlyShowing == .prebattle {
			setPortrait(player:true,index:0)
			playerBubble.clear()
			entityBubble.clear()
			playerBubble.showTouch = false
			entityBubble.showTouch = false
		} else if playerSpeaks {
			playerBubble.showTouch = true
			entityBubble.showTouch = false
			if lastPlayerSpeaks {
				entityBubble.clear()
			} else if GameState.dialogueState == .options {
				setPortrait(player:true,index:0)
			}
		} else {
			playerBubble.showTouch = false
			entityBubble.showTouch = true
			if !lastPlayerSpeaks {
				setPortrait(player:true,index:0)
				playerBubble.clear()
			}
		}
	}

	/**
	loads the current dialogue state, and automatically advances if there's nothing to do during that state.
	returns: false if the dialogue has ended. true otherwise
	*/
	func loadState() -> Bool {
		while true {
			switch loadStateRaw() {
			case .notFound:
				goodState = true
				if advanceStateRaw() {
					continue
				}
				GameState.endDialogue(won:false)
				return false
			case .cyclesEnded:
				if GameState.dialogueEntity?.name == "boss" {
					DialogueScene.menuTransition = true
					leaveThanks = true
				}
				GameState.endDialogue(won:true)
				return false
			case .loaded:
				return true
			}
		}
	}

	/**
	advances current dialogue state, and keeps on advancing if there's nothing to do during that state.
	- parameter goodState: if true, considers what happened in this state a good thing (for options and battles)
	- returns: false if the dialogue has ended. true otherwise
	*/
	func advanceState() -> Bool {
		if advanceStateRaw() {
			return loadState()
		}
		GameState.endDialogue(won:false)
		return false
	}

	/**
	loads the current dialogue state.
	- returns: the result of the load operation.
	*/
	func loadStateRaw() -> LoadStateResult {
		if GameState.dialogueState == .battle {
			return .loaded
		}
		guard let entity = GameState.dialogueEntity else { return .notFound }
		let block:DialogueBlock?
		if GameState.dialogueState.isPerCycleDialogue {
			if GameState.dialogueCycle < 0 { return .notFound }
			if GameState.dialogueCycle >= entity.dialogueCycles.count { return .cyclesEnded }
			let cycle = entity.dialogueCycles[GameState.dialogueCycle]
			switch GameState.dialogueState {
			case .beforeOptions: block = cycle.beforeOptions
			case .answer:
				if GameState.dialogueOption >= 0 && GameState.dialogueOption < cycle.answer.count {
					block = cycle.answer[GameState.dialogueOption]
				} else {
					block = nil
				}
			case .options:
				dialogueOptions = cycle.options
				dialogueBlockLine = 0
				return .loaded
			default: block = nil
			}
		} else {
			block = entity.dialogues[GameState.dialogueState]
		}
		guard let newBlock = block else { return .notFound }
		guard newBlock.lines.count > 0 else { return .notFound }
		dialogueBlock = newBlock
		dialogueBlockLine = 0
		return .loaded
	}
	enum LoadStateResult {
		case notFound
		case cyclesEnded
		case loaded
	}

	/**
	advances current dialogue state.
	- parameter goodState: if true, considers what happened in this state a good thing (for options and battles)
	- returns: false if there's nothing else to do in this scene. true otherwise
	*/
	func advanceStateRaw() -> Bool {
		let s:DialogueBlockType
		switch GameState.dialogueState {
		case .intro:
			if GameState.entityDisposition < DialogueScene.dispositionStart {
				s = .battle
			} else {
				s = .beforeOptions
			}
		case .battle:
			if goodState {
				s = .afterBattle
			} else {
				s = .sadEnding
			}
		case .beforeOptions:
			s = .options
		case .options:
			s = .answer
			if goodState {
				GameState.entityDisposition += GameState.dialogueEntity?.rightAnswerDisposition ?? 0
			} else {
				GameState.entityDisposition += GameState.dialogueEntity?.wrongAnswerDisposition ?? 0
			}
		case .answer:
			if goodState {
				s = .beforeOptions
				GameState.dialogueCycle += 1
			} else if GameState.entityDisposition < DialogueScene.dispositionMin {
				s = .beforeBattle
			} else {
				s = .options
			}
		case .beforeBattle:
			s = .battle
		case .afterBattle:
			s = .beforeOptions
		case .sadEnding:
			return false
		case .sadIntro:
			s = .battle
		}
		GameState.dialogueState = s
		return true
	}
}
