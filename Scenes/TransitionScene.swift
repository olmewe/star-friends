import SpriteKit

class TransitionScene:Scene {
	override class func create() -> Scene {
		return TransitionScene(size:Window.sceneSize)
	}

	static let fadeColour = #colorLiteral(red: 0.1019607843, green: 0.1019607843, blue: 0.1019607843, alpha: 1)
	static var toGame = true
	static var thanks = false

	let fade = SKSpriteNode()
	let background = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "transition/bg")))
	let spaceship = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "transition/spaceship")))
	let planet = SKSpriteNode()

	let balloonNode = SKNode()
	let balloon = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "transition/balloon")))
	let balloonText = SKLabelNode(fontNamed:"Schoolbell")

	var backgroundOffset:CGFloat = 0
	var spaceshipPosA = CGPoint.zero
	var spaceshipPosB = CGPoint.zero
	var spaceshipRotA:CGFloat = 0
	var spaceshipRotB:CGFloat = 0
	var planetPos = CGPoint.zero
	var balloonOffset:CGFloat = 0

	var animationTime:CGFloat = 0
	var fadeTime:CGFloat = 0
	var playSfx = true

	let sfxFromGame = "transition/fromGame.wav"
	let sfxToGame = "transition/toGame.wav"

	override func loadResources() {
		Audio.load(batch:"transition",files:[TransitionScene.toGame ? sfxToGame : sfxFromGame])
	}

	override func unloadResources() {
		Audio.unload(batch:"transition")
	}

	override func load() {
		let s = Window.sceneSize

		addChild(fade)
		fade.zPosition = 10
		fade.position = CGPoint.zero
		fade.size = s
		fade.colorBlendFactor = 1
		fade.color = TransitionScene.fadeColour

		addChild(background)
		background.zPosition = 0
		let offset:CGFloat = 0.1
		backgroundOffset = s.width*offset
		background.size = CGSize(width:s.height*3/4,height:s.height)
		background.setScale(1+offset*2)

		addChild(spaceship)
		spaceship.zPosition = 3
		let spaceshipHeight = s.width*0.2
		spaceship.size = CGSize(width:spaceshipHeight*0.863,height:spaceshipHeight)
		spaceshipPosB = CGPoint(x:s.width*0.75,y:s.height*0.25)
		spaceshipPosA = CGPoint(x:-spaceshipPosB.x,y:-spaceshipPosB.y)
		spaceshipRotA = atan2(spaceshipPosB.y,spaceshipPosB.x)
		spaceshipRotB = spaceshipRotA+CGFloat.pi

		addChild(planet)
		planet.zPosition = 1
		let planetSize = s.width*0.25
		planet.size = CGSize(width:planetSize,height:planetSize)
		planetPos = CGPoint(x:s.width*0.3,y:s.height*0.3)

		addChild(balloonNode)
		balloonNode.zPosition = 2
		let balloonHeight = s.width*0.2
		balloonOffset = s.width*0.3

		balloonNode.addChild(balloon)
		balloon.zPosition = 0
		balloon.position = CGPoint(x:0,y:balloonHeight*0.75)
		balloon.size = CGSize(width:balloonHeight*15/9,height:balloonHeight)

		balloonNode.addChild(balloonText)
		balloonText.text = "obrigado!"
		balloonText.zPosition = 0.1
		balloonText.position = balloon.position
		balloonText.horizontalAlignmentMode = .center
		balloonText.verticalAlignmentMode = .baseline
		balloonText.fontColor = .black
		balloonText.fontSize = balloonHeight*0.3
	}

	override func show() {
		Audio.stopBgms()
		planet.texture = SKTexture(imageNamed:"contexts/"+LevelContext.current.name+"/planet")
		animationTime = 0
		fadeTime = 0
		playSfx = true
		fade.isHidden = false
		fade.alpha = 1
		spaceship.zRotation = TransitionScene.toGame ? spaceshipRotA : spaceshipRotB
		balloonNode.isHidden = !TransitionScene.thanks
	}

	override func update() {
		fadeTime += Time.deltaTime
		animationTime += Time.deltaTime
		if playSfx && fadeTime >= 1.5 {
			playSfx = false
			Audio.play(sfx:TransitionScene.toGame ? sfxToGame : sfxFromGame)
		}
		if fadeTime > 2 && fadeTime < 5.5 {
			for i in Input.touchEvents {
				if i.state == .press {
					fadeTime = 5.5
					break
				}
			}
		}
		let f = fadeTime
		if f >= 7 {
			if TransitionScene.toGame {
				if GameState.dialogueEntity != nil {
					DialogueScene.menuTransition = true
					SceneManager.show(SceneManager.dialogueScene)
				} else {
					RoomScene.menuTransition = true
					SceneManager.show(SceneManager.roomScene)
				}
			} else {
				SceneManager.show(SceneManager.mainScene)
			}
			return
		}
		if f < 1.5 {
			fade.isHidden = false
			if f <= 1 {
				fade.alpha = 1
			} else {
				fade.alpha = f.lerpInv(1.5,1).easeIn
			}
		} else if f > 5.5 {
			fade.isHidden = false
			if f >= 6 {
				fade.alpha = 1
			} else {
				fade.alpha = f.lerpInv(5.5,6).easeIn
			}
		} else {
			fade.isHidden = true
		}
		if f > 1 && f < 6 {
			let p = TransitionScene.toGame ? animationTime.lerpInv(1,6) : animationTime.lerpInv(6,1)
			let pos = p.lerp(backgroundOffset,-backgroundOffset)
			background.position = CGPoint(x:pos,y:0)
			planet.position = CGPoint(x:planetPos.x+pos*1.1,y:planetPos.y)
			if TransitionScene.thanks {
				balloonNode.position = CGPoint(x:planetPos.x+pos*1.25-balloonOffset,y:planetPos.y)
			}
			if p > 0.375 && p < 0.625 {
				spaceship.position = p.lerpInv(0.375,0.625).lerp(spaceshipPosA,spaceshipPosB)
				spaceship.isHidden = false
			} else {
				spaceship.isHidden = true
			}
		}
	}
}
