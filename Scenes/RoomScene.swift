import SpriteKit

/**
room scene, where entities wander around.
*/
class RoomScene:Scene {
	override class func create() -> Scene {
		return RoomScene(size:Window.sceneSize)
	}

	var tileScale:CGFloat {
		return Window.padMode ? 30 : 45
	}
	var currentTileScale:CGFloat = 30

	let fadeNode = SKSpriteNode()
	var leaveScene = false
	var transitionTempo:CGFloat = 0
	static var menuTransition = false

	let gameNavbarNode = GameNavbarNode()

	let roomNode = SKNode()
	let lavaNode = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "room/lava")))
	let lavaAlphaNode = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "room/lava")))
	var mapNode:TileMapNode!
	var roomIndex:Int = 0

	let playerNode = PlayerNode()
	var playerTouch:Int = -1
	let playerTouchNode = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "room/touch")))
	var playerTouchTempo:CGFloat = 0
	var playerTouchPos = CGPoint.zero

	let entityReactionNode = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "room/excl")))
	var entityNodes:[(node:EntityNode,data:EntityData,moveTempo:CGFloat)] = []
	var entityContact:Int = -1

	var roomTransitionTempo:CGFloat = 0
	var roomTransitionDirection:RoomDirection = .up

	let entityMinMoveTempo:CGFloat = 2
	let entityMaxMoveTempo:CGFloat = 2.5

	let bgmRoom = "bgm/room.mp3"
	let sfxEntityReaction = "room/entityReaction.wav"
	let sfxEntrance = "room/entrance.wav"
	let sfxTransition = "room/transition.wav"
	let sfxWalkEntity = "room/walkEntity.wav"
	let sfxWalkPlayer = "room/walkPlayer.wav"
	let sfxWalkSelect = "room/walkSelect.wav"

	override func loadResources() {
		Audio.load(batch:"room",files:[
			bgmRoom,
			sfxEntityReaction,
			sfxEntrance,
			sfxTransition,
			sfxWalkEntity,
			sfxWalkPlayer,
			sfxWalkSelect,
		])
	}

	override func unloadResources() {
		Audio.unload(batch:"room")
	}

	override func load() {
		backgroundColor = SKColor(colorLiteralRed:63/255,green:67/255,blue:80/255,alpha:1)

		addChild(fadeNode)
		fadeNode.zPosition = 110
		fadeNode.color = TransitionScene.fadeColour
		fadeNode.colorBlendFactor = 1
		fadeNode.position = CGPoint.zero
		fadeNode.size = Window.sceneSize

		addChild(gameNavbarNode)
		gameNavbarNode.zPosition = 50
		gameNavbarNode.position = CGPoint.zero
		buttons += gameNavbarNode.buttons

		addChild(roomNode)

		roomNode.addChild(playerTouchNode)
		playerTouchNode.zPosition = 1.5
		playerTouchNode.size = CGSize.one
		playerTouchNode.alpha = 0.25
		playerTouchNode.isHidden = true

		roomNode.addChild(lavaNode)
		lavaNode.zPosition = -2
		lavaNode.position = CGPoint.zero

		roomNode.addChild(lavaAlphaNode)
		lavaAlphaNode.zPosition = -1
		lavaAlphaNode.position = CGPoint.zero

		roomNode.addChild(playerNode)
		playerNode.zPosition = 4

		roomNode.addChild(entityReactionNode)
		entityReactionNode.zPosition = 5
		entityReactionNode.size = CGSize.one
	}

	override func show() {
		Audio.stopBgms()
		Audio.play(bgm:bgmRoom)
		Audio.play(sfx:sfxEntrance)

		leaveScene = false
		transitionTempo = 1

		gameNavbarNode.interactable = false

		entityReactionNode.isHidden = true

		playerTouch = -1
		playerTouchNode.isHidden = true
		playerNode.resetPosition()
		playerNode.updateZ()
		entityContact = -1

		loadRoom()
		playerNode.position = GameState.playerPos
		playerNode.resetPosition()
		playerNode.updateZ()
		sceneTransitioning()
	}

	override func update() {
		let lavaAnimScale:CGFloat = 0.4
		lavaNode.size = CGSize(
			width:CGFloat(mapNode.width-1)+sin(Time.time)*lavaAnimScale,
			height:CGFloat(mapNode.height-1)+cos(Time.time)*lavaAnimScale
		)
		lavaAlphaNode.size = CGSize(
			width:CGFloat(mapNode.width-1)+cos(Time.time*0.8)*lavaAnimScale,
			height:CGFloat(mapNode.height-1)-sin(Time.time*0.8)*lavaAnimScale
		)
		lavaAlphaNode.alpha = (sin(Time.time*2.1)*0.5+0.5)*0.8+0.1

		if leaveScene {
			gameNavbarNode.interactable = false
			if transitionTempo < 1 {
				transitionTempo += Time.deltaTime*0.75
				if transitionTempo >= 1 {
					transitionTempo = 1
					if RoomScene.menuTransition {
						TransitionScene.toGame = false
						TransitionScene.thanks = false
						SceneManager.show(SceneManager.transitionScene)
					} else {
						GameState.startDialogue()
						DialogueScene.menuTransition = false
						SceneManager.show(SceneManager.dialogueScene)
					}
				} else {
					sceneTransitioning()
				}
			}
		} else {
			if transitionTempo > 0 {
				gameNavbarNode.interactable = false
				transitionTempo -= Time.deltaTime*0.75
				if transitionTempo <= 0 {
					transitionTempo = 0
					RoomScene.menuTransition = false
					gameNavbarNode.interactable = true
				}
				sceneTransitioning()
			}
		}
		if roomTransitionTempo > 0 {
			let roomTransitionOne = roomTransitionTempo > 1
			roomTransitionTempo -= Time.deltaTime*1.5
			if roomTransitionOne && roomTransitionTempo <= 1 {
				GameState.moveToRoom(direction:roomTransitionDirection)
				loadRoom()
				let pos = roomTransitionDirection.float
				playerNode.position = CGPoint(x:-pos.x*(CGFloat(mapNode.width)/2-1.55),y:-pos.y*(CGFloat(mapNode.height)/2-1.55))
				playerNode.resetPosition()
				playerNode.updateZ()
				GameState.saveLevelState()
			}
			if roomTransitionTempo < 0 {
				roomTransitionTempo = 0
			}
			sceneTransitioning()
		}

		if transitionTempo > 0 {
			//
		} else {
			if roomTransitionTempo > 0 {
				if playerTouch >= 0 {
					for i in Input.touchEvents {
						if i.id == playerTouch && (i.state == .release || i.state == .miss) {
							playerTouch = -1
							break
						}
					}
				}
			} else if menusClosed {
				for i in Input.touchEvents {
					let pos = convert(i.position,to:roomNode)
					if playerTouch < 0 {
						if i.state == .press && !gameNavbarNode.isOverNavbar(pos:i.position) {
							playerTouch = i.id
							movePlayer(to:pos)
							Audio.play(sfx:sfxWalkSelect)
						}
					} else if i.id == playerTouch {
						movePlayer(to:pos)
						if i.state == .release || i.state == .miss {
							playerTouch = -1
						}
					}
				}

				if let door = playerNode.update(map:mapNode) {
					roomTransitionDirection = door
					roomTransitionTempo = 2
					Audio.play(sfx:sfxTransition)
				}
				GameState.playerPos = playerNode.position
			}

			for i in 0..<entityNodes.count {
				var entity = entityNodes[i]
				if roomIndex != GameState.level?.bossRoom {
					entity.moveTempo -= Time.deltaTime
					if entity.moveTempo <= 0 {
						entity.moveTempo = Random.range(min:entityMinMoveTempo,max:entityMaxMoveTempo)
						entity.node.randomPosition()
					}
				}
				_ = entity.node.update(map:mapNode)
				GameState.entities[roomIndex].positions[i] = entity.node.position
				entityNodes[i] = entity
			}

			if playerNode.moving && roomTransitionTempo < 1 {
				for i in 0..<entityNodes.count {
					if GameState.entities[roomIndex].dialogueState == .won {
						continue
					}
					let entity = entityNodes[i]
					let sqrRadius:CGFloat
					if GameState.entities[roomIndex].dialogueState == .none && entity.data.reactRadius > 0 {
						sqrRadius = entity.data.reactRadius*entity.data.reactRadius
					} else if entityContact == i {
						sqrRadius = 1
					} else {
						continue
					}
					let p = entity.node.position
					if playerNode.position.sqrDistance(to:p) <= sqrRadius {
						entityReactionNode.isHidden = false
						entityReactionNode.position = CGPoint(x:p.x,y:p.y+1.5)
						Audio.play(sfx:sfxEntityReaction)
						Audio.play(sfx:sfxTransition)
						endScene()
						break
					}
				}
			}
		}

		if roomTransitionTempo > 0 || (!playerNode.moving && playerTouch < 0) {
			playerTouchNode.isHidden = true
		} else {
			if playerTouch >= 0 {
				playerTouchTempo = 1
			} else if playerTouchTempo > 0 {
				playerTouchTempo -= Time.deltaTime*3
				if playerTouchTempo <= 0 {
					playerTouchTempo = 0
				}
			}
			playerTouchNode.isHidden = false
			playerTouchNode.position = playerTouchPos
			playerTouchNode.setScale(playerTouchTempo.ease.lerp(0.2,0.7))
		}

		if menusClosed {
			if gameNavbarNode.menuButton.released {
				RoomScene.menuTransition = true
				Audio.play(sfx:CommonAudio.sfxTransition)
				endScene()
			} else if gameNavbarNode.optionsButton.released {
				openSettings(fromMenu: false)
			} else if gameNavbarNode.manualButton.released {
				openManual(fromMenu: false)
			}
		}
	}

	func endScene() {
		leaveScene = true
		Audio.set(bgm:bgmRoom,volume:0,fade:0.075)
		GameState.saveLevelState()
	}

	func loadRoom() {
		guard let room = GameState.room else { return }
		roomIndex = room.index

		if mapNode != nil {
			mapNode.removeFromParent()
		}
		mapNode = TileMapNode(bg:room.bg,fg:room.fg)
		roomNode.addChild(mapNode)
		roomNode.position = CGPoint.zero
		mapNode.zPosition = 0

		for i in entityNodes {
			i.node.removeFromParent()
		}
		entityNodes.removeAll()
		if let entity = LevelContext.get(entity:room.entity) {
			let useContext:Bool
			let entityName:String
			switch room.entity {
			case .defaultEntity(let n):
				useContext = false
				entityName = n
			case .contextualEntity(let n):
				useContext = true
				entityName = n
			default:
				useContext = false
				entityName = ""
			}
			for i in 0..<entity.quantity {
				let entityNode = EntityNode(useContext:useContext,name:entityName,happy:GameState.entities[roomIndex].dialogueState == .won)
				roomNode.addChild(entityNode)
				entityNode.position = GameState.entities[roomIndex].positions[i]
				entityNode.resetPosition()
				entityNode.updateZ()
				let entityMoveTempo = Random.range(min:0,max:entityMaxMoveTempo)
				entityNodes.append((node:entityNode,data:entity,moveTempo:entityMoveTempo))
			}
		}

		currentTileScale = tileScale
		let nw = currentTileScale*CGFloat(room.width)
		let nh = currentTileScale*CGFloat(room.height)
		let mw = gameNavbarNode.size.width-10
		let mh = gameNavbarNode.size.height-10
		if nw > mw || nh > mh {
			let nratio = nw/nh
			let mratio = mw/mh
			if nratio > mratio {
				currentTileScale *= mw/nw
			} else {
				currentTileScale *= mh/nh
			}
		}

		lavaNode.isHidden = !room.transparent
		lavaAlphaNode.isHidden = !room.transparent
	}

	func movePlayer(to pos:CGPoint) {
		playerTouchPos = pos
		playerNode.setNewPosition(x:pos.x,y:pos.y)
		entityContact = -1
		for (n,i) in entityNodes.enumerated() {
			let p = i.node.position
			if pos.x >= p.x-0.5 && pos.x <= p.x+0.5 && pos.y >= p.y-0.5 && pos.y <= p.y+1 {
				entityContact = n
				break
			}
		}
	}

	func sceneTransitioning() {
		let dt = ((transitionTempo*2-1)*1.25).easeIn
		roomNode.setScale(dt.lerp(1,1.125)*currentTileScale)
		let move = roomTransitionDirection.float
		let moveDelta:CGFloat = 50
		let m:CGFloat
		let a:CGFloat
		if roomTransitionTempo <= 0 {
			m = 0
			a = 1
		} else if roomTransitionTempo < 1 {
			m = moveDelta*roomTransitionTempo.easeIn
			a = 1-roomTransitionTempo
		} else {
			m = -moveDelta*(2-roomTransitionTempo).easeIn
			a = roomTransitionTempo-1
		}
		roomNode.alpha = a.easeOut*(1-dt)
		roomNode.position = CGPoint(x:move.x*m+gameNavbarNode.centre.x,y:move.y*m+gameNavbarNode.centre.y)
		if RoomScene.menuTransition {
			if dt <= 0 {
				fadeNode.isHidden = true
			} else {
				fadeNode.alpha = dt
				fadeNode.isHidden = false
			}
		} else {
			fadeNode.isHidden = true
		}
	}
}
