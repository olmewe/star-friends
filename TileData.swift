import Foundation

public struct TilesData{

	public let typeCollision: String
	public let nameImg: String
	public let size: Int

	public init(tc: String, nameImg: String, size: Int) {
		typeCollision = tc
		self.nameImg = nameImg
		self.size = size
	}

}


extension TilesData: JSONDecodable{

	init(decoder: JSONDecoder) throws {
		self.nameImg = try decoder.decode(key: "nameImg")
		self.typeCollision = try decoder.decode(key: "typeCollision") //apos testar, passar essa variavel para o tipo de tileCollison
		self.size = try decoder.decode(key: "size")
	}

}
