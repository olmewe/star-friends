import SpriteKit

/**
level context class. provides contextual data for each level, like dialogues and tilesets.
*/
class LevelContext {
	var name:String = ""
	var tiles:[TileData] = []
	var entities:[String:EntityData] = [:]
	var defaultEntities:Int = 0
	var totalRooms:Int = 0

	func tileCollision(of index:Int) -> TileCollision {
		if index >= 0 && index < tiles.count {
			return tiles[index].collision
		}
		return .passthrough
	}
}

extension LevelContext {
	static private(set) var current:LevelContext = LevelContext()
	static private(set) var defaultEntities:[String:EntityData] = [:]
	static private(set) var nameList:[String] = []
	static private(set) var displayNameList:[String] = []

	/** loads default values, like default entities and context name list. */
	static func loadDefaults() {
		//FUTURE: pls load this kinda stuff from a json file
		defaultEntities = generateEntitiesTemp()
		nameList = ["vulcania"]
		displayNameList = ["vulcânia"]
	}

	/**
	loads static data from a given level and sets it as current.
	- parameter level: level context id
	*/
	static func load(level:String) {
		guard current.name != level else { return }
		if let lc = get(level:level) {
			current = lc
		}
	}

	/**
	loads and returns static data from a given level.
	- parameter level: level context id
	*/
	static func get(level:String) -> LevelContext? {
		//FUTURE: load level from json
		let lc = createTemp()
		lc.name = level
		return lc
	}

	/**
	retrieves entity data from a room entity instance.
	- parameter entity: room entity
	- returns: entity data from the default database, or from the current level, if it's a contextual one
	*/
	static func get(entity:RoomHasEntity?) -> EntityData? {
		guard let entity = entity else { return nil }
		switch entity {
		case .none: return nil
		case .defaultEntity(let id): return LevelContext.defaultEntities[id]
		case .contextualEntity(let id): return LevelContext.current.entities[id]
		}
	}
}

/**
information for each tile in a tileset.
*/
struct TileData {
	let texture:String
	let collision:TileCollision
	let size:CGSize

	init(_ texture:String,_ collision:TileCollision = .passthrough,_ width:CGFloat = 1,_ height:CGFloat = 1) {
		self.texture = texture
		self.collision = collision
		size = CGSize(width:width,height:height)
	}
}

/**
collision flags.
*/
enum TileCollision:Int {
	case passthrough
	case collide
	case door
}
