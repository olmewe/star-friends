import SpriteKit

extension LevelRoom {
	/**
	lays out all the tiles and whatnot.
	- parameter entityIndex: index of this room's entity, if any
	- parameter props: amount of props in this room
	*/
	func layout(entity:RoomHasEntity,level:Level) {
		self.entity = entity

		//aaa
		let w = width-1
		let h = height-1
		let doorX = width/2
		let doorY = height/2

		//floor and walls
		for x in 1..<w {
			for y in 1..<h {
				bg[y][x] = (x-1)+(y-1)*7+29
			}
		}
		for x in 0..<width {
			for y in 0..<height {
				fg[y][x] = -1
			}
		}
		for x in 1..<w {
			bg[0][x] = 0
			bg[h][x] = 0
		}
		for y in 1..<h {
			bg[y][0] = 1
			bg[y][w] = 1
		}
		bg[0][0] = 2
		bg[0][w] = 3
		bg[h][0] = 4
		bg[h][w] = 5

		//doors and windows
		let windowFactor:CGFloat = 0.7
		if doors[.down] != nil {
			bg[0][doorX] = 6
			if Random.value < windowFactor { bg[0][doorX-1] = 10 }
			if Random.value < windowFactor { bg[0][doorX+1] = 11 }
		}
		if doors[.up] != nil {
			bg[h][doorX] = 7
			if Random.value < windowFactor { bg[h][doorX-1] = 12 }
			if Random.value < windowFactor { bg[h][doorX+1] = 13 }
		}
		if doors[.left] != nil {
			bg[doorY][0] = 8
			if Random.value < windowFactor { bg[doorY-1][0] = 14 }
			if Random.value < windowFactor { bg[doorY+1][0] = 15 }
		}
		if doors[.right] != nil {
			bg[doorY][w] = 9
			if Random.value < windowFactor { bg[doorY-1][w] = 16 }
			if Random.value < windowFactor { bg[doorY+1][w] = 17 }
		}

		//lava floor
		switch index {
		case level.preBossRoom:
			for y in 1..<h {
				bg[y][1] = 28
				bg[y][w-1] = 28
			}
			break
		case level.bossRoom:
			for y in 1..<h {
				bg[y][1] = 28
				bg[y][w-1] = 28
				if Random.bool { bg[y][2] = 28 }
				if Random.bool { bg[y][w-2] = 28 }
			}
			for x in 1..<w {
				bg[h-1][x] = 28
				if Random.bool { bg[h-2][x] = 28 }
				if Random.bool { bg[h-3][x] = 28 }
			}
			break
		default:
			break
		}

		//adjust floor tiles
		transparent = false
		for x in 1..<w {
			for y in 1..<h {
				if bg[y][x] == 28 || bg[y][x] == 27 {
					transparent = true
					if bg[y+1][x] != 28 && bg[y+1][x] != 27 {
						bg[y][x] = 27
					}
				}
			}
		}
	}
}
