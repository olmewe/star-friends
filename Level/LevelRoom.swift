import SpriteKit

/**
class for level room. stores the room tilemap, doors, entities...
*/
class LevelRoom:NSObject,NSCoding {
	let index:Int
	let width:Int
	let height:Int
	var bg:[[Int]]
	var fg:[[Int]]
	var doors:[RoomDirection:Int]
	var roomX:Int
	var roomY:Int
	var transparent:Bool
	var entity:RoomHasEntity

	/**
	creates a level room.
	- parameter index: room index in its parent level
	- parameter width: room width
	- parameter width: room height
	*/
	init(index:Int,width:Int,height:Int) {
		self.index = index
		self.width = 1+width*2
		self.height = 1+height*2
		bg = Array(repeating:Array(repeating:-1,count:self.width),count:self.height)
		fg = Array(repeating:Array(repeating:-1,count:self.width),count:self.height)
		doors = [:]
		roomX = 0
		roomY = 0
		transparent = false
		entity = .none
	}

	required init?(coder c:NSCoder) {
		let index = c.decodeInteger(forKey:"index")
		let width = c.decodeInteger(forKey:"width")
		let height = c.decodeInteger(forKey:"height")
		let roomX = c.decodeInteger(forKey:"roomX")
		let roomY = c.decodeInteger(forKey:"roomY")
		let transparent = c.decodeBool(forKey:"transparent")
		guard
			let bgFlat = c.decodeObject(forKey:"bg") as? [Int],
			let fgFlat = c.decodeObject(forKey:"fg") as? [Int]
			else { return nil }
		guard index >= 0 && width > 0 && height > 0 else { return nil }
		guard bgFlat.count == width*height && bgFlat.count == fgFlat.count else { return nil }
		self.index = index
		self.width = width
		self.height = height
		self.bg = []
		self.fg = []
		for i in 0..<height {
			var b:[Int] = []
			var f:[Int] = []
			for i in i*width..<(i+1)*width {
				b.append(bgFlat[i])
				f.append(bgFlat[i])
			}
			bg.append(b)
			fg.append(f)
		}
		var doorsN:[RoomDirection:Int] = [:]
		func check(door:RoomDirection) {
			let key = "door\(door.rawValue)"
			if c.containsValue(forKey:key) {
				doorsN[door] = c.decodeInteger(forKey:key)
			}
		}
		check(door:.up)
		check(door:.down)
		check(door:.left)
		check(door:.right)
		doors = doorsN
		self.roomX = roomX
		self.roomY = roomY
		self.transparent = transparent
		if let entityName = c.decodeObject(forKey:"entityName") as? String {
			if c.containsValue(forKey:"entityContextual") && c.decodeBool(forKey:"entityContextual") {
				entity = .contextualEntity(entityName)
			} else {
				entity = .defaultEntity(entityName)
			}
		} else {
			entity = .none
		}
	}

	func encode(with c:NSCoder) {
		c.encode(index,forKey:"index")
		c.encode(width,forKey:"width")
		c.encode(height,forKey:"height")
		var l:[Int]
		l = []
		for i in bg {
			l += i
		}
		c.encode(l,forKey:"bg")
		l = []
		for i in fg {
			l += i
		}
		c.encode(l,forKey:"fg")
		for (k,v) in doors {
			c.encode(v,forKey:"door\(k.rawValue)")
		}
		c.encode(roomX,forKey:"roomX")
		c.encode(roomY,forKey:"roomY")
		c.encode(transparent,forKey:"transparent")
		switch entity {
		case .contextualEntity(let s):
			c.encode(true,forKey:"entityContextual")
			c.encode(s,forKey:"entityName")
		case .defaultEntity(let s):
			c.encode(s,forKey:"entityName")
		case .none: break
		}
	}
}

/**
direction of a door in a room or something like that.
*/
enum RoomDirection:Int {
	case up
	case down
	case left
	case right

	/**
	this direction in coord (int) form.
	*/
	var int:(x:Int,y:Int) {
		get {
			switch self {
			case .up: return (x:0,y:1)
			case .down: return (x:0,y:-1)
			case .left: return (x:-1,y:0)
			case .right: return (x:1,y:0)
			}
		}
	}

	/**
	this direction in coord (cgfloat) form.
	*/
	var float:(x:CGFloat,y:CGFloat) {
		get {
			switch self {
			case .up: return (x:0,y:1)
			case .down: return (x:0,y:-1)
			case .left: return (x:-1,y:0)
			case .right: return (x:1,y:0)
			}
		}
	}

	/**
	the opposite direction.
	*/
	var opposite:RoomDirection {
		get {
			switch self {
			case .up: return .down
			case .down: return .up
			case .left: return .right
			case .right: return .left
			}
		}
	}
}

/**
entity reference in a level room.
*/
enum RoomHasEntity {
	case none
	case defaultEntity(String)
	case contextualEntity(String)

	var isContextual:Bool {
		switch self {
		case .contextualEntity(_): return true
		default: return false
		}
	}

	var string:String {
		switch self {
		case .contextualEntity(let s): return s
		case .defaultEntity(let s): return s
		default: return ""
		}
	}
}
