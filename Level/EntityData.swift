import SpriteKit

/**
info about entities and how their dialogue goes as well.
*/
class EntityData {
	var name:String = ""
	var quantity:Int = 1
	var reactRadius:CGFloat = 0
	var clue:Int = -1
	var boss:Bool = false

	var startDisposition:CGFloat = 0.5
	var rightAnswerDisposition:CGFloat = 0.05
	var wrongAnswerDisposition:CGFloat = -0.15
	var battleDispositionPerSecond:CGFloat = 0.02
	var battleDispositionPerHit:CGFloat = 0.05
	var battleHealthPerDamage:CGFloat = -0.1

	var dialogues:[DialogueBlockType:DialogueBlock] = [:]
	var dialogueCycles:[DialogueCycle] = []
}

/**
different dialogue contexts.
*/
enum DialogueBlockType:Int {
	//unique dialogues
	case intro
	case beforeBattle
	case afterBattle
	case sadEnding
	case sadIntro

	//per cycle dialogues
	case beforeOptions
	case options
	case answer

	//not a dialogue block!
	case battle

	/** is this a per cycle dialogue? */
	var isPerCycleDialogue:Bool {
		switch self {
		case .beforeOptions,.options,.answer: return true
		default: return false
		}
	}
}

/**
class for dialogue cycles.
*/
class DialogueCycle {
	var beforeOptions:DialogueBlock = DialogueBlock()
	var options:[DialogueOption] = []
	var answer:[DialogueBlock] = []
}

/**
class for dialogue options, which are kinda like dialogue lines but a bit different.
*/
class DialogueOption {
	var clue:Int
	var rightAnswer:Bool
	var text:String

	init(clue:Int,rightAnswer:Bool,text:String) {
		self.clue = clue
		self.rightAnswer = rightAnswer
		self.text = text
	}

	init(_ text:String) {
		self.clue = -1
		self.rightAnswer = false
		self.text = text
	}

	init(_ clue:Int,_ text:String) {
		self.clue = clue
		self.rightAnswer = true
		self.text = text
	}
}

/**
class for dialogue blocks, which are essentially lists of dialogue lines.
*/
class DialogueBlock {
	var lines:[DialogueLine]

	init() {
		lines = []
	}

	init(lines l:[(Int,Int,String)]) {
		lines = []
		for i in l {
			lines.append(DialogueLine(speaker:i.0,avatar:i.1,text:i.2))
		}
	}
}

/**
class for dialogue lines, which are essentially a line of text and who says that.
*/
struct DialogueLine {
	let playerSpeaks:Bool
	let avatar:Int
	let text:String

	init(speaker:Int,avatar:Int,text:String) {
		self.playerSpeaks = speaker == 0
		self.avatar = avatar
		self.text = text
	}
}
