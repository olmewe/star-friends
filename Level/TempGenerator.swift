import SpriteKit

extension LevelContext {
	/** TEMP: creates temporary entity data. */
	static func generateEntitiesTemp() -> [String:EntityData] {
		var entities:[String:EntityData] = [:]
		for i in 0..<10 {
			let entity = EntityData()
			entity.name = i.description
			entity.quantity = 1
			entities[entity.name] = entity
			entity.startDisposition = 0.1
			entity.generateDialoguesTemp()
		}
		return entities
	}
}

extension LevelContext {
	/** TEMP: creates temporary contextual data. */
	static func createTemp() -> LevelContext {
		let lc = LevelContext()
		lc.tiles = [
			TileData("wallH",.collide),
			TileData("wallV",.collide),

			TileData("corner",.collide,+1,-1),
			TileData("corner",.collide,-1,-1),
			TileData("corner",.collide,+1,+1),
			TileData("corner",.collide,-1,+1),

			TileData("doorH",.door,+1,-1),
			TileData("doorH",.door,+1,+1),
			TileData("doorV",.door,+1,-1),
			TileData("doorV",.door,-1,-1),

			TileData("windowH",.collide,+1,-1),
			TileData("windowH",.collide,-1,-1),
			TileData("windowH",.collide,+1,+1),
			TileData("windowH",.collide,-1,+1),
			TileData("windowV",.collide,+1,+1),
			TileData("windowV",.collide,+1,-1),
			TileData("windowV",.collide,-1,+1),
			TileData("windowV",.collide,-1,-1),

			TileData("wardrobeH",.collide,+1.5,-1.5),
			TileData("wardrobeH",.collide,-1.5,-1.5),
			TileData("wardrobeH",.collide,+1.5,+1.5),
			TileData("wardrobeH",.collide,-1.5,+1.5),
			TileData("wardrobeV",.collide,+1.5,+1.5),
			TileData("wardrobeV",.collide,+1.5,-1.5),
			TileData("wardrobeV",.collide,-1.5,+1.5),
			TileData("wardrobeV",.collide,-1.5,-1.5),

			TileData("table",.collide),

			TileData("blankshadow",.collide),
			TileData("blank",.collide),
		]
		for i in 0..<(7*9) {
			lc.tiles.append(TileData(i.description))
		}
		let entities = [
			"boss",
			"fume",
			"rock",
		]
		for (n,i) in entities.enumerated() {
			let entity = EntityData()
			entity.name = i
			entity.quantity = 1
			lc.entities[entity.name] = entity
			entity.startDisposition = 0.1
			entity.boss = n == 0
			entity.generateTemp(entity.name)
		}
		lc.defaultEntities = 0//5
		lc.totalRooms = 2//9
		return lc
	}
}

//TEMP: creating temporary data. move this to json later!
extension EntityData {
	func generateTemp(_ name:String) {
		switch name {
		case "boss": generateBossTemp()
		case "fume": generateFumeTemp()
		case "rock": generateRockTemp()
		default: return
		}
	}

	func generateBossTemp() {
		startDisposition = 0.1
		wrongAnswerDisposition = -0.3
		dialogues[.intro] = DialogueBlock(lines:[
			(0,0,"Oi, desculpa incomodar…"),
			(1,1,"^s ARRGR… ^n ^t SAI DAQUI"),
		])
		for _ in 0..<5 {
			dialogueCycles.append(DialogueCycle())
		}
		dialogueCycles[0].beforeOptions = DialogueBlock(lines:[
			(1,0,"NÃO TÁ ME OUVINDO? ^t EU DISSE PRA IR EMBORA! ^t ^s AAA… ^n ^t QUE DROGA! ^t EU ODEIO ME SENTIR ASSIM!"),
		])
		dialogueCycles[0].options = [
			DialogueOption(-1,"Não tem problema se sentir assim…"),
			DialogueOption("Nossa, calma, tenta relaxar…"),
			DialogueOption("Não precisa descontar em mim."),
		]
		dialogueCycles[0].answer = [
			DialogueBlock(lines:[
				(0,1,"Ei, fica tranquilo, não tem problema você se sentir desse jeito…"),
				(1,2,"Aaaa.. Sei lá. ^t Tem horas que eu só quero que tudo exploda mesmo."),
			]),
			DialogueBlock(lines:[
				(0,1,"Nossa, calma, tenta relaxar…"),
				(1,0,"^s S ^t A ^t I ^t D ^t A ^t Q ^t U ^t I! ^t ^n ENTENDEU AGORA?"),
			]),
			DialogueBlock(lines:[
				(0,0,"Poxa… Mas não precisa descontar em mim também."),
				(1,0,"^s S ^t A ^t I ^t D ^t A ^t Q ^t U ^t I! ^t ^n ENTENDEU AGORA?"),
			]),
		]
		dialogueCycles[1].options = [
			DialogueOption(-1,"Eu também fico assim às vezes."),
			DialogueOption("Isso não faz bem pra ninguém."),
			DialogueOption("Você precisa se controlar…"),
		]
		dialogueCycles[1].answer = [
			DialogueBlock(lines:[
				(0,1,"Eu também fico assim às vezes. Aí eu tento me concentrar em uma coisa só e esvaziar a cabeça."),
				(1,1,"^w Grr… ^n ^t Tá, fica quieto um pouco então………"),
				(1,2,"Tá, ok, vai ficar tudo bem. ^t Sei lá, eu fico tão frustrada quando penso na vida."),
				(1,3,"Parece que eu nunca vou ter tempo pra fazer o que eu gosto."),
			]),
			DialogueBlock(lines:[
				(0,1,"Isso não faz bem nem pra você, nem pra ninguém."),
				(1,1,"Pfft… Ah, tá."),
			]),
			DialogueBlock(lines:[
				(0,0,"Você precisa aprender a se controlar…"),
				(1,1,"Pfft… Ah, tá."),
			]),
		]
		dialogueCycles[2].options = [
			DialogueOption(1,"Eu sei que você se sente insegura…"),
			DialogueOption("Você vai encontrar outra coisa…"),
			DialogueOption("Não dá tempo de fazer tudo…"),
		]
		dialogueCycles[2].answer = [
			DialogueBlock(lines:[
				(0,0,"Eu sei que você se sente insegura, mas não dá pra controlar tudo."),
				(0,1,"Tenta não sofrer por antecipação, as coisas sempre acabam bem."),
				(1,3,"AAAA, eu não sei por que eu faço isso comigo mesma."),
				(1,3,"Mas você tem razão. ^t É só que eu tenho tanto medo do que vão dizer, que nunca mostro o que faço."),
			]),
			DialogueBlock(lines:[
				(0,1,"Eu sei que não é fácil, mas tem coisa que não é pra ser mesmo. Você vai encontrar outra coisa pra fazer."),
				(1,1,"É, que ótimo…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Não dá tempo de fazer tudo, você precisa escolher um caminho e se comprometer com ele."),
				(1,1,"É, que ótimo…"),
			]),
		]
		dialogueCycles[3].options = [
			DialogueOption(0,"Você deveria dar uma chance."),
			DialogueOption("O importante é ficar bem."),
			DialogueOption("Você não devia se importar tanto."),
		]
		dialogueCycles[3].answer = [
			DialogueBlock(lines:[
				(0,1,"Acho que se é uma coisa pela qual você é apaixonada, com certeza as pessoas vão se interessar também."),
				(1,3,"Acho que não custa tentar, né…"),
			]),
			DialogueBlock(lines:[
				(0,1,"Não tem problema você guardar essas coisas para você, sabe? O importante é você estar confortável."),
				(1,2,"Pode ser…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Você não devia se importar tanto assim com a opinião dos outros."),
				(1,2,"Pode ser…"),
			]),
		]
		dialogueCycles[4].options = [
			DialogueOption(-1,"Às vezes é bom conversar, sabe?"),
			DialogueOption("Espero que dê tudo certo."),
			DialogueOption("Isso aí. Força."),
		]
		dialogueCycles[4].answer = [
			DialogueBlock(lines:[
				(0,1,"Às vezes é bom conversar, sabe? É importante você entender o que te faz mal. Se precisar pode contar comigo."),
				(1,4,"Ei, obrigada. Você me ajudou bastante."),
				(1,4,"Acho que agora eu estou me sentindo bem de novo."),
				(1,4,"E desculpa, sabe, por antes. ^t Acho que eu fui bem grosseira com você. É só, difícil, sabe? ^t Frustrante."),
				(1,4,"E eu sei que não lido bem com isso e que não é culpa de ninguém. ^t Mas, enfim, né. Eu to tentando."),
			]),
			DialogueBlock(lines:[
				(0,1,"Que bom. Espero que dê tudo certo. É muito difícil passar por esse tipo de coisa, né?"),
				(1,2,"Sim, sim… É, obrigada eu acho…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Isso aí. Força. O importante é seguir em frente."),
				(1,2,"Sim, sim… É, obrigada eu acho…"),
			]),
		]
		dialogues[.beforeBattle] = DialogueBlock(lines:[
			(1,0,"^s AAARGG! ^n VOCÊ NÃO ENTENDE!")
		])
		dialogues[.afterBattle] = DialogueBlock(lines:[
			(1,1,"Tá, tá. Entendi. Você quer conversar, né? Então vamos.")
		])
		dialogues[.sadEnding] = DialogueBlock(lines:[
			(1,0,"CARA!! ^s SOME!!!")
		])
		dialogues[.sadIntro] = DialogueBlock(lines:[
			(1,0,"^s AAARGG! ^n VOCÊ NÃO ENTENDE!")
		])
	}

	func generateFumeTemp() {
		clue = 0
		startDisposition = 0.3
		dialogues[.intro] = DialogueBlock(lines:[
			(0,0,"Oi, tudo bem?"),
			(1,1,"Oi… hm…"),
		])
		for _ in 0..<3 {
			dialogueCycles.append(DialogueCycle())
		}
		dialogueCycles[0].beforeOptions = DialogueBlock(lines:[
			(1,1,"Na verdade, não sei. ^t Tô mal, eu acho. Desculpa. ^t Eu não sei o que é, só me deixa em paz, por favor."),
		])
		dialogueCycles[0].options = [
			DialogueOption(-1,"Tudo bem se você não quiser…"),
			DialogueOption("Ah, deixa disso, fala logo…"),
			DialogueOption("Quê? Desculpa?"),
		]
		dialogueCycles[0].answer = [
			DialogueBlock(lines:[
				(0,1,"Tudo bem se você não quiser conversar, mas eu tô aqui se você precisar…"),
				(1,2,"Ah, sei lá. É que às vezes eu fico meio angustiado e parece que eu não consigo mais pensar direito."),
			]),
			DialogueBlock(lines:[
				(0,1,"Ah, deixa disso, fala logo…"),
				(1,1,"Olha, eu realmente não tô a fim…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Quê? Desculpa, eu fiz alguma coisa errada?"),
				(1,1,"Olha, eu realmente não tô a fim…"),
			]),
		]
		dialogueCycles[1].options = [
			DialogueOption(-1,"Quando eu fico assim, eu…"),
			DialogueOption("Ah, sei lá, tenta se distrair…"),
			DialogueOption("Putz, que ruim. Fica bem…"),
		]
		dialogueCycles[1].answer = [
			DialogueBlock(lines:[
				(0,1,"Quando eu fico assim, eu fecho os olhos e respiro bem fundo."),
				(1,2,"Vou tentar, calma aí………"),
				(1,3,"Ok, acho que eu to um pouco melhor."),
				(1,2,"É que aqui é tão barulhento, me deixa meio nervoso."),
			]),
			DialogueBlock(lines:[
				(0,1,"Ah, sei lá, tenta se distrair…"),
				(1,1,"É, tá bom…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Putz, que ruim. Fica bem…"),
				(1,1,"É, tá bom…"),
			]),
		]
		dialogueCycles[2].options = [
			DialogueOption(-1,"Esquece o som da lava…"),
			DialogueOption("Melhor você sair daqui então."),
			DialogueOption("Por que você não tenta ignorar?"),
		]
		dialogueCycles[2].answer = [
			DialogueBlock(lines:[
				(0,1,"Olha, mas tenta prestar atenção… Esquece o som da lava, tá ouvindo a música?"),
				(1,2,"Não… ^p Calma, acho que sim. ^p É verdade. Poxa, é bem bonito."),
				(1,3,"Sabe, acho que eu to me sentindo um pouco melhor. Valeu pela ajuda."),
				(1,3,"Hoje foi um dia ruim, minha amiga tá muito nervosa. Ela não é assim, sabe? Não sei o que aconteceu."),
				(1,2,"Outro dia a gente estava conversando sobre o que ela queria fazer da vida."),
				(1,1,"Ela disse que ^1 queria que as pessoas gostassem dos desenhos que ela faz."),
				(1,2,"Acho que foi a última vez que eu vi ela bem. Mas também, ela nunca quer mostrar eles pra ninguém…"),
				(1,4,"Enfim, sei lá. Foi bom falar com você."),
			]),
			DialogueBlock(lines:[
				(0,1,"Hm. Melhor você sair daqui então, né?"),
				(1,2,"Ah, quem dera fosse tão simples assim…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Por que você não tenta ignorar?"),
				(1,2,"Ah, quem dera fosse tão simples assim…"),
			]),
		]
		dialogues[.beforeBattle] = DialogueBlock(lines:[
			(1,0,"Você nem me conhece, não tem nada melhor pra fazer não?")
		])
		dialogues[.afterBattle] = DialogueBlock(lines:[
			(1,1,"Pensando bem acho que eu preciso falar com alguém mesmo…")
		])
		dialogues[.sadEnding] = DialogueBlock(lines:[
			(1,0,"Olha, sério, me deixa em paz, vai…")
		])
		dialogues[.sadIntro] = DialogueBlock(lines:[
			(1,0,"Você nem me conhece, não tem nada melhor pra fazer não?")
		])
	}

	func generateRockTemp() {
		clue = 1
		startDisposition = 0.3
		dialogues[.intro] = DialogueBlock(lines:[
			(0,0,"Oi, tudo bem?"),
			(1,1,"Ah, oi."),
		])
		for _ in 0..<3 {
			dialogueCycles.append(DialogueCycle())
		}
		dialogueCycles[0].beforeOptions = DialogueBlock(lines:[
			(1,1,"Não precisa se preocupar. Eu só tô a fim de ficar um pouco sozinho…"),
		])
		dialogueCycles[0].options = [
			DialogueOption(-1,"Ah, tá bom… Mas se precisar…"),
			DialogueOption("Deixa disso, para de ser bobo…"),
			DialogueOption("Não precisa ser grosseiro também."),
		]
		dialogueCycles[0].answer = [
			DialogueBlock(lines:[
				(0,1,"Ah, tá bom… Mas se precisar de alguém eu tô aqui, tá?"),
				(1,2,"Aaa. Desculpa. É que eu tô me sentindo meio cansado. ^t Exausto, na verdade… hahaha."),
			]),
			DialogueBlock(lines:[
				(0,1,"Deixa disso, para de ser bobo…"),
				(1,1,"Foi mal, eu só preciso ficar em silêncio um pouco."),
			]),
			DialogueBlock(lines:[
				(0,0,"Aff… Não precisa ser grosseiro também."),
				(1,1,"Foi mal, eu só preciso ficar em silêncio um pouco."),
			]),
		]
		dialogueCycles[1].options = [
			DialogueOption(-1,"*sentar em silêncio*"),
			DialogueOption("*sentar e chamar a atenção*"),
			DialogueOption("*sentar e puxar papo*"),
		]
		dialogueCycles[1].answer = [
			DialogueBlock(lines:[
				(0,1,"*senta ao lado em silêncio*"),
				(1,2,"^w *suspiro cansado* ^n ^t Toda vez que eu tenho que lidar com outras pessoas eu acabo assim."),
			]),
			DialogueBlock(lines:[
				(0,1,"*senta ao lado e tenta chamar a atenção*"),
				(1,1,"Ai, cara, me dá um sossego…"),
			]),
			DialogueBlock(lines:[
				(0,0,"*senta ao lado e tenta puxar papo*"),
				(1,1,"Ai, cara, me dá um sossego…"),
			]),
		]
		dialogueCycles[2].options = [
			DialogueOption(-1,"Eu sei como é, também fico."),
			DialogueOption("Não tem como evitar…"),
			DialogueOption("Se eles não te fazem bem…"),
		]
		dialogueCycles[2].answer = [
			DialogueBlock(lines:[
				(0,1,"Eu sei como é, também fico. Você tem que tentar equilibrar as coisas."),
				(0,0,"É importante manter os amigos, né? ^t Faz bem ter com quem dividir as coisas."),
				(1,2,"Mas você está certo. Eu sei que eles não tem culpa. ^t Eu sei que eles gostam de mim e querem o meu bem."),
				(1,3,"Talvez se eu falar como eu me sinto para eles, eles entendam, né?"),
				(1,2,"Eu não estava com muita paciência hoje, já passei boa parte do dia preocupado com minha amiga."),
				(1,1,"Ela é tão ^1 insegura!"),
				(1,2,"E eu não entendo o motivo, ela é muito talentosa. Sei lá."),
				(1,2,"Quando ela fica mal ela tenta controlar tudo e quando percebe que não dá fica pior ainda."),
				(1,3,"Ai, ai."),
				(1,4,"Obrigado por me fazer companhia. A gente se vê por aí."),
			]),
			DialogueBlock(lines:[
				(0,1,"Não tem jeito, não tem como ser evitar as pessoas para sempre…"),
				(1,2,"Acho que você tem razão…"),
			]),
			DialogueBlock(lines:[
				(0,0,"Sei lá, se eles não te fazem bem talvez não sejam seus amigos de verdade."),
				(1,2,"Acho que você tem razão…"),
			]),
		]
		dialogues[.beforeBattle] = DialogueBlock(lines:[
			(1,0,"Af, para de ser chato. Eu já pedi para me deixar em paz.")
		])
		dialogues[.afterBattle] = DialogueBlock(lines:[
			(1,1,"Acho que tudo bem se você ficar por aqui um pouco mais…")
		])
		dialogues[.sadEnding] = DialogueBlock(lines:[
			(1,0,"Aaa, que droga! O que eu tenho que fazer para poder ficar em silêncio por aqui?")
		])
		dialogues[.sadIntro] = DialogueBlock(lines:[
			(1,0,"Af, para de ser chato. Eu já pedi para me deixar em paz.")
		])
	}

	func generateDialoguesTemp() {
		/*
		dialogues[.intro] = DialogueBlock(lines:[
			(0,0,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac nisi sed libero tincidunt vulputate eu ac augue."),
			(1,0,"ow dae"),
		])
		dialogues[.beforeBattle] = DialogueBlock(lines:[
			(1,0,"n quero mais n"),
		])
		dialogues[.afterBattle] = DialogueBlock(lines:[
			(1,0,"ta ok vamo"),
		])
		dialogues[.sadEnding] = DialogueBlock(lines:[
			(1,0,"me deixa pls"),
		])
		dialogues[.sadIntro] = DialogueBlock(lines:[
			(1,0,"nem vem"),
		])
		for i in 1...3 {
			var cycle:[DialogueBlockType:DialogueBlock] = [:]
			cycle[.beforeOptions] = DialogueBlock(lines:[
				(0,0,"ciclo #\(i)!"),
				(1,0,"mto legal"),
			])
			cycle[.options] = DialogueBlock(lines:[
				(-2,0,"resposta errada"),
				(-1,0,"resposta certa"),
				(-2,0,"resposta meio ruim tb"),
			])
			if i < 4 {
				cycle[.goodAnswer] = DialogueBlock(lines:[
					(1,0,"ai sim lek"),
				])
			} else {
				cycle[.goodAnswer] = DialogueBlock(lines:[
					(1,0,"acabou entao"),
					(1,0,"flw"),
					(0,0,"flw"),
				])
			}
			cycle[.badAnswer] = DialogueBlock(lines:[
				(1,0,"oq"),
			])
			dialogueCycles.append(cycle)
		}
		*/
	}
}
