import SpriteKit

/**
class for levels. stores rooms and whatnot.
*/
class Level:NSObject,NSCoding {
	var rooms:[LevelRoom] = []
	var startRoom:Int = 0
	var preBossRoom:Int = 0
	var bossRoom:Int = 0
	var context:String = ""

	override init() {
		super.init()
	}

	required init?(coder c:NSCoder) {
		super.init()
		let startRoom = c.decodeInteger(forKey:"startRoom")
		let preBossRoom = c.decodeInteger(forKey:"preBossRoom")
		let bossRoom = c.decodeInteger(forKey:"bossRoom")
		guard let context = c.decodeObject(forKey:"context") as? String else { return nil }
		if startRoom < 0 || preBossRoom < 0 || bossRoom < 0 {
			return nil
		}
		if startRoom == preBossRoom || startRoom == bossRoom || preBossRoom == bossRoom {
			return nil
		}
		while let roomData = c.decodeObject(forKey:"room\(rooms.count)") as? Data {
			if let room = NSKeyedUnarchiver.unarchiveObject(with:roomData) as? LevelRoom {
				rooms.append(room)
			} else {
				break
			}
		}
		if startRoom >= rooms.count || preBossRoom >= rooms.count || bossRoom >= rooms.count {
			return nil
		}
		self.startRoom = startRoom
		self.preBossRoom = preBossRoom
		self.bossRoom = bossRoom
		self.context = context
	}

	func encode(with c:NSCoder) {
		c.encode(startRoom,forKey:"startRoom")
		c.encode(preBossRoom,forKey:"preBossRoom")
		c.encode(bossRoom,forKey:"bossRoom")
		c.encode(context,forKey:"context")
		for (n,room) in rooms.enumerated() {
			c.encode(NSKeyedArchiver.archivedData(withRootObject:room),forKey:"room\(n)")
		}
	}
}
