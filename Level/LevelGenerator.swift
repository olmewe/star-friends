import SpriteKit

extension Level {
	/**
	generates a random level.
	- parameter context: level context id
	- parameter rooms: number of rooms
	- returns: random generated level
	*/
	static func generate(context:String,rooms:Int) -> Level {
		let level = Level()

		//sets context
		level.context = context
		LevelContext.load(level:context)
		let rooms = max(rooms,LevelContext.current.entities.count)

		//figures out its height and whatnot
		let roomsRoot = sqrt(Double(rooms))
		let height = max(1,Random.range(min:Int(roomsRoot),max:Int(roomsRoot*1.5)+1))
		let map = Map(rooms:rooms,height:height)

		//craetes all room instances
		for i in 0..<rooms {
			let room = LevelRoom(index:i,width:Random.range(min:3,max:5),height:Random.range(min:3,max:5))
			level.rooms.append(room)
		}
		for y in 0..<map.height {
			for x in map.a..<map.b {
				if let i = map[x,y] {
					let room = level.rooms[i]
					room.roomX = x
					room.roomY = y
				}
			}
		}

		//set room indexes
		let firstRoom:Int = 0
		let lastRoom = height-1

		//create default rooms
		level.startRoom = level.rooms.count
		level.rooms.append(LevelRoom(index:level.startRoom,width:2,height:2))
		level.preBossRoom = level.rooms.count
		level.rooms.append(LevelRoom(index:level.preBossRoom,width:3,height:5))
		level.bossRoom = level.rooms.count
		level.rooms.append(LevelRoom(index:level.bossRoom,width:4,height:5))

		/**
		connects two rooms, given their indexes.
		- parameter a: starting room
		- parameter b: ending room
		- parameter direction: direction in which the *a* room is connected to *b*
		*/
		func connectRooms(from a:Int,to b:Int,going direction:RoomDirection) {
			level.rooms[a].doors[direction] = b
			level.rooms[b].doors[direction.opposite] = a
		}

		//start connection queue and state list
		var queue:[Int] = [firstRoom]
		var connection:[RoomConnectionState] = Array(repeating:.disconnected,count:rooms)
		connection[firstRoom] = .queued

		//connect default rooms
		connectRooms(from:level.startRoom,to:firstRoom,going:.up)
		connectRooms(from:lastRoom,to:level.preBossRoom,going:.up)
		connectRooms(from:level.preBossRoom,to:level.bossRoom,going:.up)

		/**
		marks a given room as connected, and its neighbours as. uh. neighbours.
		- parameter index: room index
		*/
		func markConnected(at index:Int) {
			connection[index] = .connected
			let r = level.rooms[index]
			if let i = map[r.roomX,r.roomY-1] {
				if connection[i] == .disconnected {
					connection[i] = .neighbour
				}
			}
			if let i = map[r.roomX,r.roomY+1] {
				if connection[i] == .disconnected {
					connection[i] = .neighbour
				}
			}
			if let i = map[r.roomX-1,r.roomY] {
				if connection[i] == .disconnected {
					connection[i] = .neighbour
				}
			}
			if let i = map[r.roomX+1,r.roomY] {
				if connection[i] == .disconnected {
					connection[i] = .neighbour
				}
			}
		}

		//connection loop
		for _ in 0..<rooms {

			//try to find a room to connect.
			//if queue is empty, grab a neighbour! also set force connect to true.
			let next:LevelRoom
			let forceConnect:Bool
			if let n = queue.first {
				queue.removeFirst()
				next = level.rooms[n]
				forceConnect = false
			} else {
				var found:Int?
				for (n,i) in connection.enumerated() {
					if i == .neighbour {
						found = n
						break
					}
				}
				if let n = found {
					next = level.rooms[n]
				} else {
					break
				}
				forceConnect = true
			}

			//uhh
			markConnected(at:next.index)
			var connectedTolerance:CGFloat = forceConnect ? 1 : 0.5

			/**
			tries to connect two rooms.
			- parameter direction: direction to find the other room
			*/
			func tryConnect(direction:RoomDirection) {
				guard next.doors[direction] == nil else { return }
				let p = direction.int
				guard let i = map[next.roomX+p.x,next.roomY+p.y] else { return }
				var doIt:CGFloat
				if connection[i] == .queued || connection[i] == .connected {
					doIt = connectedTolerance
					connectedTolerance *= 0.5
				} else {
					doIt = 0.25
				}
				if Random.value <= doIt {
					connectRooms(from:next.index,to:i,going:direction)
					if connection[i] != .connected && connection[i] != .queued {
						connection[i] = .queued
						queue.append(i)
					}
				}
			}

			//try to connect to all four possible rooms in a random sequence
			var tryCon:[RoomDirection] = [.up,.down,.left,.right]
			while tryCon.count > 0 {
				tryConnect(direction:tryCon.remove(at:Random.int(tryCon.count)))
			}
		}

		//generate room contents
		var roomEntitiesRem:[Int] = []
		for i in 0..<rooms {
			roomEntitiesRem.append(i)
		}
		var roomEntities:[RoomHasEntity] = Array(repeating:.none,count:level.rooms.count)
		for entity in LevelContext.current.entities.values {
			if entity.boss {
				roomEntities[level.bossRoom] = .contextualEntity(entity.name)
			} else {
				roomEntities[roomEntitiesRem.remove(at:Random.int(roomEntitiesRem.count))] = .contextualEntity(entity.name)
			}
		}
		let defaultEntityCount = min(roomEntitiesRem.count,LevelContext.defaultEntities.count,LevelContext.current.defaultEntities)
		var defaultEntities:[String] = LevelContext.defaultEntities.keys.map { $0 }
		for _ in 0..<defaultEntityCount {
			roomEntities[roomEntitiesRem.remove(at:Random.int(roomEntitiesRem.count))] = .defaultEntity(defaultEntities.remove(at:Random.int(defaultEntities.count)))
		}
		for (i,room) in level.rooms.enumerated() {
			room.layout(entity:roomEntities[i],level:level)
		}

		//debug the current level
		debug(level:level,map:map)

		return level
	}

	/**
	debugs a level room topology and some more info.
	- parameter level: level to be debugged
	- parameter map: level topology
	*/
	private static func debug(level:Level,map:Map) {
		var mapDebug = ""
		for y in (0..<map.height).reversed() {
			var upper = ""
			var middle = ""
			var lower = ""
			for x in map.a..<map.b {
				if let i = map[x,y] {
					let r = level.rooms[i]
					if r.doors[.up] != nil {
						upper += " |  "
					} else {
						upper += "    "
					}
					if r.doors[.left] != nil {
						middle += "-"
					} else {
						middle += " "
					}
					middle += "\(i)"
					let mid:String
					if r.doors[.right] != nil {
						mid = "-"
					} else {
						mid = " "
					}
					middle += mid
					if i < 10 {
						middle += mid
					}
					if r.doors[.down] != nil {
						lower += " |  "
					} else {
						lower += "    "
					}
				} else {
					upper += "    "
					middle += "    "
					lower += "    "
				}
			}
			mapDebug += "\(upper)\n\(middle)\n"
			if y == 0 {
				mapDebug += "\(lower)"
			}
		}
		print("///")
		print(mapDebug)
		for i in level.rooms {
			let e:String
			switch i.entity {
			case .none: e = "--"
			case .defaultEntity(let id): e = "default \"\(id)\""
			case .contextualEntity(let id): e = "contextual \"\(id)\""
			}
			print("\(i.index): \(e)")
		}
		print("///")
	}

	/**
	connection state for rooms.
	*/
	private enum RoomConnectionState {
		case disconnected
		case neighbour
		case queued
		case connected
	}

	/**
	class for level topology.
	*/
	private class Map {
		let width:Int
		let height:Int
		let a:Int
		let b:Int

		private var matrix:[[Int?]]

		/**
		generates a new level topology.
		- parameter rooms: room count
		- parameter height: number of rooms vertically
		*/
		init(rooms:Int,height:Int) {
			var r = rooms-height
			var roomIndex:Int = 0

			func createArray() -> [Int?] {
				var a:[Int?] = []
				for _ in 0..<height {
					a.append(roomIndex)
					roomIndex += 1
				}
				return a
			}

			func createArray(length:Int) -> [Int?] {
				if length >= height { return createArray() }
				var a:[Int?] = Array(repeating:nil,count:height)
				if length <= 0 { return a }
				var p:[Int] = []
				for i in 0..<height {
					p.append(i)
				}
				for _ in 0..<length {
					a[p.remove(at:Random.int(p.count))] = 0
				}
				for i in 0..<height {
					if a[i] != nil {
						a[i] = roomIndex
						roomIndex += 1
					}
				}
				return a
			}

			matrix = [createArray()]
			var w:Int = 0

			while r > 0 {
				r -= height*2
				w += 1
				if r >= 0 {
					matrix.insert(createArray(),at:0)
					matrix.append(createArray())
				} else {
					let n = r+height*2
					let l:Int
					let r:Int
					if Random.bool {
						l = n/2
						r = n-l
					} else {
						r = n/2
						l = n-r
					}
					matrix.insert(createArray(length:l),at:0)
					matrix.append(createArray(length:r))
				}
			}

			a = -w
			b = w+1
			width = w*2+1
			self.height = height
		}

		subscript(x:Int,y:Int) -> Int? {
			get {
				guard x >= a && x < b && y >= 0 && y < height else { return nil }
				return matrix[x-a][y]
			}
			set {
				guard x >= a && x < b && y >= 0 && y < height else { return }
				return matrix[x-a][y] = newValue
			}
		}
	}
}
