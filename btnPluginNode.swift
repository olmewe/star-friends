import SpriteKit

class btnPluginNode: SKNode {

	let btnPlugin:SKSpriteNode
	let feedbackPlugin:SKSpriteNode
	var enable = false

	let on:CGFloat
	let off:CGFloat

	init(size:CGFloat) {

		var s:CGSize

		let feedbackHeight = size*0.64
		s = CGSize(width:feedbackHeight*(232/64),height:feedbackHeight)
		feedbackPlugin = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("fbPlugin"),color:.clear,size:s)
		feedbackPlugin.anchorPoint = CGPoint(x:1,y:0.5)
		feedbackPlugin.position = .zero
		feedbackPlugin.zPosition = 4

		s = CGSize(width:size,height:size)
		btnPlugin = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("btnPlugin"),color:.clear,size:s)
		btnPlugin.position = .zero
		btnPlugin.zPosition = 3

		on = feedbackPlugin.size.height*0.5
		off = on-feedbackPlugin.size.width

		super.init()

		addChild(btnPlugin)
		addChild(feedbackPlugin)

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func set(enable:Bool,animated:Bool = true){
		self.enable = enable
		if animated {
			let duration:TimeInterval = 0.15
			if enable {
				feedbackPlugin.run(SKAction.moveTo(x:on,duration:duration)) {
					Audio.play(sfx:CommonAudio.sfxSettingsOn)
				}
			} else {
				Audio.play(sfx:CommonAudio.sfxSettingsOff)
				feedbackPlugin.run(SKAction.moveTo(x:off,duration:duration))
			}
		} else {
			feedbackPlugin.position.x = enable ? on : off
		}
	}

}
