import UIKit
import SpriteKit

class BattleLifeNode: SKSpriteNode {

	private var leftBlock:SKShapeNode?
	private var rightBlock:SKShapeNode?
	var blockBar:SKShapeNode?
	let barSpeed:CGFloat = 0.3
	var decreasing:Bool = false
	var currAnimPercent:CGFloat = -1.0
	var animFinalPercent:CGFloat = 0.0
	var isAnimating:Bool = false
	var value:CGFloat = -1.0
	var bar:SKShapeNode?
	var text:SKLabelNode?
	var barColor:UIColor!

	let borderFactor:CGFloat = 0.85

	init(type:Int, currentValue:CGFloat, size:CGSize) {

		//Type 0 => Water Green
		//Type 1 => Violet

		//var color:UIColor

		if(type == 0) {
			barColor = UIColor(red: 151/255, green: 221/255, blue: 207/255, alpha: 1.0)
		}

		else if (type == 1) {
			barColor = UIColor(red: 180/255, green: 180/255, blue: 245/255, alpha: 1.0)
		}

		else {
			barColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
		}



		//super.init(texture: nil, color: SKColor.clear, size: CGSize(width: parentSize.width * 0.6, height: parentSize.height * 0.05))
		super.init(texture: nil, color: SKColor.clear, size: size)
		self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
		//self.color = .green


		text = SKLabelNode()
		text!.fontName = "Schoolbell"
		text!.fontSize = self.size.height - 5.0
		text!.fontColor = UIColor.white
		text!.horizontalAlignmentMode = .center
		text!.verticalAlignmentMode = .center
		text!.zPosition = 20

		let x = -self.size.width / 2
		let y = -self.size.height / 2

		let borderBar = SKShapeNode(rect: CGRect(x: x, y: y, width: self.size.width, height: self.size.height), cornerRadius: self.size.height/2)
		borderBar.lineWidth = self.size.height - (self.size.height * borderFactor)
		borderBar.strokeColor = barColor
		borderBar.fillColor = UIColor.black
		borderBar.zPosition = 10

		//let height:CGFloat = self.size.height - (borderBar.lineWidth * 2.0)
		//let cRad:CGFloat = (height / 2.0) - 0.001

		bar = SKShapeNode(rect: CGRect(x: 0.0, y: 0.0, width: 0.0, height: self.size.height), cornerRadius: self.size.height/2)
		bar!.lineWidth = 0.0
		bar!.strokeColor = barColor
		bar!.fillColor = barColor
		bar!.zPosition = 11

		self.addChild(borderBar)
		self.addChild(bar!)
		self.addChild(text!)
		//self.addChild(text!)


		updateValue(currentValue, animated: false)
	}

	func updateValue(_ a:CGFloat, animated:Bool) {

		if (a != value) {

			//currAnimPercent = value

			value = a.clamp01

			if(blockBar != nil) {
				if(blockBar!.parent != nil) {
					blockBar!.removeFromParent()
				}
			}

			isAnimating = animated

			if(animated) {

				if(currAnimPercent > 1.0) {
					currAnimPercent = 1.0
				}

				else if(currAnimPercent < 0.0) {
					currAnimPercent = 0.0
				}

				animFinalPercent = value

				if(animFinalPercent > currAnimPercent) {
					decreasing = false
				}

				else if(animFinalPercent < currAnimPercent) {
					decreasing = true
				}

				self.updateValue(value, animated: true)
			}



			else {

				//bar!.removeAllChildren()

				currAnimPercent = value

				//let border = self.size.height - (self.size.height * borderFactor)
				//let width = (self.size.width - self.size.height) * fabs(1 - percent)
				let width = (self.size.width - self.size.height) * value

				if(bar != nil) {
					if(bar!.parent != nil) {
						bar!.removeFromParent()
					}

					bar = nil
				}

				if(width > 0.0) {
					/*blockBar = SKShapeNode(rect: CGRect(x: (self.size.width/2) - (self.size.height/2) - width,
					                                    y: -(self.size.height / 2) + border,
					                                    width: width,
					                                    height: self.size.height - (border * 2)))
					blockBar!.fillColor = .black
					blockBar!.strokeColor = .black
					blockBar!.lineWidth = 2.0
					blockBar!.zPosition = 15

					bar!.addChild(blockBar!)*/

					bar = SKShapeNode(rect: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.height + width, height: self.size.height), cornerRadius: self.size.height/2)
					bar!.lineWidth = 0.0
					bar!.strokeColor = barColor
					bar!.fillColor = barColor
					bar!.zPosition = 11

					self.addChild(bar!)
				}

				//addCirclesIfNeeded(currAnimPercent)
			}


			updateTextNode(currAnimPercent)

		}


		else if(isAnimating) {

			if(decreasing) {

				currAnimPercent -= Time.deltaTime * barSpeed

				if(currAnimPercent < animFinalPercent) {
					currAnimPercent = animFinalPercent
					isAnimating = false
				}
			}

			else {

				currAnimPercent += Time.deltaTime * barSpeed

				if(currAnimPercent > animFinalPercent) {
					currAnimPercent = animFinalPercent
					isAnimating = false
				}
			}

			if(currAnimPercent > 1.0) {
				currAnimPercent = 1.0
			}

			else if(currAnimPercent < 0.0) {
				currAnimPercent = 0.0
			}

			if(bar != nil) {
				if(bar!.parent != nil) {
					bar!.removeFromParent()
				}
			}

			//let border = self.size.height - (self.size.height * borderFactor)
			//let width = (self.size.width - self.size.height) * fabs(1 - currAnimPercent)
			let width = (self.size.width - self.size.height) * currAnimPercent

			if(width > 0.0) {

				bar = SKShapeNode(rect: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.height + width, height: self.size.height), cornerRadius: self.size.height/2)

				bar!.lineWidth = 0.0
				bar!.strokeColor = barColor
				bar!.fillColor = barColor
				bar!.zPosition = 11

				self.addChild(bar!)
			}
		}

		//addCirclesIfNeeded(currAnimPercent)
		updateTextNode(currAnimPercent)
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	private func updateTextNode(_ p:CGFloat) {

		var x:Int = Int(p * 100)

		if(x <= 0 && p > 0.0) {
			x = 1
		}

		//text!.removeFromParent()
		text!.text = String(x) + "%"
		text!.zPosition = 20
		//bar!.addChild(text!)
	}

	private func addCirclesIfNeeded(_ p:CGFloat) {

		if(p <= 0.0 && leftBlock == nil) {
			addLeftSemiCircle()
		}

		if(p < 1.0 && rightBlock == nil) {
			addRightSemiCircle()
		}

		if(p > 0.0 && leftBlock != nil) {
			if(leftBlock!.parent != nil) {
				leftBlock!.removeFromParent()
			}
			leftBlock = nil
		}

		if(p >= 1.0 && rightBlock != nil) {
			if(rightBlock!.parent != nil) {
				rightBlock!.removeFromParent()
			}
			rightBlock = nil
		}

	}

	private func addLeftSemiCircle() {

		if(leftBlock != nil) {
			if(leftBlock!.parent != nil) {
				leftBlock!.removeFromParent()
				leftBlock = nil
			}
		}

		let border = self.size.height - (self.size.height * borderFactor)
		let radius = (self.size.height/2) - border

		let path = CGMutablePath()

		path.addArc(center: CGPoint.zero, radius: radius, startAngle: -CGFloat.pi * 1/2, endAngle: CGFloat.pi * 1/2, clockwise: true)
		path.addLine(to: CGPoint(x: 0, y: radius))

		leftBlock = SKShapeNode(path: path, centered: true)

		leftBlock!.lineWidth = 2.0
		leftBlock!.strokeColor = .black
		leftBlock!.fillColor = .black
		leftBlock!.zPosition = 15
		leftBlock!.position.x = -((self.size.width/2) - self.size.height/2) - (border*2)
		leftBlock!.position.y = 0
		leftBlock!.position.y = 0

		bar!.addChild(leftBlock!)

	}

	private func addRightSemiCircle() {

		if(rightBlock != nil) {
			if(rightBlock!.parent != nil) {
				rightBlock!.removeFromParent()
				rightBlock = nil
			}
		}

		let border = self.size.height - (self.size.height * borderFactor)
		let radius = (self.size.height/2) - border

		let path = CGMutablePath()

		path.addArc(center: CGPoint.zero, radius: radius, startAngle: -CGFloat.pi * 1/2, endAngle: CGFloat.pi * 1/2, clockwise: false)
		path.addLine(to: CGPoint(x: 0, y: radius))

		rightBlock = SKShapeNode(path: path, centered: true)

		rightBlock!.lineWidth = 2.0
		rightBlock!.strokeColor = .black
		rightBlock!.fillColor = .black
		rightBlock!.zPosition = 15
		rightBlock!.position.x = ((self.size.width/2) - self.size.height/2) + (border*2)
		rightBlock!.position.y = 0

		bar!.addChild(rightBlock!)
	}

}
