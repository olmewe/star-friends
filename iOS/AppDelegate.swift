import UIKit

@UIApplicationMain
class AppDelegate:UIResponder,UIApplicationDelegate {
	var window:UIWindow?

	func application(_ application:UIApplication,didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey:Any]?) -> Bool {
		return true
	}

	func applicationWillResignActive(_ application:UIApplication) {
		//sent when the application is about to move from active to inactive state. this can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state. use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. games should use this method to pause the game
		GameManager.hide()
	}

	func applicationDidEnterBackground(_ application:UIApplication) {
		//use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later
	}

	func applicationWillEnterForeground(_ application:UIApplication) {
		//called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background
	}

	func applicationDidBecomeActive(_ application:UIApplication) {
		//restart any tasks that were paused (or not yet started) while the application was inactive. if the application was previously in the background, optionally refresh the user interface
		GameManager.show()
	}

	func applicationWillTerminate(_ application:UIApplication) {
		//called when the application is about to terminate. save data if appropriate
		GameManager.exit()
	}
}
