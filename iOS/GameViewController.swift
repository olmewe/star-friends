import UIKit
import SpriteKit

class GameViewController:UIViewController {
	override func viewDidLoad() {
		super.viewDidLoad()
		let scr = UIScreen.main.bounds
		GameManager.start(with:view as! SKView,width:scr.width,height:scr.height,pad:UIDevice.current.userInterfaceIdiom == .pad)
	}

	override var shouldAutorotate:Bool {
		return false
	}

	override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
		return .portrait
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	override var prefersStatusBarHidden:Bool {
		return true
	}
}
