import SpriteKit

class PageNode: SKSpriteNode {

	let colors = [#colorLiteral(red: 1.0, green: 0.89, blue: 0.8, alpha: 1),#colorLiteral(red: 0.8173170336, green: 0.7324490823, blue: 0.6650189964, alpha: 1)]
	var currentColor = 0

	init(page:Int, size:CGSize, atlas:SKTextureAtlas) {

		super.init(texture: nil, color: colors[0], size: size)
		self.zPosition = 2
		self.anchorPoint = CGPoint(x: 1.0, y: 0.5)

		let maxWidth = self.size.width * 0.9
		let newParagraph = "^l     "

		self.setFirstColor()

		var textScale:CGFloat = 0.35

		if(!Window.padMode) {
			textScale = 0.45
		}

		if(page == 0) {

			let title = getTextNodeWith(scale: 0.75, maxWidth: maxWidth, text: "^1Manual do Astronauta")

			let text1 = getTextNodeWith(scale: textScale, maxWidth: maxWidth, text: "^1" + newParagraph + "Olá, astronauta! Este é o seu manual. Sua missão vai te colocar em situações inesperadas e estamos aqui para ajudar. A Aliança Star Friends explora as mais inóspitas regiões do espaço sideral para ajudar pessoas com desordens de ansiedade. São muitos os tipos de desordem de ansiedade que existem. Você, astronauta,foi designado ao Esquadrão Alpha TAG, responsável por auxiliar pessoas com Transtorno de Ansiedade Generalizada. A ansiedade é um sentimento natural, mas para pessoas com desordens de ansiedade, ela costuma ser desproporcional à causa, durar por um período excessivamente longo e é, em geral, debilitante em maior grau. Essa ansiedade pode ser causada por experiências do cotidiano e resulta em sintomas físicos e mentais, como fadiga muscular e insônia." + newParagraph + "Ajudar uma pessoa com ansiedade exige empatia, compreensão e paciência. Tente entender quem é aquela pessoa e pelo que ela está passando. Converse com ela, se ela estiver disposta. Mostre que ela pode confiar em você. Não seja grosseiro ou agressivo. Isso não ajuda e pode piorar a situação." + newParagraph + "Contamos com você, astronauta." + newParagraph + newParagraph + "Aliança Star Friends")

			let x = -self.size.width/2
			let y1 = self.size.height * 0.41
			let y2:CGFloat = 0.0
			title.position = CGPoint(x: x, y: y1)
			text1.position = CGPoint(x: x, y: y2)

			self.addChild(title)
			self.addChild(text1)
		}

		else if(page == 1) {

			let imgWidth = self.size.width * 0.3
			let img1Height:CGFloat = imgWidth * 0.3554
			let img2Height:CGFloat = imgWidth * 1.2023

			let y1 = self.size.height * 0.37
			let y2 = self.size.height * 0.125
			let y3 = self.size.height * -0.2

			let text1 = getTextNodeWith(scale: textScale, maxWidth: maxWidth-imgWidth, text: "^1" + "Utilize a barra de navegação para acessar os sistemas de seu traje. Não se preocupe, ele está integrado aos sistemas da nave e você terá acesso à todas as suas funcionalidades.")
			let text2 = getTextNodeWith(scale: textScale, maxWidth: maxWidth-imgWidth, text: "^1" + "Em cada planeta há uma pessoa com dificuldades. Você deve encontrar o alvo de sua missão, ajudando todos que encontrar no caminho. Navegue pela sala tocando na tela. Utilize as portas para mudar de sala. Toque nas pessoas para interagir com elas... não literalmente,astronauta. Na tela.")
			let text3 = getTextNodeWith(scale: textScale, maxWidth: maxWidth-imgWidth, text: "^1" + "Ouça com atenção e considere sua resposta com cuidado. Os sistemas do seu traje mostram o quão disposta a pessoa está a continuar o diálogo. Respostas erradas fazem com que as pessoas não queiram mais falar com você. As pessoas que habitam aquele planeta podem lhe fornecer informações valiosas para sua missão.")

			let imgX = self.size.width * -0.82
			let textX = imgX + imgWidth/2 + 6.0 + (maxWidth-imgWidth)/2

			let image1 = SKSpriteNode(texture: atlas.textureNamed("img1"), color: .red, size: CGSize(width: imgWidth, height: img1Height))
			let image2 = SKSpriteNode(texture: atlas.textureNamed("img2"), color: .red, size: CGSize(width: imgWidth, height: img2Height))
			let image3 = SKSpriteNode(texture: atlas.textureNamed("img3"), color: .red, size: CGSize(width: imgWidth, height: img2Height))

			image1.position = CGPoint(x: imgX, y: y1)
			image2.position = CGPoint(x: imgX, y: y2)
			image3.position = CGPoint(x: imgX, y: y3)

			text1.position = CGPoint(x: textX, y: y1)
			text2.position = CGPoint(x: textX, y: y2)
			text3.position = CGPoint(x: textX, y: y3)

			self.addChild(image1)
			self.addChild(image2)
			self.addChild(image3)
			self.addChild(text1)
			self.addChild(text2)
			self.addChild(text3)

		}

		else if(page == 2) {

			let imgWidth = self.size.width * 0.3
			let imgHeight:CGFloat = imgWidth * 1.2023

			let y1 = self.size.height * 0.28

			let text1 = getTextNodeWith(scale: textScale, maxWidth: maxWidth-imgWidth, text: "^1" + "Caso você escolha as opcões erradas, respire fundo e mantenha a calma. Nem tudo está perdido. Seu traje está equipado com a última versão do revolucionário SIP (Software de Infiltração Psíquica). Utilize ele para convencer a pessoa a retomar o diálogo com você. Derrote as manifestações da desconfiança e quebre ou desvie das barreiras da intimidade. Colete os itens de bônus e tome cuidado com a integridade da sua própria psíque. Não seja destruído.")

			let imgX = self.size.width * -0.82
			let textX = imgX + imgWidth/2 + 6.0 + (maxWidth-imgWidth)/2

			let image1 = SKSpriteNode(texture: atlas.textureNamed("img4"), color: .clear, size: CGSize(width: imgWidth, height: imgHeight))

			image1.position = CGPoint(x: imgX, y: y1)
			text1.position = CGPoint(x: textX, y: y1)

			self.addChild(image1)
			self.addChild(text1)

		}

		else {

		}


	}

	func nextColor() {

		if(currentColor + 1 < colors.count) {
			currentColor += 1
			self.color = colors[currentColor]
		}
	}

	func previousColor() {

		if(currentColor - 1 >= 0) {
			currentColor -= 1
			self.color = colors[currentColor]
		}
	}

	func setFirstColor() {
		self.color = colors[0]
		currentColor = 0
	}

	func setLastColor() {
		self.color = colors.last!
		currentColor = colors.count - 1
	}

	func firstColor()->UIColor {
		return colors[0]
	}

	func lastColor()->UIColor {
		return colors[colors.count-1]
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func getTextNodeWith(scale:CGFloat, maxWidth:CGFloat, text:String)->TextNode {

		let width = (1 / scale) * maxWidth
		let node = TextNode(fontNamed: "Schoolbell")
		node.align = .left
		node.maxWidth = width
		node.set(text: text, animate: false)
		node.setScale(scale)

		return node

	}

}
