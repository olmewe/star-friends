import SpriteKit

class MainMenuScene: Scene {

	var pulseOff = true
	let pulseScale:CGFloat = 1.05
	let pulseVar:CGFloat = 0.06
	let arrowFlick:CGFloat = 10.0
	let flickDuration:CGFloat = 0.2
	var starsOffset:CGFloat = 0
	var starsPoint = CGPoint.zero

	override class func create() -> Scene {
		return MainMenuScene(size:Window.sceneSize)
	}

	var currentPlanet = 0
	let menuNode = SKNode()
	var stars:SKSpriteNode!
	var background:SKSpriteNode!
	var knowmoreButton:SKSpriteNode!
	var settingsButton:SKSpriteNode!
	var playButton:SKSpriteNode!
	var rightButton:SKSpriteNode!
	var leftButton:SKSpriteNode!
	var planetSprite:SKSpriteNode!
	var pannelButton:SKSpriteNode!
	var planetNameLabel:SKLabelNode!
	var stageNumberLabel:SKLabelNode!
	var atlas:SKTextureAtlas!

	let bgmMenu = "bgm/menu.mp3"

	var leaving = false

	override func loadResources() {
		Audio.load(batch:"menu",files:[
			bgmMenu,
		])
	}

	override func unloadResources() {
		Audio.unload(batch:"menu")
	}

	override func load() {
		backgroundColor = TransitionScene.fadeColour

		atlas = SKTextureAtlas(named: "menu")

		addChild(menuNode)

		let height = Window.sceneSize.height
		let size = CGSize(width: height * 0.75, height: height)
		background = SKSpriteNode(texture: atlas.textureNamed("menubackground"), color: .clear, size: size)
		background.position = .zero

		let stHeight = size.width*1.1
		let stSize = CGSize(width: stHeight, height: stHeight*341/768)
		stars = SKSpriteNode(texture: atlas.textureNamed("stars"), color: .clear, size: stSize)
		stars.zPosition = -1
		starsOffset = (stHeight-size.width)*0.5
		starsPoint = CGPoint(x:0,y:height*159/1024)

		var kx = Window.sceneSize.width * 0.37
		var ky = Window.sceneSize.height * -0.32
		let kHeight = Window.sceneSize.height / 10.33
		var kSize = CGSize(width: kHeight / 0.53447, height: kHeight)

		if(kx + (kSize.width/2) > Window.sceneSize.width/2) {
			let newHeight = Window.sceneSize.height / 15.0
			kSize = CGSize(width: newHeight / 0.53447, height: newHeight)
			kx = Window.sceneSize.width * 0.38
			ky = Window.sceneSize.height * -0.28
		}


		knowmoreButton = SKSpriteNode(texture: atlas.textureNamed("knowmore"), color: .blue, size: kSize)
		knowmoreButton.position = CGPoint(x: kx, y: ky)
		knowmoreButton.zPosition = 1


		let sHeight = Window.sceneSize.height / 10.5
		let sSize = CGSize(width: sHeight / 0.22496, height: sHeight)
		settingsButton = SKSpriteNode(texture: atlas.textureNamed("settings"), color: .clear, size: sSize)
		settingsButton.position = CGPoint(x: 0, y: Window.sceneSize.height * -0.435)
		settingsButton.zPosition = 1

		let pHeight = Window.sceneSize.height / 4.4
		let pSize = CGSize(width: pHeight / 0.77357, height: pHeight)
		playButton = SKSpriteNode(texture: atlas.textureNamed("playgame"), color: .clear, size: pSize)
		playButton.position = CGPoint(x: 0, y: Window.sceneSize.height * -0.17)
		playButton.zPosition = 1

		let y = Window.sceneSize.height * 0.23
		let distFactor:CGFloat = 0.2
		let arrHeight = ((Window.sceneSize.height / 10.33) / 2)
		let arrSize = CGSize(width: arrHeight * 0.58317, height: arrHeight)
		leftButton = SKSpriteNode(texture: atlas.textureNamed("leftArrow"), color: .clear, size: arrSize)
		leftButton.position = CGPoint(x: background.size.width * -distFactor, y: y)
		leftButton.zPosition = 2

		rightButton = SKSpriteNode(texture: atlas.textureNamed("rightArrow"), color: .clear, size: arrSize)
		rightButton.position = CGPoint(x: background.size.width * distFactor, y: y)
		rightButton.zPosition = 2

		let planetSize = CGSize(width: Window.sceneSize.height / 11, height: Window.sceneSize.height / 11)
		planetSprite = SKSpriteNode(texture: nil, color: .clear, size: planetSize)
		planetSprite.position = CGPoint(x: 0, y: y)
		planetSprite.zPosition = 2


		planetNameLabel = SKLabelNode(text: "--")
		planetNameLabel.verticalAlignmentMode = .center
		planetNameLabel.horizontalAlignmentMode = .center
		planetNameLabel.fontName = "Schoolbell"
		planetNameLabel.fontColor = .white
		planetNameLabel.fontSize = 19.0
		planetNameLabel.position.x = 0
		planetNameLabel.position.y = planetSprite.position.y - planetSprite.size.height * 0.9
		planetNameLabel.zPosition = 3

		stageNumberLabel = SKLabelNode(text: "--")
		stageNumberLabel.verticalAlignmentMode = .center
		stageNumberLabel.horizontalAlignmentMode = .center
		stageNumberLabel.fontName = "Schoolbell"
		stageNumberLabel.fontColor = .white
		stageNumberLabel.fontSize = 19.0
		stageNumberLabel.position.x = 0
		stageNumberLabel.position.y = planetNameLabel.position.y - 25.0
		stageNumberLabel.zPosition = 3

		let panHeight = Window.sceneSize.height * 0.3117
		let panSize = CGSize(width: panHeight / 0.5247, height: panHeight)
		pannelButton = SKSpriteNode(texture: atlas.textureNamed("pannel") , color: .red, size: panSize)
		pannelButton.position = CGPoint(x: 0.0, y: Window.sceneSize.height * 0.157)
		pannelButton.zPosition = 1

		let alpha:CGFloat = 0.0
		planetSprite.alpha = alpha
		leftButton.alpha = alpha
		rightButton.alpha = alpha
		stageNumberLabel.alpha = alpha
		planetNameLabel.alpha = alpha
		pannelButton.alpha = alpha

		menuNode.addChild(background)
		menuNode.addChild(stars)
		menuNode.addChild(knowmoreButton)
		menuNode.addChild(settingsButton)
		menuNode.addChild(playButton)
		menuNode.addChild(pannelButton)
		menuNode.addChild(leftButton)
		menuNode.addChild(rightButton)
		menuNode.addChild(planetSprite)
		menuNode.addChild(planetNameLabel)
		menuNode.addChild(stageNumberLabel)
	}

	override func show() {

		if self.alpha < 1.0 {
			self.run(SKAction.fadeIn(withDuration: 1))
		}

		changeNavigationStateTo(appearing: false)

		Audio.stopBgms()
		Audio.play(bgm:bgmMenu)

		updatePlanetView()

		leaving = false
	}

	override func update() {
		stars.position = CGPoint(
			x:starsPoint.x+sin(Time.time*0.15)*starsOffset*0.5,
			y:starsPoint.y+cos(Time.time*0.15)*starsOffset*0.5
		)

		var variation:CGFloat

		if(pulseOff) {
			variation = pulseVar*Time.deltaTime

			if(knowmoreButton.xScale + variation >= pulseScale) {
				pulseOff = false
			}
		}

		else {
			variation = -pulseVar*Time.deltaTime

			if(knowmoreButton.xScale + variation <= 1.0) {
				pulseOff = true
			}
		}

		if(!planetSprite.isHidden) {

			planetSprite.setScale(planetSprite.xScale + (variation*3))

			if(planetSprite.alpha < 1.0) {

				let alpha = planetSprite.alpha + 1.5*Time.deltaTime
				planetSprite.alpha = alpha
				leftButton.alpha = alpha
				rightButton.alpha = alpha
				stageNumberLabel.alpha = alpha
				planetNameLabel.alpha = alpha
				pannelButton.alpha = alpha
			}
		} else {

			playButton.setScale(playButton.xScale + variation)

			if(planetSprite.alpha > 0.0) {

				let alpha:CGFloat = 0
				planetSprite.alpha = alpha
				leftButton.alpha = alpha
				rightButton.alpha = alpha
				stageNumberLabel.alpha = alpha
				planetNameLabel.alpha = alpha
				pannelButton.alpha = alpha
			}
		}

		knowmoreButton.setScale(knowmoreButton.xScale + variation)
		settingsButton.setScale(settingsButton.xScale + variation)


		if menusClosed {
			for i in Input.touchEvents {

				if i.state == .release {

					checkTouchOnButtonsAtPoint(i.position)
					break
				}

			}
		}
	}

	private func updatePlanetView() {


		let a = SKAction.run {
			self.planetNameLabel.text = LevelContext.displayNameList[self.currentPlanet]

			let text:String
			if let completed = GameState.levelProgress[LevelContext.nameList[self.currentPlanet]] {
				text = completed ? "(recomeçar)" : "(em andamento)"
			} else {
				text = "(fase \(self.currentPlanet+1))"
			}

			self.stageNumberLabel.text = text
		}

		self.run(a)

		planetSprite.run(SKAction.setTexture(SKTexture(imageNamed:"contexts/"+LevelContext.nameList[currentPlanet]+"/planet")))

	}

	private func changeNavigationStateTo(appearing:Bool) {

		if(!appearing) {
			playButton.run(SKAction.setTexture(atlas.textureNamed("playgame")))
			planetSprite.setScale(1.0)
			planetSprite.isHidden = true
			leftButton.isHidden = true
			rightButton.isHidden = true
			stageNumberLabel.isHidden = true
			planetNameLabel.isHidden = true
			pannelButton.isHidden = true
		}

		else{
			playButton.run(SKAction.setTexture(atlas.textureNamed("playgame2")))
			playButton.setScale(1.0)
			planetSprite.isHidden = false
			leftButton.isHidden = false
			rightButton.isHidden = false
			stageNumberLabel.isHidden = false
			planetNameLabel.isHidden = false
			pannelButton.isHidden = false
		}

	}

	private func changePlanetTo(next:Bool) {

		if(next) {
			if(currentPlanet+1 < LevelContext.nameList.count) {
				currentPlanet += 1
				//planetSprite.texture = atlas.textureNamed(String(format: "planet%d", currentPlanet))
			}

			if(rightButton.hasActions()) {
				rightButton.removeAllActions()
			}

			let x = background.size.width * 0.2
			let a = SKAction.moveTo(x: x + arrowFlick, duration: TimeInterval(flickDuration / 2))
			let b = SKAction.moveTo(x: x, duration: TimeInterval(flickDuration / 2))
			let action = SKAction.sequence([a,b])

			rightButton.run(action)

		}

		else {
			if(currentPlanet > 0) {
				currentPlanet -= 1
				//planetSprite.texture = atlas.textureNamed(String(format: "planet%d", currentPlanet))
			}

			if(leftButton.hasActions()) {
				leftButton.removeAllActions()
			}

			let x = background.size.width * -0.2
			let a = SKAction.moveTo(x: x - arrowFlick, duration: TimeInterval(flickDuration / 2))
			let b = SKAction.moveTo(x: x, duration: TimeInterval(flickDuration / 2))
			let action = SKAction.sequence([a,b])

			leftButton.run(action)
		}

		updatePlanetView()

	}

	private func checkTouchOnButtonsAtPoint(_ p:CGPoint) {


		var left = knowmoreButton.position.x  - (knowmoreButton.size.width / 2)
		var right = knowmoreButton.position.x + (knowmoreButton.size.width / 2)
		var up = knowmoreButton.position.y + (knowmoreButton.size.height / 2)
		var down = knowmoreButton.position.y - (knowmoreButton.size.height / 2)

		//Knowmore Button action
		if(p.x > left && p.x < right && p.y < up && p.y > down) {

			openManual(fromMenu: true)
		}

		left = settingsButton.position.x  - (settingsButton.size.width / 2)
		right = settingsButton.position.x + (settingsButton.size.width / 2)
		up = settingsButton.position.y + (settingsButton.size.height / 2)
		down = settingsButton.position.y - (settingsButton.size.height / 2)

		//Settings Button action
		if(p.x > left && p.x < right && p.y < up && p.y > down) {

			openSettings(fromMenu: true)
		}

		left = playButton.position.x  - (playButton.size.width / 2)
		right = playButton.position.x + (playButton.size.width / 2)
		up = playButton.position.y + (playButton.size.height / 2)
		down = playButton.position.y - (playButton.size.height / 2)

		//Play Button action
		if(p.x > left && p.x < right && p.y < up && p.y > down) {

			changeNavigationStateTo(appearing: planetSprite.isHidden)
			Audio.play(sfx:CommonAudio.sfxSelect)

		}

		left = leftButton.position.x  - (leftButton.size.width * 2)
		right = leftButton.position.x + (leftButton.size.width * 2)
		up = leftButton.position.y + (leftButton.size.height * 2)
		down = leftButton.position.y - (leftButton.size.height * 2)

		//Left Button action
		if(p.x > left && p.x < right && p.y < up && p.y > down && !leftButton.isHidden) {
			changePlanetTo(next: false)
			Audio.play(sfx:CommonAudio.sfxSelect)

			return
		}

		left = rightButton.position.x  - (rightButton.size.width * 2)
		right = rightButton.position.x + (rightButton.size.width * 2)
		up = rightButton.position.y + (rightButton.size.height * 2)
		down = rightButton.position.y - (rightButton.size.height * 2)

		//Left Button action
		if(p.x > left && p.x < right && p.y < up && p.y > down && !rightButton.isHidden) {
			changePlanetTo(next: true)
			Audio.play(sfx:CommonAudio.sfxSelect)
			return
		}

		left = planetSprite.position.x  - (planetSprite.size.width / 2)
		right = planetSprite.position.x + (planetSprite.size.width / 2)
		up = planetSprite.position.y + (planetSprite.size.height / 2)
		down = planetSprite.position.y - (planetSprite.size.height / 2)

		//Plannet button action
		if(!leaving && p.x > left && p.x < right && p.y < up && p.y > down && !planetSprite.isHidden) {

			leaving = true

			Audio.play(sfx:CommonAudio.sfxTransition)

			let time = 1.0

			/*
			self.pannelButton.alpha = 0.0
			self.planetSprite.alpha = 0.0
			self.leftButton.alpha = 0.0
			self.rightButton.alpha = 0.0
			self.stageNumberLabel.alpha = 0.0
			self.planetNameLabel.alpha = 0.0
			self.playButton.alpha = 0.0
			self.knowmoreButton.alpha = 0.0
			self.settingsButton.alpha = 0.0
			*/

			let act = SKAction.scale(to: 1.5, duration: time)
			act.timingMode = .easeIn

			self.menuNode.run(act) {
				self.menuNode.setScale(1.0)
			}

			TransitionScene.toGame = true
			TransitionScene.thanks = false

			self.run(SKAction.fadeOut(withDuration: time), completion: {

				GameState.load(levelByContext:LevelContext.nameList[self.currentPlanet])

				SceneManager.show(SceneManager.transitionScene)

				/*
				self.pannelButton.alpha = 1.0
				self.planetSprite.alpha = 1.0
				self.leftButton.alpha = 1.0
				self.rightButton.alpha = 1.0
				self.stageNumberLabel.alpha = 1.0
				self.planetNameLabel.alpha = 1.0
				self.playButton.alpha = 1.0
				self.knowmoreButton.alpha = 1.0
				self.settingsButton.alpha = 1.0
				*/
			})


			/*self.run(SKAction.fadeOut(withDuration: 0.4), completion: {
				TransitionScene.toGame = true
				TransitionScene.thanks = false
				SceneManager.show(SceneManager.transitionScene)
			})*/

			return
		}

	}
}
