import SpriteKit

class DialogueOptionNode:ButtonNode {
	let spacementFactor:CGFloat = 0.92
	let text = TextNode()
	let backgroundNode:SKShapeNode

	private var heldTempo:CGFloat = 0

	init(width:CGFloat,height:CGFloat) {
		backgroundNode = SKShapeNode(rectOf:CGSize(width:width,height:height),cornerRadius:height/2.01)
		backgroundNode.lineWidth = 0
		backgroundNode.fillColor = DialogueSpeechBubbleNode.playerColour
		backgroundNode.zPosition = 0

		super.init()

		text.zPosition = 0.1
		text.colours = [.black,.white]
		text.velocityFactor = 2
		text.cooldownFactor = 10
		let scale:CGFloat = (Window.padMode ? 0.5 : 0.7)*0.9
		text.setScale(scale)
		text.align = .left
		text.anchorX = .left
		text.anchorY = .top
		text.position = CGPoint(x:-width*spacementFactor*0.5,y:-TextNode.fontSize*scale*0.4)

		self.addChild(backgroundNode)
		self.addChild(text)

		hitSize = CGSize(width:width,height:height)
		hitOffset = CGPoint.zero
	}

	override func updateButton() {
		super.updateButton()
		if held {
			if heldTempo < 1 {
				heldTempo += Time.deltaTime*16
				if heldTempo > 1 { heldTempo = 1 }
				backgroundNode.alpha = heldTempo.ease.lerp(1,0.75)
			}
		} else {
			if heldTempo > 0 {
				heldTempo -= Time.deltaTime*16
				if heldTempo < 0 { heldTempo = 0 }
				backgroundNode.alpha = heldTempo.ease.lerp(1,0.75)
			}
		}
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}
}
