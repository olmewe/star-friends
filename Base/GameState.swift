import SpriteKit

/**
game state class. stores the current state, such as the current level and room.
*/
class GameState {
	//game progress
	static var levelProgress:[String:Bool] = [:] //nil: none. false: incomplete. true: complete

	//settings
	static var volumeBgm:CGFloat = 1
	static var volumeSfx:CGFloat = 1
	static var volumeSpeech:CGFloat = 1
	static var mute:Bool = false
	static var difficulty:Difficulty = .medium
	enum Difficulty:Int {
		case easy
		case medium
		case hard
	}
	static var textVelocity:TextVelocity = .normal
	enum TextVelocity:Int {
		case slow
		case normal
		case fast
		case reallyFast
		case immediate
	}

	//level stuff
	static var level:Level?
	static var room:LevelRoom?
	static var entities:[EntityState] = []
	class EntityState {
		var positions:[CGPoint] = []
		var dialogueState:EntityDialogueState = .none
		var dialogueCycle:Int = 0
	}
	enum EntityDialogueState:Int {
		case none
		case lost
		case won
	}
	static var clues:[Int] = []

	//room stuff
	static var playerPos:CGPoint = CGPoint.zero

	//dialogue stuff
	static var dialogueEntity:EntityData?
	static var entityDisposition:CGFloat = 0
	static var entityDispositionTemp:CGFloat = 0
	static var playerHealth:CGFloat = 1
	static var playerHealthTemp:CGFloat = 1
	static var dialogueState:DialogueBlockType = .intro
	static var dialogueCycle:Int = 0
	static var dialogueOption:Int = 0
	static var dialogueRightAnswer:Bool = false

	/** loads user settings. */
	static func loadSettings() {
		if let state = UserDefaults.standard.dictionary(forKey:"settings") {
			if let volumeBgmN = state["volumeBgm"] as? Double {
				volumeBgm = CGFloat(volumeBgmN).clamp01
			}
			if let volumeSfxN = state["volumeSfx"] as? Double {
				volumeSfx = CGFloat(volumeSfxN).clamp01
			}
			if let volumeSpeechN = state["volumeSpeech"] as? Double {
				volumeSpeech = CGFloat(volumeSpeechN).clamp01
			}
			if let muteN = state["mute"] as? Bool {
				mute = muteN
			}
			if let difficultyInt = state["difficulty"] as? Int,let difficultyN = Difficulty(rawValue:difficultyInt) {
				difficulty = difficultyN
			}
			if let textVelocityInt = state["textVelocity"] as? Int,let textVelocityN = TextVelocity(rawValue:textVelocityInt) {
				textVelocity = textVelocityN
			}
		}
	}

	/** saves user settings. */
	static func saveSettings() {
		let settings:[String:Any] = [
			"volumeBgm":Double(volumeBgm),
			"volumeSfx":Double(volumeSfx),
			"volumeSpeech":Double(volumeSpeech),
			"mute":mute,
			"difficulty":difficulty.rawValue,
			"textVelocity":textVelocity.rawValue,
		]
		UserDefaults.standard.set(settings,forKey:"settings")
	}

	/** resets loaded level state. */
	static func resetLevelState() {
		level = nil
		room = nil
		entities = []
		clues = []

		playerPos = CGPoint.zero

		dialogueEntity = nil
		entityDisposition = 0
		playerHealth = 1
		dialogueState = .intro
		dialogueCycle = 0
		dialogueOption = 0
		dialogueRightAnswer = false
	}

	/** loads level progress for all levels. */
	static func loadLevelProgress() {
		if let progress = UserDefaults.standard.dictionary(forKey:"levelProgress") as? [String:Bool] {
			levelProgress = progress
		}
	}

	/** saves level progress for all levels. */
	static func saveLevelProgress() {
		UserDefaults.standard.set(levelProgress,forKey:"levelProgress")
	}

	/** loads current level state. */
	static func loadLevelState(recreate:Bool) {
		while !recreate,let state = UserDefaults.standard.dictionary(forKey:"state_\(LevelContext.current.name)") {
			guard let playerX = state["playerX"] as? Double,let playerY = state["playerY"] as? Double else { break }
			guard let roomIndex = state["room"] as? Int else { break }
			guard let cluesN = state["clues"] as? [Int] else { break }
			var entitiesN:[EntityState] = []
			while true {
				let i = entitiesN.count
				guard let hasEntity = state["entity\(i)"] as? Bool else { break }
				let entity = EntityState()
				entitiesN.append(entity)
				guard hasEntity else { continue }
				guard
					let positions = state["entity\(i)_pos"] as? [Double],
					let dialogueStateInt = state["entity\(i)_state"] as? Int,
					let dialogueCycle = state["entity\(i)_cycle"] as? Int
					else { continue }
				guard let dialogueState = EntityDialogueState(rawValue:dialogueStateInt) else { continue }
				for i in 0..<(positions.count/2) {
					entity.positions.append(CGPoint(x:positions[i*2],y:positions[i*2+1]))
				}
				entity.dialogueState = dialogueState
				entity.dialogueCycle = dialogueCycle
			}
			let dialogue = (state["dialogue"] as? Bool) == true
			if dialogue {
				guard
					let entityDispositionN = state["entityDisposition"] as? Double,
					let playerHealthN = state["playerHealth"] as? Double,
					let dialogueStateInt = state["dialogueState"] as? Int,
					let dialogueCycleN = state["dialogueCycle"] as? Int,
					let dialogueOptionN = state["dialogueOption"] as? Int,
					let dialogueRightAnswerN = state["dialogueRightAnswer"] as? Bool
					else { break }
				guard let dialogueStateN = DialogueBlockType(rawValue:dialogueStateInt) else { break }
				entityDisposition = CGFloat(entityDispositionN)
				playerHealth = CGFloat(playerHealthN)
				dialogueState = dialogueStateN
				dialogueCycle = dialogueCycleN
				dialogueOption = dialogueOptionN
				dialogueRightAnswer = dialogueRightAnswerN
			}
			moveToRoom(index:roomIndex)
			dialogueEntity = dialogue ? LevelContext.get(entity:room?.entity) : nil
			playerPos = CGPoint(x:playerX,y:playerY)
			entities = entitiesN
			clues = cluesN
			return
		}
		moveToRoom(index:level?.startRoom)
		playerPos = CGPoint.zero
		clues = []
		initEntityState()
		saveLevelState()
	}

	/** saves current level state. */
	static func saveLevelState() {
		var state:[String:Any] = [
			"playerX":Double(playerPos.x),
			"playerY":Double(playerPos.y),
			"room":room?.index ?? 0,
			"clues":clues,
		]
		for (n,i) in entities.enumerated() {
			if i.positions.isEmpty {
				state["entity\(n)"] = false
			} else {
				state["entity\(n)"] = true
				var positions:[Double] = []
				for j in i.positions {
					positions.append(Double(j.x))
					positions.append(Double(j.y))
				}
				state["entity\(n)_pos"] = positions
				state["entity\(n)_state"] = i.dialogueState.rawValue
				state["entity\(n)_cycle"] = i.dialogueCycle
			}
		}
		if dialogueEntity != nil {
			state["dialogue"] = true
			state["entityDisposition"] = Double(entityDisposition)
			state["playerHealth"] = Double(playerHealth)
			state["dialogueState"] = dialogueState.rawValue
			state["dialogueCycle"] = dialogueCycle
			state["dialogueOption"] = dialogueOption
			state["dialogueRightAnswer"] = dialogueRightAnswer
		}
		UserDefaults.standard.set(state,forKey:"state_\(LevelContext.current.name)")
	}

	/** deletes current level state. */
	static func deleteLevelState() {
		UserDefaults.standard.removeObject(forKey:"state_\(LevelContext.current.name)")
	}

	/** loads a level, given its id. */
	static func load(levelByContext context:String) {
		guard level?.context != context else { return }
		LevelContext.load(level:context)
		var lvl:Level?
		if levelProgress[context] == false {
			if let data = UserDefaults.standard.data(forKey:"level_\(context)") {
				lvl = NSKeyedUnarchiver.unarchiveObject(with:data) as? Level
			}
		}
		if let l = lvl {
			load(level:l)
		} else {
			let l = Level.generate(context:context,rooms:LevelContext.current.totalRooms)
			load(level:l,generated:true)
			save(level:l)
		}
	}

	/**
	saves level data.
	- parameter level: level to be saved
	*/
	static func save(level:Level) {
		UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject:level),forKey:"level_\(level.context)")
	}

	/**
	deletes level data.
	- parameter level: level to be deleted
	*/
	static func delete(level:String) {
		UserDefaults.standard.removeObject(forKey:"level_\(level)")
	}

	/**
	loads a level to the game state.
	- parameter level: level to be loaded
	*/
	static func load(level:Level,generated:Bool = false) {
		LevelContext.load(level:level.context)
		resetLevelState()

		levelProgress[level.context] = false
		saveLevelProgress()
		GameState.level = level

		loadLevelState(recreate:generated)
	}

	/** initialises entity state for this level. */
	static func initEntityState() {
		guard let l = level else { return }
		entities.removeAll()
		for r in l.rooms {
			let entity = EntityState()
			let quantity = LevelContext.get(entity:r.entity)?.quantity ?? 0
			if quantity > 0 {
				if r.index == l.bossRoom {
					for _ in 0..<quantity {
						entity.positions.append(CGPoint.zero)
					}
				} else {
					let ox = -CGFloat(r.width-1)/2
					let oy = -CGFloat(r.height-1)/2
					var pos:[(x:Int,y:Int)] = []
					for x in 2..<(r.width-2) {
						for y in 2..<(r.height-2) {
							if LevelContext.current.tileCollision(of:r.bg[y][x]) == .passthrough && LevelContext.current.tileCollision(of:r.fg[y][x]) == .passthrough {
								pos.append((x:x,y:y))
							}
						}
					}
					for _ in 0..<quantity {
						let p = pos.remove(at:Random.int(pos.count))
						entity.positions.append(CGPoint(x:CGFloat(p.x)+ox,y:CGFloat(p.y)+oy))
					}
				}
			}
			entities.append(entity)
		}
	}

	/**
	moves to a new room in the current level.
	- parameter direction: moving direction
	*/
	static func moveToRoom(direction:RoomDirection) {
		if let newRoom = room?.doors[direction] {
			room = level?.rooms[newRoom]
		}
	}

	/**
	moves to a new room in the current level.
	- parameter index: room index
	*/
	static func moveToRoom(index:Int?) {
		if let i = index {
			room = level?.rooms[i]
		}
	}

	/**
	moves to a new room in the current level.
	- parameter room: new room
	*/
	static func moveToRoom(_ room:LevelRoom) {
		GameState.room = room
	}

	/**
	begins a conversation with an entity.
	*/
	static func startDialogue() {
		guard let room = room else { return }
		dialogueEntity = LevelContext.get(entity:room.entity)
		guard let entity = dialogueEntity else { return }
		switch entities[room.index].dialogueState {
		case .won:
			dialogueEntity = nil
			return
		case .lost:
			entityDisposition = 0
			dialogueState = .sadIntro
		case .none:
			entityDisposition = entity.startDisposition
			dialogueState = .intro
		}
		playerHealth = 1
		dialogueCycle = entities[room.index].dialogueCycle
		dialogueOption = 0
		dialogueRightAnswer = false
	}

	/**
	ends a conversation with an entity.
	*/
	static func endDialogue(won:Bool) {
		guard let room = room else { return }
		dialogueEntity = nil
		let entity = entities[room.index]
		if won {
			entity.dialogueState = .won
			if let ent = LevelContext.get(entity:room.entity) {
				if ent.boss {
					levelProgress[LevelContext.current.name] = true
					saveLevelProgress()
					resetLevelState()
					deleteLevelState()
					delete(level:LevelContext.current.name)
				} else if ent.clue >= 0 && !clues.contains(ent.clue) {
					clues.append(ent.clue)
				}
			}
		} else {
			entity.dialogueState = .lost
			entity.dialogueCycle = dialogueCycle
		}
	}
}
