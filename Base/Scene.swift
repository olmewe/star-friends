import SpriteKit

/**
scene class. base to all scenes.
*/
class Scene:SKScene {
	class func create() -> Scene {
		return Scene()
	}

	static let settings = SettingNode()
	static let manual = ManualNode()

	var settings:SettingNode {
		return Scene.settings
	}

	var manual:ManualNode {
		return Scene.manual
	}

	var menusClosed = true

	/** scene camera position */
	var cameraPos:CGPoint {
		get {
			return CGPoint(x:(0.5-anchorPoint.x)*size.width,y:(0.5-anchorPoint.y)*size.height)
		}
		set {
			anchorPoint = CGPoint(x:0.5-newValue.x/size.width,y:0.5-newValue.y/size.height)
		}
	}

	/** button nodes in this scene */
	var buttons:[ButtonNode] = []
	/** list of button nodes pressed during this scene */
	private(set) var buttonsPressed:[ButtonNode] = []
	/** list of button nodes held during this scene */
	private(set) var buttonsHeld:[ButtonNode] = []
	/** list of button nodes released during this scene */
	private(set) var buttonsReleased:[ButtonNode] = []

	private var loaded = false

	/** overrides from skscene. use load() instead. */
	override func sceneDidLoad() {
		guard !loaded else { return }
		loaded = true
		scaleMode = .aspectFit
		cameraPos = CGPoint.zero
		load()
	}

	/** overrides from skscene. use update() instead. */
	override func update(_ currentTime:TimeInterval) {
		GameManager.update(currentTime)
		buttonsPressed.removeAll(keepingCapacity:true)
		buttonsHeld.removeAll(keepingCapacity:true)
		buttonsReleased.removeAll(keepingCapacity:true)
		for b in buttons {
			b.updateButton()
			if b.pressed { buttonsPressed.append(b) }
			if b.held { buttonsHeld.append(b) }
			if b.released { buttonsReleased.append(b) }
		}
		menusClosed = true
		if settings.isOpen {
			menusClosed = false
			settings.update()
		}
		if manual.isOpen {
			menusClosed = false
			manual.update()

		}
		update()
	}

	/** opens the settings node. */
	func openSettings(fromMenu:Bool) {
		if settings.parent == nil {
			addChild(settings)
		} else if settings.parent != self {
			settings.removeFromParent()
			addChild(settings)
		}
		settings.zPosition = 200.0
		settings.open(fromMenu: fromMenu)
	}

	/** opens the manual node. */
	func openManual(fromMenu:Bool) {

		if(manual.parent == nil) {
			addChild(manual)
		}

		else if (manual.parent != self) {
			manual.removeFromParent()
			addChild(manual)
		}

		manual.zPosition = 200.0
		manual.showManual(fromMenu: fromMenu)


	}

	/** called on scene load (usually when an instance is created). */
	func load() {}
	/** called when this scene is shown to this view. */
	func show() {}
	/** called when this scene needs to load resources. */
	func loadResources() {}
	/** called when this scene needs to unload resources. */
	func unloadResources() {}
	/** called when this scene is hidden from this view. */
	func hide() {}
	/** called on game loop. */
	func update() {}
}

#if os(iOS)
	extension Scene {
		override func touchesBegan(_ touches:Set<UITouch>,with event:UIEvent?) {
			touchesUpdated(touches)
		}

		override func touchesMoved(_ touches:Set<UITouch>,with event:UIEvent?) {
			touchesUpdated(touches)
		}

		override func touchesEnded(_ touches:Set<UITouch>,with event:UIEvent?) {
			touchesUpdated(touches)
		}

		override func touchesCancelled(_ touches:Set<UITouch>,with event:UIEvent?) {
			touchesUpdated(touches)
		}

		private func touchesUpdated(_ touches:Set<UITouch>) {
			for t in touches {
				let state:TouchState
				switch t.phase {
				case .began: state = .press
				case .ended: state = .release
				case .cancelled: state = .miss
				default: state = .hold
				}
				let force:CGFloat
				if #available(iOS 9,*) {
					force = t.force
				} else {
					force = 0
				}
				Input.add(touch:Touch(id:t.hashValue,state:state,position:t.location(in:self),pressure:((force-0.5)/6.16).clamp01))
			}
		}
	}
#else
	extension Scene {
		override func mouseDown(with event:NSEvent) {
			mouseUpdated(with:event,type:.leftMouseDown,state:.press)
		}

		override func mouseDragged(with event:NSEvent) {
			mouseUpdated(with:event,type:.leftMouseDragged,state:.hold)
		}

		override func mouseUp(with event:NSEvent) {
			mouseUpdated(with:event,type:.leftMouseUp,state:.release)
		}

		private func mouseUpdated(with event:NSEvent,type:NSEventType,state:TouchState) {
			guard event.type == type else { return }
			Input.add(touch:Touch(id:0,state:state,position:event.location(in:self)))
		}
	}
#endif
