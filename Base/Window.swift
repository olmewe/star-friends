import SpriteKit

/**
window class. stores more info about the current game view.
*/
class Window {
	static private(set) var width:CGFloat = 0
	static private(set) var height:CGFloat = 0

	static private(set) var ratio:CGFloat = 0
	static private(set) var ratioUnit:CGFloat = 0
	static private(set) var sceneSize = CGSize.zero
	static private(set) var padMode:Bool = false

	static let simulateOppositePadMode = false

	/**
	sets new values to this window.
	- parameter width: window width
	- parameter height: window height
	*/
	static func start(width:CGFloat,height:CGFloat,pad:Bool) {
		Window.width = width
		Window.height = height
		if width > height {
			ratio = width/height
		} else {
			ratio = height/width
		}
		if simulateOppositePadMode {
			ratio = pad ? 16/9 : 4/3
		} else {
			ratio = ratio.clamp(4/3,16/9)
		}
		ratioUnit = ratio.lerpInv(4/3,16/9)
		let sceneWidth:CGFloat = 320
		sceneSize = CGSize(width:sceneWidth,height:ratio*sceneWidth)
		padMode = simulateOppositePadMode ? !pad : pad
	}
}
