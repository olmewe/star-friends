import SpriteKit

/**
game manager class. the Singleton™
*/
class GameManager {
	static private let debug = false

	/** spritekit view being used */
	static private(set) var view:SKView!

	static var hidden = false

	/**
	starts the game with a given spritekit window.
	- parameter view: spritekit view
	- parameter width: window width
	- parameter height: window height
	*/
	static func start(with view:SKView,width:CGFloat,height:CGFloat,pad:Bool) {
		GameManager.view = view
		view.ignoresSiblingOrder = true
		view.showsFPS = debug
		view.showsNodeCount = debug
		view.showsDrawCount = debug

		Window.start(width:width,height:height,pad:pad)
		LevelContext.loadDefaults()
		GameState.loadSettings()
		GameState.loadLevelProgress()

		//cool ok start it
		SceneManager.show(SceneManager.firstScene)
	}

	/**
	updates game state and whatnot.
	- parameter currentTime: current time given by spritekit
	*/
	static func update(_ currentTime:TimeInterval) {
		Time.update(currentTime)
		Audio.update()
		Input.update()
	}

	/** called when the window is hidden. e.g. user presses home button. */
	static func hide() {
		if SceneManager.current != SceneManager.mainScene {
			GameState.saveLevelState()
		}
		hidden = true
	}

	/** called when the window is shown. e.g. user reopens the app. */
	static func show() {
		if hidden {
			hidden = false
			Audio.unloadUnused()
		}
		CommonAudio.loadResources()
		SceneManager.current.loadResources()
		Time.reset()
	}

	/** called when the window is closed. e.g. user kills the app. */
	static func exit() {
		if SceneManager.current != SceneManager.mainScene {
			GameState.saveLevelState()
		}
		Audio.unloadAll()
	}
}
