import SpriteKit

/**
scene manager class. shows and hides scenes.
*/
class SceneManager {
	/** current scene being shown */
	static private(set) var current:Scene!

	static let roomScene = RoomScene.create() as! RoomScene
	static let dialogueScene = DialogueScene.create() as! DialogueScene
	static let mainScene = MainMenuScene.create() as! MainMenuScene
	static let transitionScene = TransitionScene.create() as! TransitionScene

	/** starting scene of this game */
	static var firstScene:Scene {
		return mainScene
	}

	/**
	changes the current scene.
	- parameter scene: new scene to be shown
	*/
	static func show(_ scene:Scene) {
		if current != nil {
			current.hide()
			current.unloadResources()
		}
		current = scene
		GameManager.view.presentScene(scene)
		scene.sceneDidLoad()
		scene.loadResources()
		scene.show()
	}
}
