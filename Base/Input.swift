import SpriteKit

/**
input class. takes care of user input and touch events.
*/
class Input {
	/** list of all current touches */
	static fileprivate(set) var touches:[Touch] = []
	/** list of current touch events */
	static fileprivate(set) var touchEvents:[Touch] = []
	static fileprivate var touchEventsNew:[Touch] = []

	/** called by the game manager to update the event list. */
	static func update() {
		touchEvents = touchEventsNew
		touchEventsNew = []
		let touchesOld = touches
		touches = []
		for t in touchesOld {
			switch t.state {
			case .press:
				var nt = t
				nt.state = .hold
				touches.append(nt)
			case .hold: touches.append(t)
			default: continue
			}
		}
		for t in touchEvents {
			if t.state == .press {
				if !touches.contains(where:{$0.id == t.id}) {
					touches.append(t)
				}
			} else {
				if let i = touches.index(where:{$0.id == t.id}) {
					touches[i] = t
				} else if t.state == .hold {
					var nt = t
					nt.state = .press
					touches.append(nt)
				} else {
					touches.append(t)
				}
			}
		}
	}

	/**
	adds a touch to the future event list.
	- parameter touch: new touch
	*/
	static func add(touch:Touch) {
		touchEventsNew.append(touch)
	}
}

/**
touch struct. immutable; don't store an instance expecting to have it updated. instead, store its id.
*/
struct Touch {
	let id:Int
	var state:TouchState
	let position:CGPoint
	let pressure:CGFloat

	init(id:Int,state:TouchState,position:CGPoint,pressure:CGFloat = 0) {
		self.id = id
		self.state = state
		self.position = position
		self.pressure = pressure
	}
}

/**
touch state. miss means the touch was released, but not registered.
*/
enum TouchState:String {
	case press
	case hold
	case release
	case miss
}
