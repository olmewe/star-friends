import Foundation

class CommonAudio {
	static let sfxSelect = "menu/select.wav"
	static let sfxManualClose = "manual/close.wav"
	static let sfxManualNext = "manual/next.wav"
	static let sfxManualOpen = "manual/open.wav"
	static let sfxManualPrevious = "manual/previous.wav"
	static let sfxSettingsClose = "settings/close.wav"
	static let sfxSettingsOff = "settings/off.wav"
	static let sfxSettingsOn = "settings/on.wav"
	static let sfxSettingsOpen = "settings/open.wav"
	static let sfxTransition = "transition/transition.wav"

	static func loadResources() {
		Audio.load(batch:"common",files:[
			sfxSelect,
			sfxManualClose,
			sfxManualNext,
			sfxManualOpen,
			sfxManualPrevious,
			sfxSettingsClose,
			sfxSettingsOff,
			sfxSettingsOn,
			sfxSettingsOpen,
			sfxTransition,
		])
	}

	static func unloadResources() {
		Audio.unload(batch:"common")
	}
}
