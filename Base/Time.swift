import SpriteKit

/**
time class. stores time info.
*/
class Time {
	/** current time in seconds */
	static private(set) var time:CGFloat = 0
	/** time in seconds since last frame. one over current frame rate, too */
	static private(set) var deltaTime:CGFloat = defaultDeltaTime

	static private var firstTime = true
	static private let defaultDeltaTime:CGFloat = 1/60
	static private let maxDeltaTime:CGFloat = 4/60

	/**
	called by the game manager to update the current time.
	- paramenter currentTime: current time given by spritekit
	*/
	static func update(_ currentTime:TimeInterval) {
		if firstTime {
			firstTime = false
			time = CGFloat(currentTime)
			deltaTime = defaultDeltaTime
		} else {
			let previousTime = time
			time = CGFloat(currentTime)
			deltaTime = time-previousTime
			if deltaTime <= 0 {
				deltaTime = defaultDeltaTime
			} else if deltaTime > maxDeltaTime {
				deltaTime = maxDeltaTime
			}
		}
	}

	/** resets the time counter. */
	static func reset() {
		firstTime = true
	}
}
