import Foundation
import AVFoundation

class Audio {
	private static var files:[String:AudioFile] = [:]
	private static var loadedBatches:[String:[AudioFile]] = [:]
	private static var bgm:[String:Bgm] = [:]
	private static var bgmFade:[String:BgmFade] = [:]

	static func load(batch:String,files newFiles:[String]) {
		var newBatch:[AudioFile] = loadedBatches[batch] ?? []
		for i in newFiles {
			if let audioFile = files[i] {
				if !audioFile.batches.contains(batch) {
					audioFile.batches.append(batch)
					newBatch.append(audioFile)
				}
			} else if let audioFile = AudioFile(path:i,batch:batch) {
				files[i] = audioFile
				audioFile.batches.append(batch)
				newBatch.append(audioFile)
			} else {
				print("AUDIO WARNING: file couldn't be loaded. \(i)")
			}
		}
		loadedBatches[batch] = newBatch
	}

	static func unload(batch:String) {
		guard let batchFiles = loadedBatches[batch] else { return }
		for i in batchFiles {
			if i.remove(batch:batch) {
				DispatchQueue.global(qos:.default).async {
					if i.player.isPlaying {
						i.player.stop()
					}
				}
				files[i.path] = nil
				bgm[i.path] = nil
			}
		}
		loadedBatches[batch] = nil
	}

	static func unloadAll() {
		for (_,i) in files {
			DispatchQueue.global(qos:.default).async {
				if i.player.isPlaying {
					i.player.stop()
				}
			}
		}
		files.removeAll()
		loadedBatches.removeAll()
		bgm.removeAll()
		bgmFade.removeAll()
	}

	static func unloadUnused() {
		let f = files.values
		for i in f {
			if !i.player.isPlaying {
				files[i.path] = nil
				bgm[i.path] = nil
			}
		}
		for (batch,audios) in loadedBatches {
			let a = audios.filter { files[$0.path] != nil }
			if a.isEmpty {
				loadedBatches[batch] = nil
			} else {
				loadedBatches[batch] = a
			}
		}
	}

	static func play(sfx:String,volume:CGFloat = 1) {
		guard let audio = files[sfx] else { return }
		DispatchQueue.global(qos:.default).async {
			if audio.player.isPlaying {
				audio.player.pause()
			}
			audio.player.numberOfLoops = 0
			audio.player.currentTime = 0
			audio.player.volume = Float(volume)*sfxVolume()
			audio.player.play()
		}
	}

	static func play(bgm bgmNames:[String],volume:[CGFloat] = [],isDialogue:Bool = false) {
		for (n,i) in bgmNames.enumerated() {
			guard bgm[i] == nil else { continue }
			guard let audio = files[i] else { continue }
			let b = Bgm(audio:audio,isDialogue:isDialogue)
			b.volume = (n < volume.count) ? Float(volume[n]) : 1
			bgm[i] = b
			DispatchQueue.global(qos:.default).async {
				if audio.player.isPlaying {
					audio.player.pause()
				}
				audio.player.numberOfLoops = -1
				audio.player.currentTime = 0
				audio.player.volume = b.volume*bgmVolume(forDialogue:isDialogue)
				audio.player.play()
			}
		}
	}

	static func play(bgm bgmName:String,volume:CGFloat = 1,isDialogue:Bool = false) {
		play(bgm:[bgmName],volume:[volume],isDialogue:isDialogue)
	}

	static func set(bgm bgmNames:[String],volume:[CGFloat],fade:CGFloat = 0) {
		for (n,i) in bgmNames.enumerated() {
			guard let b = bgm[i] else { continue }
			guard n < volume.count else { return }
			let fadeTo = Float(volume[n])
			if b.volume != fadeTo {
				if fade <= 0 {
					b.volume = fadeTo
					b.audio.player.volume = fadeTo*bgmVolume(forDialogue:b.isDialogue)
				} else {
					bgmFade[i] = BgmFade(audio:i,currentVolume:b.volume,newVolume:fadeTo,duration:Float(fade))
				}
			} else {
				bgmFade[i] = nil
			}
		}
	}

	static func set(bgm bgmName:String,volume:CGFloat,fade:CGFloat = 0) {
		set(bgm:[bgmName],volume:[volume],fade:fade)
	}

	static func stop(bgm bgmName:String) {
		if let b = bgm[bgmName] {
			DispatchQueue.global(qos:.default).async {
				if b.audio.player.isPlaying {
					b.audio.player.pause()
				}
			}
			bgm[bgmName] = nil
		}
	}

	static func stopBgms() {
		for (_,i) in bgm {
			DispatchQueue.global(qos:.default).async {
				if i.audio.player.isPlaying {
					i.audio.player.pause()
				}
			}
		}
		bgm.removeAll()
	}

	static func updateVolume() {
		for (_,i) in bgm {
			DispatchQueue.global(qos:.default).async {
				i.audio.player.volume = i.volume*bgmVolume(forDialogue:i.isDialogue)
			}
		}
	}

	static func update() {
		guard !bgmFade.isEmpty else { return }
		let fade = bgmFade.keys
		for i in fade {
			guard let fadeProps = bgmFade[i] else { continue }
			guard let b = bgm[fadeProps.audio] else {
				bgmFade[i] = nil
				continue
			}
			var nv = b.volume+fadeProps.velocity*Float(Time.deltaTime)
			if fadeProps.velocity < 0 {
				if nv <= fadeProps.volume {
					nv = fadeProps.volume
					bgmFade[i] = nil
				}
			} else {
				if nv >= fadeProps.volume {
					nv = fadeProps.volume
					bgmFade[i] = nil
				}
			}
			b.volume = nv
			DispatchQueue.global(qos:.default).async {
				b.audio.player.volume = nv*bgmVolume(forDialogue:b.isDialogue)
			}
		}
	}

	private static func sfxVolume() -> Float {
		return GameState.mute ? 0 : Float(GameState.volumeSfx)
	}

	private static func bgmVolume(forDialogue:Bool) -> Float {
		return GameState.mute ? 0 : Float(forDialogue ? GameState.volumeSpeech : GameState.volumeBgm)
	}

	private struct BgmFade {
		let audio:String
		let volume:Float
		let velocity:Float

		init(audio:String,currentVolume:Float,newVolume:Float,duration:Float) {
			self.audio = audio
			volume = newVolume
			velocity = (newVolume-currentVolume)/duration
		}
	}

	private class Bgm {
		let audio:AudioFile
		let isDialogue:Bool
		var volume:Float

		init(audio:AudioFile,isDialogue:Bool) {
			self.audio = audio
			self.isDialogue = isDialogue
			self.volume = 1
		}
	}

	private class AudioFile {
		var path:String
		let player:AVAudioPlayer
		var batches:[String]

		init?(path:String,batch:String) {
			self.path = path
			guard let url = Bundle.main.url(forResource:"Audio/"+path,withExtension:nil) else {
				return nil
			}
			do {
				try player = AVAudioPlayer(contentsOf:url)
			} catch {
				return nil
			}
			batches = [batch]
		}

		func remove(batch:String) -> Bool {
			guard let index = batches.index(of:batch) else { return false }
			batches.remove(at:index)
			return batches.isEmpty
		}
	}
}
