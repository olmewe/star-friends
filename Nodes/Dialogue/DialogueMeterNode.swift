import SpriteKit

class DialogueMeterNode:SKNode {
	let bg = SKShapeNode(rectOf:CGSize(width:1,height:1))
	let meter = SKShapeNode(rectOf:CGSize(width:1,height:1))

	var level:CGFloat = 0 {
		didSet {
			level = level.clamp01
			resizeMeter()
		}
	}

	private let width:CGFloat
	private let height:CGFloat

	override init() {
		width = Window.sceneSize.width*0.7
		height = width*0.05
		super.init()

		addChild(bg)
		bg.position = CGPoint.zero
		bg.zPosition = 0
		bg.xScale = width
		bg.yScale = height
		bg.fillColor = .darkGray
		bg.lineWidth = 0

		addChild(meter)
		meter.zPosition = 0.1
		meter.yScale = height
		meter.fillColor = .green
		meter.lineWidth = 0

		resizeMeter()
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	private func resizeMeter() {
		meter.position = CGPoint(x:level.lerp(-width*0.5,0),y:0)
		meter.xScale = width*level
	}
}
