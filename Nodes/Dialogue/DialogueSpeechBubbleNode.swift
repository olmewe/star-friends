import SpriteKit

class DialogueSpeechBubbleNode:SKNode {
	let borderFactor:CGFloat = 0.01
	let spacementFactor:CGFloat = 0.95

	let text = TextNode()
	let backgroundNode:SKShapeNode
	let touch = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "ui/touch")))
	let player:Bool

	let options:[DialogueOptionNode]

	var showTouch = false

	static let playerColour = #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.9607843137, alpha: 1)
	static let entityColour = #colorLiteral(red: 0.5921568627, green: 0.8666666667, blue: 0.8117647059, alpha: 1)
	static let highlightColour = #colorLiteral(red: 0.8073909059, green: 0.9652064732, blue: 0.9336433597, alpha: 1)

	private var showingText = false
	private var textAnimation:CGFloat = 0

	private var showingOptions = false
	private var showingOptionsFull = false
	private var optionAnimation:CGFloat = 0
	private var optionCurrent:Int = 0
	private var optionTexts:[String] = []
	private var optionMax:Int = 0
	private var optionAvailable:[Bool] = []

	init(player:Bool,width:CGFloat,height:CGFloat) {
		self.player = player
		backgroundNode = SKShapeNode(rectOf:CGSize(width:width,height:height),cornerRadius:2)

		if player {
			var op:[DialogueOptionNode] = []

			let optionCount:Int = 3
			let optionWidth = width*0.95

			let optionMargin:CGFloat = 2
			let optionMarginMiddle:CGFloat = 1
			let optionHeight:CGFloat = 7.5

			let optionUnit = height/(optionMargin*2-optionMarginMiddle+(optionMarginMiddle+optionHeight)*CGFloat(optionCount))
			let optionUnitHeight = optionUnit*optionHeight
			let optionUnitDistance = optionUnit*(optionMarginMiddle+optionHeight)

			for i in 0..<optionCount {
				let option = DialogueOptionNode(width:optionWidth,height:optionUnitHeight)
				option.zPosition = 0.1
				option.position = CGPoint(x:0,y:(CGFloat(optionCount-1)/2-CGFloat(i))*optionUnitDistance)
				op.append(option)
			}
			options = op
		} else {
			options = []
		}
		optionAvailable = Array(repeating:false,count:options.count)

		super.init()

		let colour = player ? DialogueSpeechBubbleNode.playerColour : DialogueSpeechBubbleNode.entityColour

		addChild(backgroundNode)
		backgroundNode.zPosition = 0
		backgroundNode.position = CGPoint.zero
		backgroundNode.lineWidth = width*borderFactor
		backgroundNode.strokeColor = colour
		backgroundNode.fillColor = .black

		addChild(text)
		text.zPosition = 0.1
		let scale:CGFloat = Window.padMode ? 0.5 : 0.7
		text.setScale(scale)
		text.align = .left
		text.colours = [colour,DialogueSpeechBubbleNode.highlightColour]
		text.maxWidth = width*spacementFactor/scale
		text.anchorX = .left
		text.anchorY = .top
		let margin = width*(1-spacementFactor)*0.5
		text.position = CGPoint(x:-width*spacementFactor*0.5,y:height*0.5-TextNode.fontSize*scale-margin)

		addChild(touch)
		touch.zPosition = 0.1
		let touchSize:CGFloat = Window.sceneSize.height*0.025
		touch.position = CGPoint(x:width*0.5-touchSize,y:touchSize-height*0.5)
		touch.size = CGSize(width:touchSize,height:touchSize)
		touch.color = colour
		touch.isHidden = true

		for i in options {
			addChild(i)
		}
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	func update() {
		if showingText {
			if showTouch && text.animationEnded {
				touch.isHidden = false
				touch.colorBlendFactor = sin(Time.time*4)*0.5+0.5
			} else {
				touch.isHidden = true
			}
		} else {
			touch.isHidden = true
			if textAnimation > 0 {
				textAnimation -= Time.deltaTime*4
				if textAnimation <= 0 {
					textAnimation = 0
					text.isHidden = true
				} else {
					text.alpha = textAnimation.easeOut
				}
			}
		}
		if showingOptions {
			if optionAnimation < 1 {
				optionAnimation += Time.deltaTime*4
				if optionAnimation >= 1 {
					optionAnimation = 1
					showingOptionsFull = true
				}
				for i in options {
					i.isHidden = false
					i.yScale = optionAnimation.easeOut
				}
			}
			if showingOptionsFull && optionCurrent < optionMax && (optionCurrent < 0 || options[optionCurrent].text.animationEnded) {
				while true {
					optionCurrent += 1
					if optionCurrent < optionMax {
						if !optionAvailable[optionCurrent] {
							continue
						}
						options[optionCurrent].text.set(text:optionTexts[optionCurrent],animate:true)
					}
					break
				}
			}
		} else {
			if optionAnimation > 0 {
				showingOptionsFull = false
				optionAnimation -= Time.deltaTime*4
				if optionAnimation <= 0 {
					for i in options {
						i.isHidden = true
					}
				} else {
					for i in options {
						i.yScale = optionAnimation.easeOut
					}
				}
			}
		}
		text.update()
		for (n,i) in options.enumerated() {
			i.interactable = showingOptionsFull && optionAvailable[n] && optionCurrent >= optionMax
			i.text.update()
		}
	}

	func reset() {
		showingText = false
		showingOptions = false
		text.isHidden = true
		for i in options {
			i.isHidden = true
		}
	}

	func clear() {
		clearText()
		clearOptions()
	}

	private func clearText() {
		showingText = false
	}

	private func clearOptions() {
		if showingOptions {
			showingOptions = false
			Audio.play(sfx:SceneManager.dialogueScene.sfxDialogueOptionsHide)
			for i in options {
				if !i.released {
					i.text.text = ""
				}
			}
		}
	}

	func set(text:String) {
		clearOptions()
		showingText = true
		textAnimation = 1
		self.text.isHidden = false
		self.text.alpha = 1
		self.text.set(text:text,animate:true)
	}

	func set(options optionTexts:[String]) {
		clearText()
		showingOptions = true
		self.optionTexts = optionTexts
		optionAnimation = 0
		optionCurrent = -1
		for (n,i) in options.enumerated() {
			optionAvailable[n] = n < optionTexts.count && !optionTexts[n].isEmpty
			i.alpha = optionAvailable[n] ? 1 : 0.5
			i.text.text = ""
		}
		optionMax = min(options.count,optionTexts.count)
		Audio.play(sfx:SceneManager.dialogueScene.sfxDialogueOptionsShow)
	}

	func forceAnimationEnd() -> Bool {
		if showingText {
			return text.forceAnimationEnd()
		}
		if showingOptions {
			if optionCurrent >= optionMax {
				return false
			}
			if optionCurrent >= 0 {
				_ = options[optionCurrent].text.forceAnimationEnd()
			}
			if optionCurrent+1 < optionMax {
				for n in (optionCurrent+1)..<optionMax {
					options[n].text.text = optionTexts[n]
				}
			}
			optionCurrent = optionMax
			return true
		}
		return false
	}
}
