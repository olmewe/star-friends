import UIKit
import SpriteKit

class BattleBulletNode: SKSpriteNode {

	var angle : Double = 0
	var isFromCharacter = false

	init(entity : SKSpriteNode, isFromCharacter:Bool, texture:SKTexture) {
		/*var texture:SKTexture
		if(isFromCharacter) {
			texture = SKTexture(imageNamed: "battle/bulletPlayer")
		}

		else {
			texture = SKTexture(imageNamed: "battle/bulletEnemy")
		}*/

		super.init(texture: texture, color: SKColor.clear, size: CGSize(width:15,height:15))
		self.name = "bullet"
		self.position = CGPoint(x: entity.position.x, y: entity.position.y)
		self.isFromCharacter = isFromCharacter
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	func updatePosition() {

		if(!isFromCharacter) {
			self.position.x -= 180*CGFloat(sin(self.angle))*Time.deltaTime
			self.position.y -= 180*CGFloat(cos(self.angle))*Time.deltaTime
		}

		else {
			self.position.y += 300*Time.deltaTime
		}
	}
}
