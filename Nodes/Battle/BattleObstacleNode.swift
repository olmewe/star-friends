import UIKit
import SpriteKit

class BattleObstacleNode: SKSpriteNode {

	var velocity : CGFloat = 100
	var hits = 1

	init(y:CGFloat,left:CGFloat,right:CGFloat) {
		super.init(texture:nil,color:#colorLiteral(red: 0.5921568627, green: 0.8666666667, blue: 0.8117647059, alpha: 1),size:CGSize(width:abs(right-left),height:8))
		self.name = "obstacle"
		self.position = CGPoint(x:(left+right)*0.5,y:y)
		self.alpha = 0
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	func updatePosition() {
		if(self.alpha < 1.0) {
			self.alpha += 6*Time.deltaTime
		}
		self.position.y -= self.velocity * Time.deltaTime
	}
}
