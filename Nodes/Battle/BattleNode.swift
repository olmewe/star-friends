import UIKit
import SpriteKit

class BattleNode: SKSpriteNode {

	var dialogueScene:DialogueScene!

	let spriteAtlas = SKTextureAtlas(named: "battle")

	var obstacleDamage:CGFloat = 0.15
	//let bulletDamage:CGFloat = 20.0

	var xFactor:CGFloat = 0.0
	var moveX:CGFloat = 0.0
	let characterSpeed:CGFloat = 150.0
	var entitySpeed:CGFloat = 70.0

	var timeBetweenBlocks:CGFloat = 1.3
	var blockTimer:CGFloat = 1.3

	var timeToEntity:CGFloat = 8.0
	var entityTimer:CGFloat = 5.0

	var timeToBullets:CGFloat = 1.25
	var bulletTimer:CGFloat = 0.0

	let timeToShoot:CGFloat = 0.2
	var shootTimer:CGFloat = 0.0

	var battlePaused = true

	var backgroundBack:SKSpriteNode!
	var backgroundFront:SKSpriteNode!
	var character:BattlePlayerNode!
	var entity:BattleEntityNode!
	var bullets : [BattleBulletNode] = []
	var charBullets: [BattleBulletNode] = []
	var obstacles : [BattleObstacleNode] = []

	var countdown:CGFloat = 0
	var endPause:CGFloat = 0

	var dispositionPerSecond:CGFloat = 0.01
	var dispositionPerHit:CGFloat = 0.05
	var healthPerDamage:CGFloat = -0.1

	let countdownNumbers:CGFloat = 3
	let countdownDuration:CGFloat = 3
	let endPauseDuration:CGFloat = 3

	var isBoss = false
	var difficulty:Int = 0

	let overlayLabel = SKLabelNode(fontNamed:"Schoolbell")

	init() {

		let size:CGSize

		if Window.padMode {
			size = CGSize(width: Window.sceneSize.width * 0.826, height: Window.sceneSize.height * 0.755)
		} else {
			size = CGSize(width: Window.sceneSize.width * 1.0, height: Window.sceneSize.height * 0.86)
		}

		character = BattlePlayerNode(texture: spriteAtlas.textureNamed("player"), parentSize: size)
		entity = BattleEntityNode(texture: spriteAtlas.textureNamed("entity"), parentSize: size)

		backgroundBack = SKSpriteNode(texture: spriteAtlas.textureNamed("background0"), color: .clear, size: size)
		backgroundBack.setScale(0.20)
		backgroundBack.size = size
		backgroundBack.position = CGPoint.zero
		backgroundBack.zPosition = 0

		backgroundFront = SKSpriteNode(texture: spriteAtlas.textureNamed("background1"), color: .clear, size: size)
		backgroundFront.setScale(0.20)
		backgroundFront.size = size
		backgroundFront.position = CGPoint.zero
		backgroundFront.zPosition = 0.1


		super.init(texture: nil, color: SKColor.black, size: size)
		self.addChild(backgroundBack)
		self.addChild(backgroundFront)
		self.addChild(self.character)
		self.character.zPosition = 1
		self.addChild(self.entity)
		self.entity.zPosition = 3
		self.addChild(self.overlayLabel)

		overlayLabel.text = ""
		overlayLabel.verticalAlignmentMode = .center
		overlayLabel.horizontalAlignmentMode = .center
		overlayLabel.fontColor = .white
		overlayLabel.zPosition = 10

	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	/** resets the battle node. */
	func reset() {
		character.resetPosition()
		entity.isHidden = true
		self.position = CGPoint(x: 0.0, y: (Window.sceneSize.height / 2) * -0.14)
		countdown = countdownNumbers
		overlayLabel.fontSize = 48
		endPause = 0
		for i in bullets {
			i.removeFromParent()
		}
		bullets.removeAll()
		for i in charBullets {
			i.removeFromParent()
		}
		charBullets.removeAll()
		for i in obstacles {
			i.removeFromParent()
		}
		obstacles.removeAll()
		backgroundFront.alpha = (GameState.entityDispositionTemp > 0.5) ? 1 : 0
		battlePaused = true
	}

	/**
	updates the current node.
	- returns: true if it's still active; false if it should be closed
	*/
	func update() -> Bool {
		if !dialogueScene.menusClosed {
			battlePaused = true
			countdown = 3
			return true
		}

		if endPause > 0 {
			endPause -= Time.deltaTime
			if endPause <= 0 {
				endPause = 0
				return false
			}
			let hide = (Int(endPause*3) % 2) == 1
			if hide {
				overlayLabel.isHidden = true
			} else if overlayLabel.isHidden {
				overlayLabel.isHidden = false
				Audio.play(sfx:dialogueScene.sfxBattleEnd)
			}
			return true
		}

		if battlePaused {
			battlePaused = false
			isBoss = GameState.dialogueEntity?.boss == true
			switch GameState.difficulty {
			case .easy: difficulty = 0
			case .medium: difficulty = 1
			case .hard: difficulty = 2
			}
			let d = CGFloat(difficulty)/2
			obstacleDamage = d.lerp(0.1,0.2)
			entitySpeed = d.lerp(60,80)
			timeBetweenBlocks = d.lerp(1.6,1)
			timeToEntity = d.lerp(10,6)
			timeToBullets = d.lerp(1.5,1.2)
			if let ent = GameState.dialogueEntity {
				dispositionPerSecond = ent.battleDispositionPerSecond
				dispositionPerHit = ent.battleDispositionPerHit
				healthPerDamage = ent.battleHealthPerDamage
			}
			if d == 0 {
				dispositionPerSecond *= 1.5
				dispositionPerHit *= 1.5
				healthPerDamage *= 0.5
			} else if d == 2 {
				dispositionPerSecond *= 0.8
				dispositionPerHit *= 0.8
				healthPerDamage *= 1.5
			}
		}

		if countdown > 0 {
			let prev = Int(floor(countdown+1))
			countdown -= Time.deltaTime*countdownNumbers/countdownDuration
			let new = Int(floor(countdown+1))
			if countdown <= 0 {
				countdown = 0
				Audio.play(sfx:dialogueScene.sfxBattleCountdown)
				overlayLabel.isHidden = true
			} else if countdown > 0 {
				overlayLabel.isHidden = false
				if prev != new {
					overlayLabel.text = new.description
					Audio.play(sfx:dialogueScene.sfxBattleCountdown)
				}
				return true
			}
		}

		self.updateBackgroundIfNeeded()
		self.spawnBullet()
		self.spawnObstacle()
		self.moveCharacterIfNeeded()
		self.spawnEntityIfNeeded()


		for i in bullets {
			i.updatePosition()

			//Off bounds
			if i.position.x > self.size.width || i.position.x < -self.size.width/2.0 || i.position.y < -self.size.height/2.0 || i.position.y > self.size.height/2.0 {
				i.removeFromParent()
				self.bullets.remove(at: self.bullets.index(of: i)!)
			}

			//Collision of bullet in character
			if(spritesColliding(i,character)) {

				GameState.playerHealthTemp += healthPerDamage

				bullets.remove(at: bullets.index(of: i)!)
				i.removeFromParent()
				Audio.play(sfx:dialogueScene.sfxBattlePlayerDamage)
			}

		}

		for a in charBullets {
			a.updatePosition()
			var hit = false

			for i in obstacles {

				//One of the char bullets hitted obstacle
				if(spritesColliding(i,a) && !hit) {
					a.removeFromParent()
					charBullets.remove(at: charBullets.index(of: a)!)

					if i.hits <= 1 {
						i.removeFromParent()
						obstacles.remove(at: obstacles.index(of: i)!)
					}

					else {
						i.hits -= 1
					}

					hit = true
					Audio.play(sfx:dialogueScene.sfxBattleObstacle)
				}

			}


			if !entity.isHidden && (spritesColliding(a, entity) && !hit) {

				a.removeFromParent()
				charBullets.remove(at: charBullets.index(of: a)!)
				entity.hits -= 1

				//Entity is destroyed
				if(entity.hits <= 0) {
					entityTimer = 0.0
					entity.isHidden = true
					entity.position = CGPoint(x: 0, y: (self.size.height / 2) + (entity.size.height / 2))

					GameState.entityDispositionTemp += dispositionPerHit

				}
				Audio.play(sfx:dialogueScene.sfxBattleEnemyDamage)

			}
		}


		for i in obstacles {
			i.updatePosition()

			//Collision with character
			if(spritesColliding(i,character)) {

				GameState.playerHealthTemp -= obstacleDamage

				obstacles.remove(at: obstacles.index(of: i)!)
				i.removeFromParent()
				Audio.play(sfx:dialogueScene.sfxBattlePlayerDamage)
			}

			//Off bounds
			else if i.position.y + (i.size.height / 2.0) < -self.size.height/2.0 {
				i.removeFromParent()
				self.obstacles.remove(at: self.obstacles.index(of: i)!)
			}

		}


		for i in Input.touchEvents {

			if i.state == .press {

				let left = character.position.x - (character.size.width / 2)
				let right = character.position.x + (character.size.width / 2)

				if touchIsInsideNode(i.position) {

					//Touch close on the character, so it needs to SHOOT!!
					if(i.position.x > left && i.position.x < right) {

						if shootTimer >= timeToShoot {
							shootTimer = 0.0
							let bullet = BattleBulletNode(entity: character, isFromCharacter: true, texture:spriteAtlas.textureNamed("bulletPlayer"))
							bullet.angle = angleToTarget()
							self.addChild(bullet)
							bullet.zPosition = 4
							self.charBullets.append(bullet)
							Audio.play(sfx:dialogueScene.sfxBattlePlayerShoot)
						}

					}

						//Just move
					else{

						moveX = i.position.x

						let bLeft:CGFloat = -self.size.width / 2.0 + (character.size.width / 2) + 10.0
						let bRight:CGFloat = self.size.width / 2.0 - (character.size.width / 2) - 10.0

						//Check if x is offbounds
						if(moveX > bRight) {
							moveX = bRight
						}

						//Check if x is offbounds
						if(moveX < bLeft) {
							moveX = bLeft
						}


						//Factor for direction
						if (i.position.x - character.position.x) > 0.0 {
							xFactor = 1.0
						}

						else {
							xFactor = -1.0
						}

					}
				}

			}

		}

		GameState.entityDispositionTemp += dispositionPerSecond*Time.deltaTime

		if GameState.playerHealthTemp <= 0 {
			GameState.playerHealthTemp = 0
			endPause = endPauseDuration
			overlayLabel.fontSize = 22
			overlayLabel.text = "VIDA ESGOTADA!"
			overlayLabel.isHidden = true
		} else if GameState.entityDispositionTemp >= DialogueScene.dispositionMax {
			GameState.entityDispositionTemp = min(GameState.entityDispositionTemp,1)
			endPause = endPauseDuration
			overlayLabel.fontSize = 22
			overlayLabel.text = "DISPOSIÇÃO RECUPERADA!"
			overlayLabel.isHidden = true
		}

		return true
	}

	private func updateBackgroundIfNeeded() {
		if GameState.entityDispositionTemp > 0.5 {
			if backgroundFront.alpha < 1 {
				backgroundFront.alpha += Time.deltaTime
				if backgroundFront.alpha > 1 { backgroundFront.alpha = 1 }
			}
		} else {
			if backgroundFront.alpha > 0 {
				backgroundFront.alpha -= Time.deltaTime
				if backgroundFront.alpha < 0 { backgroundFront.alpha = 0 }
			}
		}
	}

	private func spawnEntityIfNeeded() {

		entityTimer += Time.deltaTime

		//Time to spawn
		if(entityTimer >= timeToEntity && entity.isHidden) {

			entity.position = CGPoint(x: Random.range(min: -self.size.width*0.4, max: self.size.width*0.4), y: (self.size.height / 2) + (entity.size.height / 2))
			entity.hits = 2
			entity.isHidden = false
		}

		if(!entity.isHidden) {

			if(entity.position.y > 0) {
				entity.position.y -= entitySpeed * Time.deltaTime
			}

			//Random movements
			else {
				entity.updatePosition()
			}
		}



	}

	private func touchIsInsideNode(_ p:CGPoint)->Bool {

		let left = self.position.x - (self.size.width / 2.0)
		let right = self.position.x + (self.size.width / 2.0)
		let up = self.position.y + (self.size.height / 2.0)
		let down = self.position.y - (self.size.height / 2.0)

		if(p.x > left && p.x < right) {

			if(p.y > down && p.y < up) {
				return true
			}

		}

		return false
	}

	private func moveCharacterIfNeeded() {

		shootTimer += Time.deltaTime

		if (xFactor > 0.0 && character.position.x < moveX)  {

			character.position.x += (characterSpeed * Time.deltaTime) * xFactor

			if(character.position.x > moveX) {
				character.position.x = moveX
				xFactor = 0.0
			}
		}

		else if (xFactor < 0.0 && character.position.x > moveX) {

			character.position.x += (characterSpeed * Time.deltaTime) * xFactor

			if(character.position.x < moveX) {
				character.position.x = moveX
				xFactor = 0.0
			}
		}


	}

	private func spawnBullet() {

		bulletTimer += Time.deltaTime

		if bullets.count < 5 && bulletTimer >= timeToBullets && !entity.isHidden {
			bulletTimer = 0.0
			let bullet = BattleBulletNode(entity: entity, isFromCharacter: false, texture:spriteAtlas.textureNamed("bulletEnemy"))
			bullet.angle = angleToTarget()
			self.addChild(bullet)
			bullet.zPosition = 4
			self.bullets.append(bullet)
			Audio.play(sfx:dialogueScene.sfxBattleEnemyShoot)
		}
	}

	private func spawnObstacle() {

		blockTimer += Time.deltaTime

		if obstacles.count < 7 && blockTimer >= timeBetweenBlocks {
			blockTimer = 0.0

			let obstacleTypes:[Int]
			let velocity:CGFloat
			switch difficulty {
			case 0:
				obstacleTypes = isBoss ? [0,1] : [0]
				velocity = 80
			case 1:
				obstacleTypes = isBoss ? [0,1,2] : [0,1]
				velocity = 100
			case 2:
				obstacleTypes = isBoss ? [1,2] : [0,1,2]
				velocity = 120
			default: return
			}

			let width = Random.range(min:0.2,max:0.25)+CGFloat(difficulty)*0.075
			let spaceWidth = Random.range(min:0.15,max:0.2)-CGFloat(difficulty)*0.025

			let spawnX = size.width*0.5
			let spawnY = size.height*0.5
			func spawn(left:CGFloat,right:CGFloat) {
				let block = BattleObstacleNode(y:spawnY,left:left.lerp(-spawnX,spawnX),right:right.lerp(-spawnX,spawnX))
				block.velocity = velocity
				self.addChild(block)
				block.zPosition = 3
				self.obstacles.append(block)
			}

			let obstacleType = obstacleTypes[Random.range(min:0,max:obstacleTypes.count)]
			switch obstacleType {
			case 0: //single random obstacle
				let left = Random.value*(1-width)
				spawn(left:left,right:left+width)
			case 1: //two random obstacles
				for i in [CGFloat(0),CGFloat(0.5)] {
					let left = Random.value*(0.5-width)+i
					spawn(left:left,right:left+width)
				}
			case 2: //basically a wall with a tiny space to go through
				let left = Random.value*(1-spaceWidth)
				if left > 0.01 {
					spawn(left:0,right:left)
				}
				if left+spaceWidth < 0.99 {
					spawn(left:left+spaceWidth,right:1)
				}
			default: break
			}

		}
	}

	private func angleToTarget() -> Double {
		let x = Double(self.entity.position.x - self.character.position.x)
		let y = Double(self.entity.position.y - self.character.position.y)

		return atan(x / y)
	}

	private func spritesColliding(_ a:SKSpriteNode, _ b:SKSpriteNode)->Bool {

		let aLeft = a.position.x - (a.size.width/2.0)
		let aRight = a.position.x + (a.size.width/2.0)
		let aUp = a.position.y + (a.size.height/2.0)
		let aDown = a.position.y - (a.size.height/2.0)

		let bLeft = b.position.x - (b.size.width/2.0)
		let bRight = b.position.x + (b.size.width/2.0)
		let bUp = b.position.y + (b.size.height/2.0)
		let bDown = b.position.y - (b.size.height/2.0)

		if( (aLeft > bLeft && aLeft < bRight) || (aRight > bLeft && aRight < bRight) ) {

			if( (aDown < bUp && aDown > bDown) || (aUp < bUp && aUp > bDown) ) {
				return true
			}
		}

		else if( (bLeft > aLeft && bLeft < aRight) || (bRight > aLeft && bRight < aRight) ) {

			if( (bDown < aUp && bDown > aDown) || (bUp < aUp && bUp > aDown) ) {
				return true
			}
		}

		return false

	}
}
