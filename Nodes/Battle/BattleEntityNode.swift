import UIKit
import SpriteKit

class BattleEntityNode: SKSpriteNode {

	var iterations:CGFloat = 1
	var moveAmount:CGFloat = 0
	private var parentSize:CGSize!

	var velocity : CGFloat = 35.0
	var hits = 0

	init(texture: SKTexture, parentSize:CGSize) {
		self.parentSize = parentSize
		super.init(texture: texture, color: SKColor.clear, size: CGSize(width:30,height:30))
		self.name = name
		self.position = CGPoint(x: 0, y: parentSize.height * 0.45)
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	func randomPosition() {
		self.moveAmount = Random.bool ? 1 : -1
	}

	func updatePosition() {

		iterations -= Time.deltaTime*0.3

		if(iterations <= 0) {
			iterations = 1
			randomPosition()
		}

		if(self.moveAmount != 0) {
			move(by:self.moveAmount)
		}

	}

	func move(by amount:CGFloat) {

		let limit = (parentSize.width/2.0) - (self.size.width/2.0) - 10.0
		let go = self.position.x + self.velocity * Time.deltaTime * amount

		self.position.x = go.clamp(-limit,limit)
	}

}
