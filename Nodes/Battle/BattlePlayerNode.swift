import UIKit
import SpriteKit

class BattlePlayerNode: SKSpriteNode {

	var velocity: CGFloat = 3
	let startPosition:CGPoint

	init(texture: SKTexture, parentSize:CGSize) {
		startPosition = CGPoint(x: 0, y: parentSize.height * -0.3)
		super.init(texture: texture, color: SKColor.clear, size: CGSize(width:30,height:30))
		self.name = name
	}

	required init?(coder aDecoder: NSCoder) {
		startPosition = CGPoint.zero
		super.init(coder: aDecoder)
	}

	func resetPosition() {
		self.position = startPosition
	}
}
