import SpriteKit

class ButtonSpriteNode:ButtonNode {
	let sprite = SKSpriteNode()

	override var hitSize:CGSize {
		get {
			return CGSize(width:sprite.frame.width,height:sprite.frame.height)
		}
		set {
			//
		}
	}

	override var hitOffset:CGPoint {
		get {
			return CGPoint(x:sprite.frame.midX,y:sprite.frame.midY)
		}
		set {
			//
		}
	}

	override init() {
		super.init()
		addChild(sprite)
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	private var scaleAnim:CGFloat = 1

	override func updateButton() {
		super.updateButton()
		if released {
			scaleAnim = 1.1
			updateSpriteScale()
		} else {
			let s:CGFloat = held ? 0.9 : 1
			if scaleAnim > s {
				scaleAnim -= Time.deltaTime*4
				if scaleAnim < s { scaleAnim = s }
				updateSpriteScale()
			} else if scaleAnim < s {
				scaleAnim += Time.deltaTime*4
				if scaleAnim > s { scaleAnim = s }
				updateSpriteScale()
			}
		}
	}

	private func updateSpriteScale() {
		sprite.setScale(scaleAnim)
	}
}
