import SpriteKit

class ButtonTextNode:ButtonNode {
	let text = TextNode()

	override var hitSize:CGSize {
		get {
			return CGSize(width:text.width,height:text.height*TextNode.fontSize)
		}
		set {
			//
		}
	}

	override var hitOffset:CGPoint {
		get {
			return CGPoint(x:text.centreX,y:text.centreY*TextNode.fontSize)
		}
		set {
			//
		}
	}

	override init() {
		super.init()
		addChild(text)
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	private var scaleAnim:CGFloat = 1

	override func updateButton() {
		super.updateButton()
		if released {
			scaleAnim = 1.1
			updateTextScale()
		} else {
			let s:CGFloat = held ? 0.9 : 1
			if scaleAnim > s {
				scaleAnim -= Time.deltaTime*4
				if scaleAnim < s { scaleAnim = s }
				updateTextScale()
			} else if scaleAnim < s {
				scaleAnim += Time.deltaTime*4
				if scaleAnim > s { scaleAnim = s }
				updateTextScale()
			}
		}
	}

	private func updateTextScale() {
		text.setScale(scaleAnim)
	}
}
