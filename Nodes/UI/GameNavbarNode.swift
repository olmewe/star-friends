import SpriteKit

class GameNavbarNode:SKNode {
	let centre:CGPoint
	let size:CGSize

	let bg = SKSpriteNode()
	let level = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "ui/navbarLevelPlaceholder")))

	let menuButton = GameNavbarButtonNode(text:"menu")//.localized)
	let optionsButton = GameNavbarButtonNode(text:"opções")//.localized)
	let manualButton = GameNavbarButtonNode(text:"manual")//.localized)

	var buttons:[ButtonNode] {
		return [
			menuButton,
			optionsButton,
			manualButton,
		]
	}

	var interactable:Bool {
		get {
			return menuButton.interactable
		}
		set {
			menuButton.interactable = newValue
			optionsButton.interactable = newValue
			manualButton.interactable = newValue
		}
	}

	override init() {
		let s = Window.sceneSize
		let buttonStart:CGFloat
		let buttonOffset:CGFloat
		let levelSize:CGFloat
		let levelPosition:CGFloat
		if Window.padMode {
			centre = CGPoint(x:0,y:-0.0685*s.height)
			size = CGSize(width:s.width*0.825,height:s.height*0.742)
			bg.texture = SKTexture(image:#imageLiteral(resourceName: "ui/navbarPad"))
			bg.position = CGPoint.zero
			bg.size = s
			buttonStart = 0.356
			buttonOffset = 0.166
			levelSize = s.width*0.35
			levelPosition = -0.25
		} else {
			let h = 0.264*s.width
			centre = CGPoint(x:0,y:-h*0.5)
			size = CGSize(width:s.width,height:s.height-h)
			bg.texture = SKTexture(image:#imageLiteral(resourceName: "ui/navbarPhone"))
			bg.position = CGPoint(x:0,y:(s.height-h)*0.5)
			bg.size = CGSize(width:s.width,height:h)
			buttonStart = 0.4
			buttonOffset = 0.18
			levelSize = s.width*0.4
			levelPosition = -0.27
		}
		super.init()
		addChild(bg)
		bg.zPosition = 0
		let y = s.height*0.5-s.width*0.1337
		addChild(level)
		level.zPosition = 1
		level.size = CGSize(width:levelSize,height:levelSize/2.625)
		level.position = CGPoint(x:levelPosition*s.width,y:y)
		addChild(menuButton)
		menuButton.zPosition = 1
		menuButton.position = CGPoint(x:(buttonStart-buttonOffset*2)*s.width,y:y)
		addChild(optionsButton)
		optionsButton.zPosition = 1
		optionsButton.position = CGPoint(x:(buttonStart-buttonOffset)*s.width,y:y)
		addChild(manualButton)
		manualButton.zPosition = 1
		manualButton.position = CGPoint(x:buttonStart*s.width,y:y)
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func isOverNavbar(pos:CGPoint) -> Bool {
		return pos.x < centre.x-size.width*0.5 || pos.x > centre.x+size.width*0.5 || pos.y < centre.y-size.height*0.5 || pos.y > centre.y+size.height*0.5
	}
}
