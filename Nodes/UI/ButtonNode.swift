import SpriteKit

class ButtonNode:SKNode {
	/** if true, button reacts to touches */
	var interactable:Bool = true

	/** button hitbox size (considers current scale) */
	var hitSize:CGSize = CGSize.one
	/** button hitbox offset (considers current scale) */
	var hitOffset:CGPoint = CGPoint.zero

	/** has this button been pressed? */
	private(set) var pressed = false
	/** is this button being held? */
	var held:Bool {
		return !touchesHold.isEmpty
	}
	/** has this button been released? */
	private(set) var released = false

	private var touches:[Int] = []
	private var touchesHold:[Int] = []

	/** updates the current button state, given the frame inputs. should only be called by Scene. */
	func updateButton() {
		pressed = false
		released = false
		guard interactable else {
			if !touches.isEmpty {
				touches.removeAll()
				touchesHold.removeAll()
			}
			return
		}
		let xm = hitOffset.x-hitSize.width*0.5
		let xM = hitOffset.x+hitSize.width*0.5
		let ym = hitOffset.y-hitSize.height*0.5
		let yM = hitOffset.y+hitSize.height*0.5
		for i in Input.touchEvents {
			let p = convert(i.position,from:SceneManager.current)
			let inside = p.x >= xm && p.x <= xM && p.y >= ym && p.y <= yM
			if i.state == .press {
				if inside && !touches.contains(i.id) {
					if touches.isEmpty {
						pressed = true
					}
					touches.append(i.id)
					touchesHold.append(i.id)
				}
			} else if touches.contains(i.id) {
				if i.state == .hold {
					if inside {
						if !touchesHold.contains(i.id) {
							touchesHold.append(i.id)
						}
					} else if let index = touchesHold.index(of:i.id) {
						touchesHold.remove(at:index)
					}
				} else {
					if let index = touches.index(of:i.id) {
						touches.remove(at:index)
					}
					if let index = touchesHold.index(of:i.id) {
						touchesHold.remove(at:index)
					}
					if inside && i.state == .release && touches.isEmpty {
						released = true
					}
				}
			}
		}
	}
}
