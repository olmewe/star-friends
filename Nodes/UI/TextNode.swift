import SpriteKit

/**
text node. animated and formatted multiline text for spritekit.
*/
class TextNode:SKNode {
	/** font being used by the text node */
	let fontName:String

	/** text animation velocity factor */
	var velocityFactor:CGFloat = 1
	/** text animation cooldown velocity factor */
	var cooldownFactor:CGFloat = 1

	/** text being shown by the node */
	var text:String {
		get {
			return textRaw
		}
		set {
			set(text:newValue)
		}
	}
	private var textRaw:String = ""

	/** colour list (can use up to 10 colours) */
	var colours:[SKColor] = [
		.white,
		.black,
	]


	/** text horizontal anchor */
	var anchorX:TextPosX = .centre
	/** text vertical anchor */
	var anchorY:TextPosY = .centre
	/** text horizontal alignment */
	var align:TextPosX = .centre
	/** maximum width (if not positive, width isn't clamped) */
	var maxWidth:CGFloat = 0

	/** full text width */
	private(set) var width:CGFloat = 0
	/** full text height */
	private(set) var height:CGFloat = 0
	/** text frame centre x */
	var centreX:CGFloat {
		let ox:CGFloat
		switch anchorX {
		case .left: ox = width/2
		case .centre: ox = 0
		case .right: ox = -width/2
		}
		return ox
	}
	/** text frame centre y */
	var centreY:CGFloat {
		let oy:CGFloat
		switch anchorY {
		case .top: oy = 1/2+(height-1)/2
		case .centre: oy = 1/2
		case .bottom: oy = 1/2-(height-1)/2
		}
		return oy
	}
	/** was there a character showing up on screen just now? */
	var characterAnim:Bool {
		return characterAnimTempo > 0
	}
	/** has the animation already ended? */
	var animationEnded:Bool {
		return !(animIndex < animMax || animCooldown > 0)
	}

	/** default font used by the text node */
	static let defaultFontName = "Schoolbell"
	/** text node font size */
	static let fontSize:CGFloat = 30

	init(fontNamed fontName:String = TextNode.defaultFontName) {

		self.fontName = fontName
		if fontName == TextNode.defaultFontName {
			sizeTest = TextNode.sizeTest
		} else {
			sizeTest = SKLabelNode(fontNamed:fontName)
		}

		sizeTest.fontSize = TextNode.fontSize
		sizeTest.horizontalAlignmentMode = .left
		sizeTest.verticalAlignmentMode = .baseline
		super.init()
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	/**
	sets a new text to this node.
	- parameter text: new text to be shown
	- parameter animate: if true, animates the text entrance
	*/
	func set(text:String,animate:Bool = false) {
		textRaw = text
		parse(animate:animate && GameState.textVelocity != .immediate)
		self.animate(forceEnd:false)
		arrange(firstTime:true)
	}

	/** updates the text node. should be called every frame. */
	func update() {
		animate(forceEnd:false)
		arrange(firstTime:false)
		if characterAnimTempo > 0 {
			characterAnimTempo -= Time.deltaTime*24
			if characterAnimTempo < 0 { characterAnimTempo = 0 }
		}
	}

	/** rearranges the text. call this after changing colours or anything like that. */
	func rearrange() {
		arrange(firstTime:true)
	}

	/**
	forces the animation to skip to its end, where the entire text can be seen.
	- returns: false if the animation had already ended; true otherwise
	*/
	func forceAnimationEnd() -> Bool {
		if animIndex < animMax {
			animate(forceEnd:true)
			return true
		}
		return animCooldown > 0
	}

	//
	//internals below!!
	//

	private static let noise = NoiseMap(samples:128)
	private static let sizeTest = SKLabelNode(fontNamed:defaultFontName)
	private let sizeTest:SKLabelNode

	private var chunks:[Chunk] = []
	private var lineWidth:[CGFloat] = []

	private var charTiming:[(chunk:Int,index:Int)] = []
	private var animIndex:CGFloat = 0
	private var animMax:CGFloat = 0
	private var animCooldown:CGFloat = 0
	private var characterAnimTempo:CGFloat = 0

	/**
	chunk of text.
	*/
	private class Chunk {
		let node:SKLabelNode

		var length:Int = 0
		var text:String = ""
		var pos:CGFloat = 0
		var line:Int = 0
		var style = ChunkStyle()

		init(fontNamed fontName:String,parent:SKNode) {
			node = SKLabelNode(fontNamed:fontName)
			parent.addChild(node)
			node.fontSize = TextNode.fontSize
			node.horizontalAlignmentMode = .left
			node.verticalAlignmentMode = .baseline
		}

		func remove() {
			node.removeFromParent()
		}
	}

	/**
	text style in a chunk.
	*/
	private struct ChunkStyle {
		var anim:CharAnim = .none
		var colour:Int = 0
	}

	/**
	character individual animation in a text.
	*/
	private enum CharAnim {
		case none
		case shake
		case wave
	}

	/**
	parses the current text as text chunks.
	- parameter animate: if true, animates the text entrance
	*/
	private func parse(animate:Bool) {
		lineWidth.removeAll(keepingCapacity:true)
		charTiming.removeAll(keepingCapacity:true)
		sizeTest.text = ""
		var fullLine = ""
		var prevFullLine = ""

		var index:Int = 0
		var line:Int = 0

		let useMaxWidth = maxWidth > 0
		var currentWidth:CGFloat = 0

		var chunk:Chunk?

		var newLine = false
		var command = false
		var space = false
		var style = ChunkStyle()

		let linePause:Int
		let manualPause:Int
		switch GameState.textVelocity {
		case .slow,.normal:
			linePause = 3
			manualPause = 8
		case .fast:
			linePause = 2
			manualPause = 5
		case .reallyFast,.immediate:
			linePause = 1
			manualPause = 1
		}

		/**
		adds a pause to the text animation.
		- parameter frames: amount of time in animation frames to wait
		*/
		func addPause(_ frames:Int) {
			guard animate else { return }
			for _ in 0..<frames {
				charTiming.append((chunk:-1,index:0))
			}
		}

		/** gets the next chunk. creates a new one if needed. */
		func getChunk() -> Chunk {
			let chunk:Chunk
			if index < chunks.count {
				chunk = chunks[index]
			} else {
				chunk = Chunk(fontNamed:fontName,parent:self)
				chunks.append(chunk)
			}
			index += 1
			return chunk
		}

		/** finishes the current chunk and moves to the next. */
		func finishChunk() {
			guard let ch = chunk else { return }
			chunk = nil
			ch.node.text = ch.text
			sizeTest.text = fullLine
			if useMaxWidth && sizeTest.frame.width > maxWidth && ch.text != fullLine {
				sizeTest.text = prevFullLine
				finishLine()
				ch.line = line
				fullLine = ch.text
				sizeTest.text = fullLine
			}
			ch.pos = sizeTest.frame.width-ch.node.frame.width
			if animate {
				for i in 0..<ch.length {
					charTiming.append((chunk:index-1,index:i))
				}
			}
			prevFullLine = fullLine
		}

		/** finishes the current line. */
		func finishLine() {
			finishChunk()
			newLine = true
			line += 1
			let currentWidth = sizeTest.frame.width
			lineWidth.append(currentWidth)
			if width < currentWidth {
				width = currentWidth
			}
			fullLine = ""
			prevFullLine = ""
			addPause(linePause)
		}

		//go through all chars!
		for c in textRaw.characters {
			if command {
				command = false
				switch c {
				case "0": style.colour = 0; finishChunk()
				case "1": style.colour = 1; finishChunk()
				case "2": style.colour = 2; finishChunk()
				case "3": style.colour = 3; finishChunk()
				case "4": style.colour = 4; finishChunk()
				case "5": style.colour = 5; finishChunk()
				case "6": style.colour = 6; finishChunk()
				case "7": style.colour = 7; finishChunk()
				case "8": style.colour = 8; finishChunk()
				case "9": style.colour = 9; finishChunk()

				case "n": style.anim = .none; finishChunk()
				case "s": style.anim = .shake; finishChunk()
				case "w": style.anim = .wave; finishChunk()

				case "p": addPause(manualPause)
				case "l": finishLine()

				default: break
				}
				continue
			}
			if c == "^" {
				command = true
			} else if c == " " {
				if !newLine && !space {
					space = true
					if useMaxWidth { finishChunk() }
				}
			} else {
				newLine = false
				let ch:Chunk
				let sc = String(c)
				if let nch = chunk {
					ch = nch
					if space {
						ch.text += " "
						ch.length += 1
					}
					ch.text += sc
					ch.length += 1
				} else {
					ch = getChunk()
					chunk = ch
					ch.text = sc
					ch.length = 1
				}
				if space {
					space = false
					fullLine += " "
				}
				fullLine += sc
				ch.line = line
				ch.style = style
				if style.anim != .none {
					finishChunk()
				}
			}
		}

		//finish setting stuff
		finishChunk()
		finishLine()
		height = CGFloat(line)
		sizeTest.text = nil

		//remove any extra chunks that weren't used
		if index < chunks.count {
			for i in index..<chunks.count {
				chunks[i].remove()
			}
			chunks.removeLast(chunks.count-index)
		}

		//sets animation parameters
		animIndex = 0
		if animate {
			animCooldown = 0
			animMax = CGFloat(charTiming.count)
			for i in chunks {
				i.node.isHidden = true
			}
		} else {
			animCooldown = 1
			animMax = 0
			for i in chunks {
				i.node.isHidden = false
				i.node.setScale(1)
			}
		}
	}

	/** animates text entrance. */
	private func animate(forceEnd:Bool) {
		if animCooldown > 0 {
			animCooldown -= Time.deltaTime*7*cooldownFactor
			if animCooldown < 0 { animCooldown = 0 }
		} else if animIndex < animMax {
			let count = charTiming.count-1
			let from = Int(floor(animIndex))
			if forceEnd {
				animIndex = animMax
				animCooldown = 1
				characterAnimTempo = 1
			} else {
				let vel:CGFloat
				switch GameState.textVelocity {
				case .slow: vel = 16
				case .normal: vel = 32
				case .fast: vel = 48
				case .reallyFast: vel = 96
				case .immediate: vel = 0; animIndex = animMax
				}
				animIndex += Time.deltaTime*vel*velocityFactor
				if animIndex >= animMax {
					animIndex = animMax
					animCooldown = 1
				}
			}
			let last = Int(floor(animIndex))
			let to = min(last,count)
			if from <= to {
				var chunk:Int = -1
				var indexMax:Int = 0
				func animate(last:Bool) {
					guard chunk >= 0 else { return }
					let c = chunks[chunk]
					characterAnimTempo = 1
					c.node.isHidden = false
					if last && c.style.anim != .none {
						c.node.setScale(animIndex.truncatingRemainder(dividingBy:1).lerp(1.5,1))
					} else {
						c.node.setScale(1)
						if indexMax < c.length {
							c.node.text = c.text.substring(to:c.text.index(c.text.startIndex,offsetBy:indexMax))
						} else {
							c.node.text = c.text
						}
					}
				}
				for i in from...to {
					let ni = charTiming[i]
					if ni.chunk != chunk {
						animate(last:false)
						chunk = ni.chunk
					}
					indexMax = ni.index+1
				}
				animate(last:to == last)
			}
			if animIndex >= animMax {
				charTiming.removeAll(keepingCapacity:true)
			}
		}
	}

	/**
	arranges text chunks.
	- parameter firstTime: if true, arranges everything; else, arranges only what's changed over time.
	*/
	private func arrange(firstTime:Bool) {
		let ox:CGFloat
		switch anchorX {
		case .left: ox = 0
		case .centre: ox = -width/2
		case .right: ox = -width
		}
		let oy:CGFloat
		switch anchorY {
		case .top: oy = 0
		case .centre: oy = (height-1)/2
		case .bottom: oy = (height-1)
		}
		for (n,c) in chunks.enumerated() {
			if firstTime {
				c.node.fontColor = colours[c.style.colour]
			}
			let f = CGFloat(n)
			var x:CGFloat
			var y:CGFloat
			switch c.style.anim {
			case .shake:
				let t = Time.time*48-f
				x = TextNode.noise.value(at:t)*4
				y = TextNode.noise.value(at:t-32)*4
			case .wave:
				let t = Time.time*2-f
				let s = TextNode.fontSize*0.07
				x = sin(t)*s
				y = cos(t)*s
			default:
				if firstTime {
					x = 0
					y = 0
				} else {
					continue
				}
			}
			let lox:CGFloat
			switch align {
			case .left: lox = 0
			case .centre: lox = (width-lineWidth[c.line])/2
			case .right: lox = width-lineWidth[c.line]
			}
			x += lox+ox+c.pos
			y += (oy-CGFloat(c.line))*TextNode.fontSize
			c.node.position = CGPoint(x:x,y:y)
		}
	}
}

/**
text horizontal positioning.
*/
enum TextPosX {
	case left
	case centre
	case right
}

/**
text vertical positioning.
*/
enum TextPosY {
	case top
	case centre
	case bottom
}
