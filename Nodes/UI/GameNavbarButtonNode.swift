import SpriteKit

class GameNavbarButtonNode:ButtonNode {
	let sprite = SKSpriteNode()
	let text = TextNode()

	override var hitSize:CGSize {
		get {
			return CGSize(width:sprite.frame.width,height:sprite.frame.height)
		}
		set {
			//
		}
	}

	override var hitOffset:CGPoint {
		get {
			return CGPoint(x:sprite.frame.midX,y:sprite.frame.midY)
		}
		set {
			//
		}
	}

	init(text:String) {
		super.init()
		let scale:CGFloat = Window.padMode ? 0.135 : 0.15
		addChild(sprite)
		sprite.texture = SKTexture(image:#imageLiteral(resourceName: "ui/navbarButton"))
		let s = Window.sceneSize.width*scale
		sprite.size = CGSize(width:s,height:s)
		addChild(self.text)
		self.text.colours = [SKColor.black]
		self.text.text = text
		self.text.zPosition = 1
		self.text.setScale(scale*2.5)
		self.text.position = CGPoint(x:0,y:-self.text.yScale*TextNode.fontSize*0.25)
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	private var scaleAnim:CGFloat = 1

	override func updateButton() {
		super.updateButton()
		if released {
			scaleAnim = 1.1
			updateScale()
		} else {
			let s:CGFloat = held ? 0.9 : 1
			if scaleAnim > s {
				scaleAnim -= Time.deltaTime*2
				if scaleAnim < s { scaleAnim = s }
				updateScale()
			} else if scaleAnim < s {
				scaleAnim += Time.deltaTime*2
				if scaleAnim > s { scaleAnim = s }
				updateScale()
			}
		}
	}

	private func updateScale() {
		setScale(scaleAnim)
	}
}
