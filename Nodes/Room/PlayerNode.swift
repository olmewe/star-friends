import SpriteKit

class PlayerNode: CharacterNode {
	init() {
		super.init(type:.player,path:"entities/player/sprite")
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
	}
}
