import SpriteKit

class CharacterNode: SKNode {
	let shadow = SKSpriteNode(texture:SKTexture(image:#imageLiteral(resourceName: "room/shadow")))
	let sprite:SKSpriteNode
	let textures:[SKTexture]
	let type:CharacterType
	enum CharacterType {
		case player
		case boss
		case entity
	}

	var velocity: CGFloat = 3
	var moving: Bool = false
	var tx: CGFloat = 0.0
	var ty: CGFloat = 0.0

	var frameChange:CGFloat = 0
	var currentFrame:Int = 0
	var spriteDirection:CGFloat = 1

	init(type:CharacterType,path:String) {
		textures = [
			SKTexture(imageNamed:path+"0"),
			SKTexture(imageNamed:path+"1"),
			SKTexture(imageNamed:path+"2"),
		]
		sprite = SKSpriteNode(texture:textures[0])
		self.type = type
		super.init()
		if type != .boss {
			addChild(shadow)
			shadow.zPosition = -1.5
			shadow.position = CGPoint.zero
			shadow.size = CGSize.one
			shadow.alpha = 0.125
		}
		addChild(sprite)
		sprite.zPosition = 0
		if type == .boss {
			sprite.position = CGPoint(x:0,y:1)
			sprite.size = CGSize(width:3,height:3)
		} else {
			sprite.position = CGPoint(x:0,y:0.5)
			sprite.size = CGSize(width:1.5,height:1.5)
		}
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	func resetPosition() {
		self.tx = self.position.x
		self.ty = self.position.y
		moving = false
	}

	func setNewPosition(x: CGFloat, y: CGFloat) {
		self.tx = x
		self.ty = y
		moving = true
	}

	func update(map:TileMapNode) -> RoomDirection? {
		var direction:RoomDirection? = nil

		if moving {
			let delta = self.velocity * Time.deltaTime
			let vx = self.tx - self.position.x
			let vy = self.ty - self.position.y
			let sqrMagnitude = (vx * vx) + (vy * vy)
			spriteDirection = (vx > 0) ? 1 : -1

			let px = self.position.x
			let py = self.position.y
			var nx:CGFloat
			var ny:CGFloat
			if sqrMagnitude <= delta*delta {
				nx = self.tx
				ny = self.ty
				moving = false
			}else{
				let factor = delta/sqrt(sqrMagnitude)
				nx = px + vx*factor
				ny = py + vy*factor
			}

			var stop:Bool
			switch map.collide(at: CGPoint(x:nx,y:ny), size: CGSize.one) {
			case .passthrough:
				stop = false
				let x = map.centerX - 0.5
				let y = map.centerY - 0.5
				if nx < -x {
					nx = -x
					stop = true
				} else if nx > x {
					nx = x
					stop = true
				}
				if ny < -y {
					ny = -y
					stop = true
				} else if ny > y {
					ny = y
					stop = true
				}
			case .door(let dir):
				direction = dir
				fallthrough
			case .collide:
				stop = true
				nx = px
				ny = py
			}

			self.position = CGPoint(x:nx,y:ny)
			updateZ()
			if stop {
				resetPosition()
			}
		}

		if type == .boss || frameChange > 0 {
			frameChange -= Time.deltaTime*velocity*3
			if frameChange <= 0 {
				updateFrame()
			}
		} else if moving {
			updateFrame()
		}

		return direction
	}

	private func updateFrame() {
		if moving || type == .boss {
			frameChange = 1
			currentFrame += 1
			if currentFrame >= (type == .boss ? 3 : 4) {
				currentFrame = 0
			}
		} else {
			frameChange = 0
			currentFrame = 0
		}
		if type == .boss {
			sprite.texture = textures[currentFrame]
		} else {
			var playSfx:Bool
			switch currentFrame {
			case 1: sprite.texture = textures[1]; playSfx = true
			case 3: sprite.texture = textures[2]; playSfx = true
			default: sprite.texture = textures[0]; playSfx = false
			}
			if playSfx {
				Audio.play(sfx:(type == .player) ? SceneManager.roomScene.sfxWalkPlayer : SceneManager.roomScene.sfxWalkEntity)
			}
		}
		sprite.xScale = spriteDirection
	}

	func updateZ() {
		self.zPosition = 3-((position.y+5)/10)
	}
}
