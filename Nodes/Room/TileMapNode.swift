import SpriteKit

class TileMapNode: SKNode {
	private(set) var width:Int
	private(set) var height:Int

	private(set) var centerX:CGFloat
	private(set) var centerY:CGFloat

	private var bg:[[Int]] = []
	private var fg:[[Int]] = []

	private static var tileAtlas:SKTextureAtlas?
	private static var tileMax:Int = 0
	private static var tileLoaded:String = ""

	init(bg:[[Int]],fg:[[Int]]) {
		height = bg.count
		width = bg[0].count
		centerX = CGFloat(width)/2
		centerY = CGFloat(height)/2
		self.bg = bg
		self.fg = fg
		super.init()
		if TileMapNode.tileLoaded != LevelContext.current.name {
			TileMapNode.tileLoaded = LevelContext.current.name
			let baseName = "contexts/\(TileMapNode.tileLoaded)/tiles/"
			var tex:[String:UIImage] = [:]
			for i in LevelContext.current.tiles {
				if tex[i.texture] == nil {
					tex[i.texture] = UIImage(named:baseName+i.texture)
				}
			}
			TileMapNode.tileAtlas = SKTextureAtlas(dictionary:tex)
			TileMapNode.tileMax = LevelContext.current.tiles.count
		}
		createMap(matrix:bg,z:0)
		createMap(matrix:fg,z:0.1)
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	private func createMap(matrix:[[Int]],z:CGFloat) {
		guard let tiles = TileMapNode.tileAtlas else { return }
		let ox = -centerX+0.5
		let oy = -centerY+0.5
		for y in 0..<matrix.count {
			for x in 0..<matrix[y].count {
				let i = matrix[y][x]
				if i < 0 || i >= TileMapNode.tileMax { continue }
				let tile = LevelContext.current.tiles[i]
				let t = SKSpriteNode(texture:tiles.textureNamed(tile.texture))
				addChild(t)
				t.position = CGPoint(x:CGFloat(x)+ox,y:CGFloat(y)+oy)
				t.zPosition = z
				t.size = tile.size
			}
		}
	}

	func collide(at point:CGPoint) -> CollisionResult {
		let x = Int(floor(centerX+point.x))
		let y = Int(floor(centerY+point.y))
		return collide(xa:x,xb:x,ya:y,yb:y)
	}

	func collide(at center:CGPoint,size:CGSize) -> CollisionResult {
		let x = centerX+center.x
		let y = centerY+center.y
		let w = size.width/2
		let h = size.height/2
		return collide(
			xa:Int(floor(x-w)),
			xb:Int(floor(x+w)),
			ya:Int(ceil(y-h)-1),
			yb:Int(ceil(y+h)-1)
		)
	}

	func collide(xa:Int,xb:Int,ya:Int,yb:Int) -> CollisionResult {
		if xa > xb || ya > yb || xb < 0 || xa >= width || yb < 0 || ya >= height {
			return .passthrough
		}
		var collide = false
		for x in max(0,xa)...min(width-1,xb) {
			for y in max(0,ya)...min(height-1,yb) {
				let bc = LevelContext.current.tileCollision(of:bg[y][x])
				let fc = LevelContext.current.tileCollision(of:fg[y][x])
				if bc == .door || fc == .door {
					if x == 0 { return .door(.left) }
					if x == width-1 { return .door(.right) }
					if y == 0 { return .door(.down) }
					if y == height-1 { return .door(.up) }
					collide = true
				} else if bc == .collide || fc == .collide {
					collide = true
				}
			}
		}
		return collide ? .collide : .passthrough
	}

	enum CollisionResult {
		case passthrough
		case collide
		case door(_:RoomDirection)
	}
}
