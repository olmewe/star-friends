import SpriteKit

class EntityNode: CharacterNode {

	init(useContext:Bool,name:String,happy:Bool) {
		let boss = name == "boss"
		var path:String
		if useContext {
			path = "contexts/"+LevelContext.current.name+"/"
		} else {
			path = ""
		}
		path += "entities/"+name+"/sprite"
		if !boss {
			path += happy ? "b" : "a"
		}
		super.init(type:boss ? .boss : .entity,path:path)
		self.velocity = 1.5
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
	}

	func randomPosition() {
		let r = CGFloat(1.5)

		let newX = Random.range(min: self.position.x - r, max: self.position.x + r)
		let newY = Random.range(min: self.position.y - r, max: self.position.y + r)

		self.setNewPosition(x: newX, y: newY)
	}

}
