import Foundation


public struct FaseData{

	public let fases: [TilesData]

	public init(fases: [TilesData]) {
		self.fases = fases
	}

}


extension FaseData: JSONDecodable{

	init(decoder: JSONDecoder) throws {
		self.fases = try decoder.decode(key: "tiles.fase1")

	}

}
