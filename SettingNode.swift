import SpriteKit

class SettingNode: SKNode {
	static let spriteAtlas = SKTextureAtlas(named:"settings")

	var isOpen = false
	var fromMenu = false

	var btnBack: SKSpriteNode!
	var btnManual: SKSpriteNode!
	var btnPluginMute: btnPluginNode!
	var btnPlugin:[btnPluginNode] = []
	var sliderBar: [SliderNode] = []

	var pluginEnable = 1

	override init(){
		super.init()

		isOpen = false
		isHidden = true

		let orange = #colorLiteral(red: 0.968627451, green: 0.5764705882, blue: 0.1176470588, alpha: 1)
		let red = #colorLiteral(red: 0.9450980392, green: 0.3529411765, blue: 0.1411764706, alpha: 1)
		let fontSize = Window.sceneSize.height*0.035

		//line aligment values and stuff
		let margin0 = Window.sceneSize.width*(3/40-0.5)
		let margin1 = Window.sceneSize.width*(19/120-0.5)
		let lineTop = Window.sceneSize.height*0.5-Window.sceneSize.width*(17/60+Window.ratioUnit*0.075)
		let lineBottom = Window.sceneSize.width*(7/20+Window.ratioUnit*0.075)-Window.sceneSize.height*0.5
		let plugSize = Window.sceneSize.width*1/19*(Window.padMode ? 1 : 1.25)
		let slider0 = Window.sceneSize.width*(Window.ratioUnit.lerp(0.43,0.55)-0.5)
		let slider1 = -margin0
		func lineHeight(at line:Int) -> CGFloat {
			let lines:CGFloat = 9
			return (CGFloat(line)/(lines-1)).lerp(lineTop,lineBottom)
		}
		func title(text:String) {
			let label = SKLabelNode(fontNamed:"Schoolbell")
			label.fontColor = .white
			label.fontSize = fontSize*4/3
			label.horizontalAlignmentMode = .center
			label.verticalAlignmentMode = .baseline
			label.text = text
			label.position = CGPoint(x:0,y:Window.sceneSize.height*0.5-Window.sceneSize.width*0.16)
			label.zPosition = 5
			addChild(label)
		}
		func label(line:Int,text:String,margin:Bool,hl:Bool) {
			let label = SKLabelNode(fontNamed:"Schoolbell")
			label.fontColor = hl ? red : orange
			label.fontSize = fontSize
			label.horizontalAlignmentMode = .left
			label.verticalAlignmentMode = .baseline
			label.text = text
			label.position = CGPoint(x:margin ? margin1 : margin0,y:lineHeight(at:line)-5)
			label.zPosition = 5
			addChild(label)
		}
		func plug(line:Int,enabled:Bool) -> btnPluginNode {
			let plugin = btnPluginNode(size:plugSize)
			plugin.position = CGPoint(x:margin0+plugSize*0.5,y:lineHeight(at:line))
			plugin.zPosition = 5
			plugin.set(enable:enabled,animated:false)
			addChild(plugin)
			return plugin
		}
		func slider(line:Int,value:CGFloat) -> SliderNode {
			let slider = SliderNode(size:CGSize(width:slider1-slider0,height:Window.sceneSize.height*Window.ratioUnit.lerp(0.04,0.05)))
			slider.position = CGPoint(x:(slider0+slider1)*0.5,y:lineHeight(at:line))
			slider.zPosition = 5
			slider.set(porcent:Int(value*100))
			addChild(slider)
			return slider
		}

		//background solid
		let background = SKSpriteNode(color: #colorLiteral(red: 0.3529411765, green: 0.2352941176, blue: 0.4117647059, alpha: 1), size: Window.sceneSize)
		addChild(background)
		background.position = .zero
		background.zPosition = 0

		//header pic
		let header = SKSpriteNode(texture: SettingNode.spriteAtlas.textureNamed("header"))
		addChild(header)
		header.anchorPoint = CGPoint(x:0.5,y:1)
		header.size = CGSize(width:Window.sceneSize.width,height:Window.sceneSize.width*231/1024)
		header.position = CGPoint(x:0,y:Window.sceneSize.height*0.5)
		header.zPosition = 1

		//footer buttons and bottom cable
		let buttonOffset:CGFloat = 0.1257
		let cableOffset:CGFloat = 0.0821
		let buttonSize = Window.sceneSize.height*0.1
		let buttonS = CGSize(width:buttonSize,height:buttonSize)
		let buttonY = Window.sceneSize.width*1/15-Window.sceneSize.height*0.5
		var buttonX = -margin0-buttonSize*0.5
		btnManual = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("btnManual"),color:.clear,size:buttonS)
		addChild(btnManual)
		btnManual.position = CGPoint(x:buttonX,y:buttonY+buttonSize*0.5)
		btnManual.zPosition = 5
		buttonX -= Window.sceneSize.height*buttonOffset
		btnBack = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("btnBack"),color:.clear,size:buttonS)
		addChild(btnBack)
		btnBack.position = CGPoint(x:buttonX,y:buttonY+buttonSize*0.5)
		btnBack.zPosition = 5
		buttonX -= Window.sceneSize.height*cableOffset
		let cable0 = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("cable0"))
		addChild(cable0)
		let cable0Width = buttonX+Window.sceneSize.width*0.5
		cable0.size = CGSize(width:cable0Width,height:cable0Width*342/693)
		cable0.anchorPoint = CGPoint(x:1,y:0)
		cable0.position = CGPoint(x:buttonX,y:buttonY)
		cable0.zPosition = 1

		//right cable
		let cable1 = SKSpriteNode(texture:SettingNode.spriteAtlas.textureNamed("cable1"))
		addChild(cable1)
		let cable1Width = Window.sceneSize.width*16/75
		cable1.size = CGSize(width:cable1Width,height:cable1Width*393/254)
		cable1.anchorPoint = CGPoint(x:1,y:0.5)
		cable1.position = CGPoint(x:Window.sceneSize.width*0.5,y:Window.sceneSize.height*0.1965)
		cable1.zPosition = 1

		//title
		title(text:"sistemas de navegação")

		//difficulty
		label(line:0,text:"dificuldade:",margin:false,hl:true)
		label(line:1,text:"fácil",margin:true,hl:false)
		label(line:2,text:"médio",margin:true,hl:false)
		label(line:3,text:"difícil",margin:true,hl:false)
		switch GameState.difficulty {
		case .easy: pluginEnable = 0
		case .medium: pluginEnable = 1
		case .hard: pluginEnable = 2
		}
		for i in 0..<3 {
			btnPlugin.append(plug(line:i+1,enabled:i == pluginEnable))
		}

		//volume
		label(line:4,text:"volume:",margin:false,hl:true)
		label(line:5,text:"trilha sonora",margin:false,hl:false)
		label(line:6,text:"efeitos sonoros",margin:false,hl:false)
		label(line:7,text:"diálogo",margin:false,hl:false)
		label(line:8,text:"mudo",margin:true,hl:false)
		sliderBar.append(slider(line:5,value:GameState.volumeBgm))
		sliderBar.append(slider(line:6,value:GameState.volumeSfx))
		sliderBar.append(slider(line:7,value:GameState.volumeSpeech))
		btnPluginMute = plug(line:8,enabled:GameState.mute)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func update() {
		for i in 0..<3 {
			sliderBar[i].update()
		}

		var updateAudio = false
		if sliderBar[0].updated {
			GameState.volumeBgm = CGFloat(sliderBar[0].porcent)/100
			updateAudio = true
		}
		if sliderBar[1].updated {
			GameState.volumeSfx = CGFloat(sliderBar[1].porcent)/100
		}
		if sliderBar[2].updated {
			GameState.volumeSpeech = CGFloat(sliderBar[2].porcent)/100
			updateAudio = true
		}

		for events in Input.touchEvents{

			if events.state == .release {
				if (events.position.x > (btnBack.position.x - btnBack.size.width/2)) && (events.position.x < (btnBack.position.x + btnBack.size.width/2)) && (events.position.y > (btnBack.position.y - btnBack.size.width/2)) && (events.position.y < (btnBack.position.y + btnBack.size.width/2)){
					close()
					break
				}
				if (events.position.x > (btnManual.position.x - btnManual.size.width/2)) && (events.position.x < (btnManual.position.x + btnManual.size.width/2)) && (events.position.y > (btnManual.position.y - btnManual.size.width/2)) && (events.position.y < (btnManual.position.y + btnManual.size.width/2)){
					close()
					SceneManager.current.openManual(fromMenu:fromMenu)
					break
				}
			} else if events.state == .press{

				for i in 0..<3{
					if (events.position.x > (btnPlugin[i].position.x - btnPlugin[i].feedbackPlugin.size.width/2)) && (events.position.x < (btnPlugin[i].position.x + btnPlugin[i].feedbackPlugin.size.width/2)) && (events.position.y > (btnPlugin[i].position.y - btnPlugin[i].btnPlugin.size.width/2)) && (events.position.y < (btnPlugin[i].position.y + btnPlugin[i].btnPlugin.size.width/2)){

						veriPlugin(i: i)
					}
				}

				if (events.position.x > (btnPluginMute.position.x - btnPluginMute.feedbackPlugin.size.width/2)) && (events.position.x < (btnPluginMute.position.x + btnPluginMute.feedbackPlugin.size.width/2)) && (events.position.y > (btnPluginMute.position.y - btnPluginMute.btnPlugin.size.width/2)) && (events.position.y < (btnPluginMute.position.y + btnPluginMute.btnPlugin.size.width/2)){

					btnPluginMute.set(enable: !btnPluginMute.enable)
					GameState.mute = btnPluginMute.enable
					updateAudio = true
				}
			}
		}

		if updateAudio {
			Audio.updateVolume()
		}
	}

	func veriPlugin(i: Int){

		if i != pluginEnable {
			btnPlugin[i].set(enable: true)
			btnPlugin[pluginEnable].set(enable: false)
			pluginEnable = i
			switch i {
			case 0: GameState.difficulty = .easy
			case 1: GameState.difficulty = .medium
			case 2: GameState.difficulty = .hard
			default: break
			}
		}

	}

	func open(fromMenu:Bool) {
		guard !isOpen else { return }
		isOpen = true
		self.fromMenu = fromMenu
		isHidden = false
		Audio.play(sfx:CommonAudio.sfxSettingsOpen)
		self.alpha = 0
		self.run(SKAction.fadeIn(withDuration: 0.2)) {
			self.isHidden = false
		}
	}

	func close() {
		guard isOpen else { return }
		GameState.saveSettings()
		isOpen = false
		isHidden = false
		Audio.play(sfx:CommonAudio.sfxSettingsClose)
		self.alpha = 1
		self.run(SKAction.fadeOut(withDuration: 0.2)) {
			self.isHidden = true
		}
	}
}
